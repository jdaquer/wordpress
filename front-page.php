<?php get_header() ?>

<?php get_template_part( 'tpl/widget/_slider' ); ?>

<div class="hidden-xs hidden-sm">
	<?php get_template_part( 'tpl/widget/_home-list_page' ); ?>
</div>


<?php if ( cz( 'features' ) ) { ?>
	<div class="container">
		<div class="row">
			<?php get_template_part( 'tpl/widget/_features' ); ?>
		</div>
	</div>
<?php } ?>

	<!--TOP SELLING PRODUCT-->
	<div id="top-selling-product" class="list-product top-selling-product">
		<div class="container">
			<div class="p-heading">
                <h3 class="p-title">
                    <?php _e( 'Top Selling Products', 'rem' );?>
                </h3>
			</div>
			<div class="row">
				<?php get_template_part( 'tpl/product/home/_topselling' ); ?>
			</div>
		</div>
	</div>
	<!--BEST DEALS-->
	<div id="best-deals" class="list-product best-deals">
		<div class="container">
			<div class="p-heading">
                <h3 class="p-title">
                    <?php _e( 'Best deals', 'rem' );?>
                </h3>
			</div>
			<div class="row">
				<?php get_template_part( 'tpl/product/home/_bestdials' ); ?>
			</div>
		</div>
	</div>
	<!--NEW ARRIVALS-->
	<div id="new-arrivals" class="list-product new-arrivals">
		<div class="container">
			<div class="p-heading">
                <h3 class="p-title">
                    <?php _e( 'new arrivals', 'rem' );?>
                </h3>
			</div>
			<div class="row">
				<?php get_template_part( 'tpl/product/home/_arrivals' ); ?>
			</div>
		</div>
	</div>
	<!-- PAGE-ARTICLE-->
<?php if ( trim( cz( 'tp_home_article' ) ) ): ?>
	<div class="wrap-page-article">
		<div class="container">
			<div class="row">
				<div class="col-xs-60 col-md-40">
					<div class="page-article">
						<?php
						$content = cz( 'tp_home_article' );
						echo apply_filters( 'the_content', $content );
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>

<?php get_template_part( 'tpl/widget/_instagram' ); ?>
<?php get_template_part( 'tpl/widget/_video' ); ?>


<?php get_footer() ?>
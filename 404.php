<?php
/*global $wp_query;
print_r($wp_query);*/
?>
<?php get_header() ?>
<div class="page-content">
	<div class="container">
		<div class="page-404">
			<div class="page-404__text">
				<h3><?php _e( '404 Page not found', 'rem' ); ?>.</h3>
				<p><?php _e( 'Sorry, but the page you are looking for has not been found', 'rem' ); ?>.<br>
					<?php _e( 'Please check your spelling', 'rem' ); ?>.</p>
			</div>
			<div class="page-404__back_btn">
				<a href="/" class="btn btn-bord"><?php _e( 'Go To Homepage', 'rem' ); ?></a>
			</div>
		</div>
	</div>
</div>
<?php get_footer() ?>

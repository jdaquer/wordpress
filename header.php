<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js" xmlns="http://www.w3.org/1999/html">
<head>
	<link rel="shortcut icon" href="<?php echo cz( 'tp_favicon' ); ?>"/>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="apple-mobile-web-app-capable" content="yes"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <?php adstm_wp_seo(); ?>
	<link href='//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css'
	      rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&subset=cyrillic" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=EB+Garamond&amp;subset=cyrillic-ext" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri(); ?>/style.css?100" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri(); ?>/inc/customization/css/custom_style.css?100" rel="stylesheet">
	<?php wp_head(); ?>

	<!--[if lt IE 9]>
	<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>    <![endif]-->

	<?php /*
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri() ?>/img/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri() ?>/img/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri() ?>/img/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri() ?>/img/favicon/apple-icon-152x152.png">
	<link rel='shortcut icon' type="image/x-icon" href='<?php echo get_template_directory_uri() ?>/img/favicon/favicon-16x16.png'>
	*/ ?>
	<?php if ( cz( 'tp_head_ga' ) ): ?>
		<script>
			(function ( i, s, o, g, r, a, m ) {
				i[ 'GoogleAnalyticsObject' ] = r;
				i[ r ] = i[ r ] || function () {
						(i[ r ].q = i[ r ].q || []).push( arguments )
					}, i[ r ].l = 1 * new Date();
				a = s.createElement( o ),
					m = s.getElementsByTagName( o )[ 0 ];
				a.async = 1;
				a.src   = g;
				m.parentNode.insertBefore( a, m )
			})( window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga' );

			ga( 'create', '<?php echo cz( 'tp_head_ga' );?>', 'auto' );
			ga( 'send', 'pageview' );

		</script>
	<?php else: ?>
		<script>
			window.ga = window.ga || function () {
				};
		</script>
	<?php endif; ?>

	<?php if ( cz( 'tp_facebook_pixel' ) ): ?>
		<!-- Facebook Pixel Code -->
		<script>
			!function ( f, b, e, v, n, t, s ) {
				if ( f.fbq )return;
				n = f.fbq = function () {
					n.callMethod ?
						n.callMethod.apply( n, arguments ) : n.queue.push( arguments )
				};
				if ( !f._fbq )f._fbq = n;
				n.push    = n;
				n.loaded  = !0;
				n.version = '2.0';
				n.queue   = [];
				t         = b.createElement( e );
				t.async   = !0;
				t.src     = v;
				s         = b.getElementsByTagName( e )[ 0 ];
				s.parentNode.insertBefore( t, s )
			}( window,
				document, 'script', 'https://connect.facebook.net/en_US/fbevents.js' );

			fbq( 'init', '<?php echo cz( 'tp_facebook_pixel' );?>' );
			fbq( 'track', "PageView" );

			<?php do_action( 'rem_fb_pixel' ) ?>

		</script>
		<noscript><img height="1" width="1" style="display:none"
		               src="https://www.facebook.com/tr?id=<?php echo cz( 'tp_facebook_pixel' ); ?>&ev=PageView&noscript=1"
			/></noscript>
		<!-- End Facebook Pixel Code -->
	<?php endif; ?>

	<style>
		<?php echo cz('tp_style');?>
	</style>
	<?php echo cz( 'tp_head' ); ?>
</head>

<body <?php body_class(); ?>>
<div class="wrap">

	<?php get_template_part( 'header', 'top' ); ?>

	<header class="header-desc hidden-xs hidden-sm">
		<div class="container">
			<div class="wrap-header">

				<div class="logo-box pull-left">
					<div class="logo">
						<a href="<?php echo home_url(); ?>"><img src="<?php echo cz( 'tp_logo_img' ); ?>?1000" alt=""></a>
					</div>
				</div>

				<div class="pull-right">
					<div class="cart">
						<?php printf( '<a href="%1$s"><span class="count_item" data-cart="quantity"></span><span data-cart="pluralize_items"></span></a>',
							esc_url( home_url( '/cart/' ) )
						); ?>
					</div>
				</div>


				<div class="pull-right">
					<div class="text-shipping"><?php echo cz('tp_text_top_header'); ?></div>
				</div>
				<?php if ( cz( 'tp_img_left_cart' ) ): ?>
					<div class="pull-right">
						<span class="text-secure"></span>
					</div>
				<?php endif; ?>
				<div class="pull-right">
					<form class="search" method="GET" action="<?php echo esc_url( home_url( '/' ) ) ?>">
						<div class="input-group">
							<input type="text" name="s" class="form-control" placeholder="Search" value="">
							<span class="input-group-btn">
							<button class="btn btn-default btn-inverse" type="submit"><i class="fa fa-search"></i>
							</button>
						</span>
						</div>
					</form>
				</div>

			</div>
			<?php
			if ( file_exists( dirname( __FILE__ ) . '/tpl/_categories_menu.php' ) ) {
				include dirname( __FILE__ ) . '/tpl/_categories_menu.php';
			} ?>
		</div>
	</header>

	<header class="header-mobile visible-xs visible-sm">
		<div class="pull-right">
			<form class="search" method="GET" action="<?php echo esc_url( home_url( '/' ) ) ?>">
				<div class="input-group">
					<input type="text" name="s" class="form-control" placeholder="Search" value="" required>
                    <button class="btn btn-default btn-inverse icon" type="submit"></button>
				</div>
			</form>
			<?php if ( cz( 's_mail' ) ): ?>
				<a href="mailto:<?php echo cz( 's_mail' ); ?>" class="icon email"></a>
			<?php endif; ?>
			<?php if ( cz( 's_phone' ) ): ?>
				<a href="tel:<?php echo cz( 's_phone' ); ?>" class="icon tel"></a>
			<?php endif; ?>
			<?php adstm_login_button();?>
		</div>

		<div class="menu">
			<button type="button" class="navbar-toggle-btn collapsed" data-toggle="collapse" data-target="#main-menu-mobile" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
				<span class="icon-bar"></span> <span class="icon-bar"></span>
				<span class="memu_close"></span>
			</button>
			
		</div>
		<div class="menu-list collapse" id="main-menu-mobile">
			<div class="mobile-top_home">
				<div class="dropdown dropdown_currency">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#" ajax_update="currency"></a>
					<ul class="dropdown-menu" role="menu">
						<?php
						if ( function_exists( 'ads_select_currency' ) ) {
							echo ads_select_currency();
						}
						?>
					</ul>
				</div>
				<ul>
					<li class="menu-item-home"><a href="/"><?php _e( 'Home', 'rem' ); ?></a></li>
				</ul>
			</div>

			<div class="mobile-main-menu">
				<div class="head"><?php _e( 'Products', 'rem' ); ?></div>
				<?php if ( file_exists( dirname( __FILE__ ) . '/tpl/_categories_menu_mobile.php' ) ) {
					include dirname( __FILE__ ) . '/tpl/_categories_menu_mobile.php';
				} ?>
			</div>
			<div class="visible-sm visible-xs mobile-top_menu">
				<?php wp_nav_menu(
					array(
						'menu'       => 'Top Menu',
						'container'  => '',
						'menu_class' => '',
						'items_wrap' => '<ul>%3$s</ul>'
					)
				) ?>
			</div>

			<div class="menu-contact">
				<?php if ( cz( 's_mail' ) ): ?>
					<div class="email"><?php echo( cz( 's_mail' ) ); ?></div>
				<?php endif; ?>
				<?php if ( cz( 's_phone' ) ): ?>
					<div class="tel"><?php echo( cz( 's_phone' ) ); ?></div>
				<?php endif; ?>

				<?php if(true): ?>
					<ul class="list-social">
						<?php if ( cz( 's_link_fb' ) ): ?>
							<li>
								<a href="<?php echo cz( 's_link_fb' ); ?>" class="ico-social fb" target="_blank" rel="nofollow"></a>
							</li>
						<?php endif; ?>
						<?php if ( cz( 's_link_in' ) ): ?>
							<li>
								<a href="<?php echo cz( 's_link_in' ); ?>" class="ico-social in" target="_blank" rel="nofollow"></a>
							</li>
						<?php endif; ?>
						<?php if ( cz( 's_link_tw' ) ): ?>
							<li>
								<a href="<?php echo cz( 's_link_tw' ); ?>" class="ico-social tw" target="_blank" rel="nofollow"></a>
							</li>
						<?php endif; ?>
						<?php if ( cz( 's_link_gl' ) ): ?>
							<li>
								<a href="<?php echo cz( 's_link_gl' ); ?>" class="ico-social gp" target="_blank" rel="nofollow"></a>
							</li>
						<?php endif; ?>
						<?php if ( cz( 's_link_pt' ) ): ?>
							<li>
								<a href="<?php echo cz( 's_link_pt' ); ?>" class="ico-social pn" target="_blank" rel="nofollow"></a>
							</li>
						<?php endif; ?>
						<?php if ( cz( 's_link_yt' ) ): ?>
							<li>
								<a href="<?php echo cz( 's_link_yt' ); ?>" class="ico-social yt" target="_blank" rel="nofollow"></a>
							</li>
						<?php endif; ?>
					</ul>
				<?php endif; ?>
			</div>
		</div>

        <span class="text-info"><?php echo cz('tp_text_top_header') ?></span>

		<div class="wrap-header">

			<div class="logo-box pull-left">
				<div class="logo">
					<a href="<?php echo home_url(); ?>"><img src="<?php echo cz( 'tp_logo_img' ); ?>?1000" alt=""></a>
				</div>
			</div>
            <div class="pull-right">
				<div class="cart">
					<?php printf( '<a href="%1$s"><span class="count_item" data-cart="quantity"></span><span data-cart="pluralize_items"></span></a>',
						esc_url( home_url( '/cart/' ) )
					); ?>
				</div>
			</div>
		</div>
	</header>
<?php get_header();?>

<?php if ( is_singular( 'product' ) ) {
	get_template_part( 'tpl/product/single' );
} else {
	get_template_part( 'tpl/blog/index' );
} ?>

<?php get_footer(); ?>
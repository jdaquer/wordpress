<?php get_header(); ?>

<?php if ( is_page('thank-you-contact') ) {
	get_template_part( 'tpl/page/thank-you-contact' );
} else if ( is_page('about-us') ) {
	get_template_part( 'tpl/page/about-us' );
} else if ( is_page('contact-us') ) {
	get_template_part( 'tpl/page/contact-us' );
} else if ( is_page('account') ) {
    get_template_part ( 'tpl/page/account' );
} else if (is_page( 'orders' )) {
    get_template_part ( 'tpl/page/orders' );
} else if (is_page( 'userlogin' )) {
    get_template_part ( 'tpl/page/userlogin' );
} else {
	get_template_part( 'tpl/page/index' );
} ?>

<?php get_footer(); ?>
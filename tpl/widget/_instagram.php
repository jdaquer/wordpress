<?php
/**
 * Created by PhpStorm.
 * User: sunfun
 * Date: 05.09.16
 * Time: 10:42
 */
?>
<?php
//TODO переделать на js
$in = new adstm_instagram( cz('s_in_name_api') );
$params   = $in->params();
if ( $params ):
	?>
	<div class="instagram-box">
		<div class="container">
			<div class="row">
				<div class="col-md-60">
					<div class="p-heading">
						<div class="p-title"><?php echo cz('s_in_name_group'); ?></div>
					</div>
					<ul class="follow">
						<?php
						foreach ( $params[ 'images' ] as $image ) {
							printf( '<li><div class="img-wrap"><a href="%2$s"><img src="%1$s" alt=""></a></div></li>',
								$image,
								cz('s_link_in')
							);
						} ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>

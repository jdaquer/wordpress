<?php $list_product = cz('home_list_product'); ?>

<div class="home-list_page">
	<div class="container">
		<div class="row">
			<div class="col-xs-60 col-sm-20 col-md-15">
				<div class="box box1">
					<div class="text">
						<div class="head"><?php echo $list_product[0]['head'] ?></div>
						<p><?php echo $list_product[0]['text'] ?></p>
						<a href="<?php echo $list_product[0]['shop_now_link'] ?>"><?php _e( 'Shop Now', 'rem' ); ?></a>
					</div>
				</div>
			</div><div class="col-xs-30 col-sm-20 col-md-30">
				<div class="box box2">
					<div class="text">
						<div class="head"><?php echo $list_product[1]['head'] ?></div>
						<p><?php echo $list_product[1]['text'] ?></p>
						<a href="<?php echo $list_product[1]['shop_now_link'] ?>"><?php _e( 'Shop Now', 'rem' ); ?></a>
					</div>
				</div>
			</div><div class="col-xs-30 col-sm-20 col-md-15">
				<div class="box box3">
					<div class="text">
						<div class="head"><?php echo $list_product[2]['head'] ?></div>
						<p><?php echo $list_product[2]['text'] ?></p>
						<a href="<?php echo $list_product[2]['shop_now_link'] ?>"><?php _e( 'Shop Now', 'rem' ); ?></a>
					</div>
				</div>
			</div>
		</div><!-- .row -->
	</div><!-- .container -->
</div>
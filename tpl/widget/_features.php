<?php
/**
 * Created by PhpStorm.
 * User: sunfun
 * Date: 05.09.16
 * Time: 10:40
 */
?>
<!--FEATURES-->
<?php if ( file_exists( dirname( __FILE__ ) . '/_icon_features.php' ) ) {
	require_once dirname( __FILE__ ) . '/_icon_features.php';
} ?>

<div class="features hidden-xs hidden-sm">
	<div class="p-heading">
		<h2 class="p-title"><?php _e( 'Why shop with us', 'rem' ) ?></h2>
	</div>
	<div class="col-md-15 col-sm-30">
		<div class="">
			<div class="img-feat">
				<?php echo rem_icon( 'feature-ico1.svg', cz( 'tp_color_features' ) ); ?>

			</div>
			<div class="text-feat"><?php echo cz( 'features_1_head' ); ?></div>
		</div>
		<p><?php echo cz( 'features_1_text' ); ?></p>

	</div>
	<div class="col-md-15 col-sm-30">
		<div class="">
			<div class="img-feat">
				<?php echo rem_icon( 'feature-ico2.svg', cz( 'tp_color_features' ) ); ?>
			</div>
			<div class="text-feat"><?php echo cz( 'features_2_head' ); ?></div>
		</div>
		<p><?php echo cz( 'features_2_text' ); ?></p>

	</div>

	<div class="col-md-15 col-sm-30">
		<div class="">
			<div class="img-feat">
				<?php echo rem_icon( 'feature-ico3.svg', cz( 'tp_color_features' ) ); ?>
			</div>
			<div class="text-feat"><?php echo cz( 'features_3_head' ); ?></div>
		</div>
		<p><?php echo cz( 'features_3_text' ); ?></p></div>
	<div class="col-md-15 col-sm-30">
		<div class="">
			<div class="img-feat">
				<?php echo rem_icon( 'feature-ico4.svg', cz( 'tp_color_features' ) ); ?>
			</div>
			<div class="text-feat"><?php echo cz( 'features_4_head' ); ?></div>
		</div>
		<p><?php echo cz( 'features_4_text' ); ?></p></div>
</div>

<div class="features features-slider visible-sm visible-xs">
	<div class="p-heading">
		<h2 class="p-title">
            <?php _e( 'Why shop with us', 'rem' );?>
        </h2>
	</div>
	<div class="list">
		<div class="item">
			<div class="">
				<div class="img-feat">
					<?php echo rem_icon( 'feature-ico1.svg', cz( 'tp_color_features' ) ); ?>

				</div>
				<div class="text-feat"><?php echo cz( 'features_1_head' ); ?></div>
			</div>
			<p><?php echo cz( 'features_1_text' ); ?></p>
		</div>
		<div class="item">
			<div class="">
				<div class="img-feat">
					<?php echo rem_icon( 'feature-ico2.svg', cz( 'tp_color_features' ) ); ?>
				</div>
				<div class="text-feat"><?php echo cz( 'features_2_head' ); ?></div>
			</div>
			<p><?php echo cz( 'features_2_text' ); ?></p>

		</div>

		<div class="item">
			<div class="">
				<div class="img-feat">
					<?php echo rem_icon( 'feature-ico3.svg', cz( 'tp_color_features' ) ); ?>
				</div>
				<div class="text-feat"><?php echo cz( 'features_3_head' ); ?></div>
			</div>
			<p><?php echo cz( 'features_3_text' ); ?></p></div>
		<div class="item">
			<div class="">
				<div class="img-feat">
					<?php echo rem_icon( 'feature-ico4.svg', cz( 'tp_color_features' ) ); ?>
				</div>
				<div class="text-feat"><?php echo cz( 'features_4_head' ); ?></div>
			</div>
			<p><?php echo cz( 'features_4_text' ); ?></p></div>
	</div>
</div>
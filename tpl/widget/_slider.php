<?php
/**
 * Created by PhpStorm.
 * User: sunfun
 * Date: 05.09.16
 * Time: 10:39
 */
?>

<!--SLIDER-->
	<div class="home-slider clearfix" style="opacity: 0">
		<?php foreach (cz('slider_home') as $key => $item):
			$emptyTextClass = !$item['head'] && !$item['text'] ? 'emptyText':'';
		if( !$item['img'] ){
			continue;
        }
			?>
		<div style="background: url(<?php echo $item['img'] ?>?1000) center/cover no-repeat">
			<div class="info <?php echo $emptyTextClass ?>">
				<div class="head" style="color:<?php echo $item['head_color'] ?>">
                    <?php echo ($key == 0) ? '<h1>' : '';?>
                        <?php echo $item['head'];?>
                    <?php echo ($key == 0) ? '</h1>' : '';?>
                </div>
				<div class="text" style="color:<?php echo $item['text_color'] ?>"><?php echo $item['text'] ?></div>
				<div class="ft">
					<a href="<?php echo $item['shop_now_link'] ?>" class="btn shop_now"><?php _e( 'Shop Now', 'rem' ); ?></a>
					<?php if(cz('id_video_youtube_home')): ?>
					<a href="" class="btn view_video" data-toggle="modal" data-target="#prHome_video" onclick="return false;"><span><?php _e( 'View Video', 'rem' ); ?></span></a>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<?php endforeach; ?>
	</div>

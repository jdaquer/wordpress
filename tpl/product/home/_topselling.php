<?php
/**
 * Created by PhpStorm.
 * User: sunfun
 * Date: 05.09.16
 * Time: 8:29
 */

global $GLOBAL, $post;
if ( ! isset( $GLOBAL[ 'id_post_show' ] ) ) {
	$GLOBAL[ 'id_post_show' ] = array();
}

$args = array(
	'post_type'      => 'product',
	'posts_per_page' => 4,
	'_orderby'       => 'promotionVolume',
	'_order'         => 'DESC',
	'post__not_in'   => $GLOBAL[ 'id_post_show' ]
);

query_posts( $args );

if ( have_posts() ) : while ( have_posts() ) :
	the_post();
	$GLOBAL[ 'id_post_show' ][] = $post->ID;
	get_template_part( 'products/product' );
	echo '<div class="col-xs-30 col-md-15">';
	get_template_part( 'tpl/product/loop/_item' );
	echo '</div>';
endwhile; endif;

wp_reset_query();
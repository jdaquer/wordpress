<?php

global $post;

$args = array(
	'post_type'      => 'product',
	'posts_per_page' => 4,
	'post__not_in'   => array( $post->ID )
);

if ( isset( $post->links ) && $post->links != '' ) {

	$links = explode( ',', $post->links );

	if ( ! empty( $links ) ) {

		$links = ads_shuffle_assoc( $links );
		$args  = array(
			'post_type'      => 'product',
			'posts_per_page' => 4,
			'_orderby'       => 'post_id',
			'_order'         => 'array',
			'post__in'       => $links
		);
	}
} else {

	/*rand product*/
	$terms = wp_get_post_terms( $post->ID, 'product_cat' );
	$terms = current( $terms );

	if ( $terms ) {
		$querystr = "	SELECT p.ID
					FROM {$wpdb->term_relationships} t INNER JOIN {$wpdb->posts} p ON p.ID = t.object_id
					WHERE p.ID NOT IN({$post->ID}) AND t.term_taxonomy_id={$terms->term_id} AND p.post_status = 'publish' AND p.post_type = 'product' 
					ORDER BY rand() LIMIT 4 ";

		$pageposts = $wpdb->get_results( $querystr );

		$post__in = array();
		foreach ( $pageposts as $sub ) {
			$post__in[] = $sub->ID;
		}

		$args = array(
			'post_type'      => 'product',
			'posts_per_page' => 4,
			'post__in'       => $post__in
		);
	}
}

query_posts( $args );

if ( have_posts() ) :

	printf( '<div class="list-product related-products">
				<div class="container">
					<div class="p-heading">
						<div class="p-title">%1$s</div>
			</div>',
		__( 'RELATED PRODUCTS', 'rem' ) );

	echo '<div class="row">';

	while ( have_posts() ) : the_post();

		echo '<div class="col-xs-30 col-md-15">';
		get_template_part( 'tpl/product/loop/_item' );
		echo '</div>';

	endwhile;

	echo '</div></div></div>';

endif;

wp_reset_query();

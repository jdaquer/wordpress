<?php
/**
 * Created by PhpStorm.
 * User: sunfun
 * Date: 06.09.16
 * Time: 13:01
 */

?>

<div class="modal fade modal-addCart" id="modalCart" tabindex="-1" role="dialog" aria-labelledby="modalCartLabel"
     aria-hidden="true">
	<script id="modal_cart_details_img_template" type="text/template">
		<div class="details-img"><img src="{{url}}" class="img-responsive"></div>
	</script>
	<script id="modal_cart_details_template" type="text/template">
		<span class="details-title">{{title}}:</span>
		<span class="details-info">{{variation}}</span>
	</script>
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span> <span class="sr-only"><?php _e( 'Close', 'rem' ) ?></span>
				</button>
				<h4 class="modal-title" id="modalCartLabel">
					<?php _e( 'Added to cart', 'rem' ) ?>
				</h4>
			</div>
			<div class="modal-body clearfix">
				<div class="list-order hidden-xs">
					<div class="head">
						<div class="col-sm-10"></div>
						<div class="col-sm-24 col-lg-26"><?php _e( 'Product', 'rem' ); ?></div>
						<div class="col-sm-14 col-lg-12 text-center"><?php _e( 'Quantity', 'rem' ); ?></div>
						<div class="col-sm-10 col-lg-10 text-center"><?php _e( 'Amount', 'rem' ); ?></div>
						<div class="col-sm-1"></div>
					</div>
				</div>
				<div id="js-modal_cart_items" class="col-xs-60 list" data-modalAddOrder="list" data-open="head-list">

					<script id="modal_item_template" type="text/template">
						<div class="row cart-item" data-key="{{order_id}}" data-post="{{post_id}}">
							<div class="hidden-xs desc">
								<div class="col-sm-10 text-center">
									<div class="im-wrap">
										<div class="thumb-wrap">
											<div class="thumb"><img src="{{thumb}}" class="img-responsive"></div>
										</div>
									</div>
								</div>
								<div class="col-sm-24 col-lg-26">
									<a href="{{link}}" class="title-link">{{title}}</a>
									<div class="shipping">
										<span class="head"><?php _e( 'Shipping', 'rem' ); ?>:</span>
										<?php if ( ! isOneFreeShipping() ): ?>
											<span class="js-shipping method">{{shipping}}</span>
										<?php else: _e( 'free', 'rem' ); ?>
										<?php endif; ?>
									</div>
									<div class="details">{{details}}</div>
								</div>
								<div class="col-sm-14 col-lg-12">
									<div class="text-center">
										<div class="s_q js-s_q">
											<button class="_btn js-quantity_remove" data-quantity="remove">
												<span>-</span></button>
											<input data-modalAddOrder="quantity" name="quantity" type="text" value="{{quantity}}" min="1" max="9999" maxlength="5"
											       autocomplete="off">
											<button class="_btn add js-quantity_add" data-quantity="add"><span>+</span>
											</button>
										</div>
									</div>
								</div>
								<div class="col-sm-10 col-lg-10">
									<div class="text-center">
										<div>
											<span class="price" data-modalAddOrder="total_salePrice">{{total_salePrice}}</span>
										</div>
										<div>
											<strike class="price__old" data-modalAddOrder="total_price">{{total_price}}</strike>
										</div>
									</div>
								</div>
								<div class="col-sm-1">
									<span class="remove-item js-remove-item" data-productActions="remove"><i aria-hidden="true"></i></span>
								</div>
							</div>
							<!---->
							<div class="visible-xs row mobile">
								<div class="col-xs-16 text-center">
									<div class="im-wrap">
										<span class="remove-item js-remove-item" data-productActions="remove"><i aria-hidden="true"></i></span>
										<div class="thumb-wrap">
											<div class="thumb"><img src="{{thumb}}" class="img-responsive"></div>
										</div>
									</div>
								</div>
								<div class="col-xs-24">
									<a href="{{link}}" class="title-link">{{title}}</a>
								</div>
								<div class="col-xs-20">
									<div class="">
										<div class="s_q js-s_q">
											<button class="_btn js-quantity_remove"><span>-</span></button>
											<input data-modalAddOrder="quantity" name="quantity" type="text" value="{{quantity}}" min="1" max="9999" maxlength="5"
											       autocomplete="off">
											<button class="_btn add js-quantity_add"><span>+</span></button>
										</div>
									</div>
									<div class="text-center">
										<div>
											<span class="price js-total_salePrice" data-modalAddOrder="total_salePrice">{{total_salePrice}}</span>
										</div>
										<div>
											<strike class="price__old js-total_price" data-modalAddOrder="total_price">{{total_price}}</strike>
										</div>
									</div>
								</div>
								<div class="col-xs-60">
									<div class="wrap-meta">
										<?php if ( ! isOneFreeShipping() ): ?>
											<div class="shipping">
												<span class="head"><?php _e( 'Shipping', 'rem' ); ?>:</span>
												<span class="js-shipping method">{{shipping}}</span>
											</div>
										<?php endif; ?>
										<div class="details">{{details}}</div>
									</div>
								</div>
							</div>


						</div>
					</script>
					<script id="modal_details_img_template" type="text/template">
						<span class="img"><img src="{{url}}" class="img-responsive"></span>
					</script>
					<script id="modal_details_text_template" type="text/template">
						<span class="text">{{text}}</span>
					</script>
					<script id="modal_details_template" type="text/template">
						<div class="sku-item"><span>{{title}}</span>:{{variation}}</div>
					</script>
				</div>
			</div>
			<div class="total">
				<div class="count">
					<span class="name"><?php _e( 'Number of products', 'rem' ); ?>:</span>
					<span class="value" data-modalAddOrder="total_count"></span>
				</div>
				<div class="price">
					<span class="name"><?php _e( 'Total price', 'rem' ); ?>:</span>
					<span class="value" data-modaladdorder="cur_salePriceFree"></span>
				</div>
			</div>
			<div class="modal-footer">
				<div class="wrap">
					<div class="continue">
						<span>&#8249;</span> <a data-dismiss="modal">
							<?php _e( 'Continue shopping', 'rem' ) ?>
						</a>
					</div>
					<a href="<?php echo esc_url( home_url( '/cart/' ) ) ?>" id="proceed_to_checkout" class="btn" onclick="ga('send', 'event', 'Proceed to Checkout', 'Click')"><?php _e( 'Proceed to Checkout', 'rem' ) ?>
					</a>
				</div>


			</div>
		</div>
	</div>
</div>
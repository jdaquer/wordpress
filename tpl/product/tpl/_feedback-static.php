<div class="row feedback-static">

	<div class="col-xs-60 col-md-30">
		<div class="row" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">

			<div class="text-center l-star">
				<div class="stars stars-big">
					<?php $reviews->renderStarRating( $reviews->averageStar() ); ?>
				</div>
			</div>

			<div class="text-center star-rating">
				<div property="aggregateRating" typeof="AggregateRating">
					<?php
					printf( '<div class="text">%3$s:</div>
							<div class="info"><span itemprop="ratingValue">%1$s</span> out of <span itemprop="bestRating">5</span> ( <span itemprop="reviewCount">%2$s</span> %4$s)</div>',
						$reviews->averageStar(),
						$reviews->countFeedback(),
						__( 'Average Star Rating', 'rem' ),
						__( 'Ratings', 'rem' )
					); ?>
				</div><!-- .star-rating -->
			</div>

		</div>
	</div>

	<div class="col-xs-60 col-md-30">
		<div class="info-text"><?php _e( 'Feedback Rating for This Product', 'rem' ); ?></div>
		<table class="feedback-rating">
			<?php
			$stars = $reviews->getStat();

			$stars[ 'stars' ] = array_reverse( $stars[ 'stars' ], true );

			$row = array(
				5 => sprintf( '<td class="col1" rowspan="2">%2$s <span>(%1$s&#37;)</span></td>',
					$stars[ 'positive' ],
					__( 'Positive', 'rem' )
				),
				3 => sprintf( '<td class="col1">%2$s <span>(%1$s&#37;)</span></td>',
					$stars[ 'neutral' ],
					__( 'Neutral', 'rem' )
				),

				2 => sprintf( '<td class="col1" rowspan="2">%2$s <span>(%1$s&#37;)</span></td>',
					$stars[ 'negative' ],
					__( 'Negative', 'rem' )
				)
			);

			foreach ( $stars[ 'stars' ] as $key => $value ) {

				printf( '<tr class="%6$s">
							%5$s
							<td class="col2"><div class="star-info"><span>%3$d %4$s</span> <span class="pr">(%1$d)</span></div></td>
							<td class="col3">
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="%2$d" aria-valuemin="0" aria-valuemax="100" style="width: %2$s%%;">
									<span class="sr-only">%2$s%%</span>
								</div>
							</div>
							</td>
						</tr>',
					$value[ 'count' ],
					$value[ 'percent' ],
					$key,
					$key == 1 ? __( 'Star', 'rem' ) : __( 'Stars', 'rem' ),
					isset( $row[ $key ] ) ? $row[ $key ] : '',
					'row-' . $key
				);
			}; ?>
		</table>
	</div>
</div>


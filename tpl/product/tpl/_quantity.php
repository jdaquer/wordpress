<?php
/**
 * Created by PhpStorm.
 * User: sunfun
 * Date: 30.08.16
 * Time: 22:14
 */
?>
<div class="product-quantity js-wrap_quantity">
<?php printf( '<div class="input-group input-group-sm input-group-quantity" data-quantity>
			<span class="input-group-btn">
				<button class="btn btn-minus" type="button" data-quantity="remove">&#45;</button>
			</span>
			<input name="quantity" type="text" class="form-control" value="1" min="1" max="9999" maxlength="5" autocomplete="off" placeholder="1" data-oldprice="%1$s" data-price="%2$s">
			<span class="input-group-btn">
				<button class="btn btn-plus" type="button" data-quantity="add">&#43;</button>
			</span>
		</div>',
	$product['price'],
	$product['salePrice']
);
?>
</div>
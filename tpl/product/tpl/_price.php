<?php
/**
 * Created by PhpStorm.
 * User: sunfun
 * Date: 07.09.16
 * Time: 23:43
 */
?>
<div class="product-price" itemprop="offers" itemscope>
    <meta itemprop="price" content="<?php echo $product['_salePrice']; ?>" />
	<meta itemprop="priceCurrency" content="<?php echo $product[ 'currency' ]; ?>" />
	<div class="price" data-productPrice="price" style="display:none;">
		<span class="name"><?php _e( 'List price', 'rem' ); ?>:</span>
		<span class="value" data-singleProduct="price"></span>
	</div>

	<div class="salePrice" data-productPrice="salePrice" style="display:none;">
		<span class="name"><?php _e( 'Price', 'rem' ); ?>:</span>
		<span class="value" data-singleProduct="savePrice"></span>
	</div>

	<div class="youSave" data-productPrice="savePercent" style="display:none;">
		<span class="name"><?php _e( 'You save', 'rem' ); ?>:</span><span class="save" data-singleProduct="save"></span>
		<span class="savePercent">(<span data-singleProduct="savePercent"></span>%)</span>
	</div>

	<?php if( $product[ 'quantity' ] ) : ?>
		<div class="stock" data-productPrice="quantity" <?php echo $product[ 'quantity' ] > 15 ? 'style="display:none;"' : '' ?> itemprop="availability" href="http://schema.org/InStock"><?php _e( 'Only', 'rem' ); ?>
			<span data-singleProduct="quantity"><?php echo $product[ 'quantity' ]; ?></span> <?php _e( 'left in stock', 'rem' ); ?>
		</div>
	<?php else: ?>
		<div class="stock" data-productPrice="quantity" itemprop="availability" href="http://schema.org/InStock">
			<?php _e( 'Out of stock', 'rem' ); ?>
		</div>
	<?php endif; ?>

</div>

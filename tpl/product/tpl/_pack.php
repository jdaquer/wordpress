
<?php
global $product;
$pack = $product['pack'];

if (cz('tp_pack') && $pack ) : ?>
	<h2><?php _e( 'Packaging Details', 'rem' ) ?></h2>
	<div class="row ">
		<div class="col-xs-20 text-right">
			<b><?php _e( 'Type', 'rem' ) ?>:</b>&nbsp;
		</div>
		<div class="col-xs-40">&nbsp;<?php echo $pack[ 'type' ] ?></div>
	</div>
	<div class="row ">
		<div class="col-xs-20 text-right">
			<b><?php _e( 'Weight', 'rem' ) ?>:</b>&nbsp;
		</div>
		<div class="col-xs-40"><?php echo $pack[ 'weight' ] ?></div>
	</div>
	<div class="row">
		<div class="col-xs-20 text-right">
			<b><?php _e( 'Size', 'rem' ) ?>:</b>&nbsp;
		</div>
		<div class="col-xs-40"><?php echo $pack[ 'size' ];?></div>
	</div>
<?php endif; ?>
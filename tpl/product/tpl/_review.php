<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 03.06.2016
 * Time: 15:11
 */
global $reviews;
$posts_per_page = ( isset( $wp_query->query_vars[ 'comments_per_page' ] ) &&
                    intval( $wp_query->query_vars[ 'comments_per_page' ] ) ) ?
	$wp_query->query_vars[ 'comments_per_page' ] :
	intval( get_option( 'comments_per_page' ) );

?>

<?php if ( comments_open() ): ?>

	<div class="inner-box">
		<div class="controls">
			<div class="col-xs-60">
				<table class="review_list">
					<thead>
					<tr>
						<th><?php _e( 'Buyer', 'rem' ); ?></th>
						<th><?php _e( 'Rating', 'rem' ); ?></th>
						<th class="feedback"><?php _e( 'Feedback', 'rem' ); ?></th>
					</tr>
					<tr class="th">
						<td colspan="3"></td>
					</tr>
					</thead>
					<?php

					$args = array(
						'walker'            => null,
						'max_depth'         => '',
						'style'             => 'tr',
						'callback'          => 'list_review',
						'end-callback'      => null,
						'type'              => 'all',
						'reply_text'        => 'Reply',
						'page'              => 1,
						'per_page'          => $posts_per_page,
						'avatar_size'       => 32,
						'reverse_top_level' => null,
						'reverse_children'  => '',
						'format'            => 'html5',
						'echo'              => true,
						'status'            => 'approve'
					);

					wp_list_comments( $args, $reviews->comments );
					?>
					<tr class="th">
						<td colspan="3"></td>
					</tr>
				</table>
			</div>
		</div>

	</div>
	<?php if(get_comment_pages_count()>1): ?>
	<div class="inner-box-comment-form">
		<div class="wrap-pagination">
			<div class="pagination">
				<?php
				paginate_comments_links(
                    array(
                        'prev_text' => '&laquo;',
                        'next_text' => '&raquo;',
                        'current'   => $reviews->getPage()
                    )
                );
				?>
			</div>
		</div>
		<?php
		//TODO возможность отставлять коментарии только купившим
		//rem_comment_form_add(); ?>
	</div>
	<?php endif; ?>
<?php endif; ?>


<div class="tab-wrap">
	<div class="tab-body">
		<?php if ( $product['attrib'] ) : ?>
			<?php
			foreach ( $product['attrib'] as $k => $attr ) {

				printf(
					'<div class="row attr">
                                                <div class="col-xs-20 text-right"><strong>%1$s:</strong>&nbsp;</div>
                                                <div class="col-xs-40">&nbsp;%2$s</div></div>',
					$attr[ 'name' ],
					$attr[ 'value' ]
				);
			}
			?>
		<?php endif; ?>
	</div>
</div>
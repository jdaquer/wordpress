<div class="tab-wrap">
	<div class="tab-body" itemprop="description" itemtype="http://schema.org/Product">
		<div class="content">
			<?php the_content(); ?>
		</div>
	</div>
</div>
<div class="md-feedback hidden-sm hidden-xs">
	<?php get_template_part( 'tpl/widget/_features' ); ?>
</div>

<div class="md-feedback hidden-sm hidden-xs">
	<?php get_template_part( 'tpl/product/tpl/_feedback' ) ?>
</div>

<?php global $reviews; ?>
	<?php if($reviews->countFeedback() > 0 && comments_open()):?>
		<?php printf('<div id="feedback-head" class="feedback-head">%1$s <span>(%2$s)</span></div>',
			__( 'Feedback', 'rem' ),
			$reviews->countFeedback()
		);  ?>
		<?php if ( file_exists( dirname( __FILE__ ) . '/_feedback-static.php' ) ) {
			include dirname( __FILE__ ) . '/_feedback-static.php';
		} ?>
		<div class="row">
			<?php comments_template( '/tpl/product/tpl/_review.php' ); ?>
		</div>
        <?php if (class_exists('models\review\RenderForm')):?> 
            <?php echo \models\review\RenderForm::showReview();?>
        <?php endif;?>
	<?php endif; ?>

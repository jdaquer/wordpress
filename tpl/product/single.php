<?php
/**
 * Created by PhpStorm.
 * User: sunfun
 * Date: 19.08.16
 * Time: 9:35
 */
?>
<?php
global $reviews;
$reviews = new adsFeedBackTM( $post->ID );

$info = new adsProductTM( array(
	'attributes' => true,
	'alimeta'    => true
) );
$info->setData( $post );

global $product;
$product = $info->singleProduct();

?>
<!-- BREADCRUMBS -->
<div class="breadcrumbs">
	<div class="container">
		<?php adstm_breadcrumbs() ?>
	</div>
</div>

<?php while ( have_posts() ) : the_post(); ?>

	<div class="product-main">
		<div class="container">
			<div class="row">
				<div class="col-lg-28">
					<div class="js-sticky">
						<?php rem_theGallery( $product[ 'gallery' ] ) ?>
						<?php if ( cz( 'tp_share' ) ): ?>
							<div class="share1">
								<?php get_template_part( 'tpl/widget/_share_button' ); ?>
							</div>
						<?php endif; ?>
					</div>

				</div>
				<div class="col-lg-32">
					<div class="product-info js-sticky-main">
						<h1 class="head" itemprop="name"><?php the_title() ?></h1>
						<?php if ( cz( 'tp_share' ) ): ?>
							<div class="share2">
								<?php get_template_part( 'tpl/widget/_share_button' ); ?>
							</div>
						<?php endif; ?>
						<div class="product-rate">
							<?php if ( $product[ 'rate' ] > 0 ):
								echo $info->starRating() ?>
								<span><?php echo $info->ratingPercentage() ?>%</span>
								<?php _e( 'of buyers enjoyed this product!', 'rem' ) ?>
							<?php endif; ?>
							<?php if ( $product[ 'promotionVolume' ] > 0 && cz( 'tp_show_quantity_orders' ) ): ?>
								<span class="orders">
							<?php printf( _n( '%s order', '%s orders', $product[ 'promotionVolume' ], 'rem' ), $product[ 'promotionVolume' ] ) ?>
						</span>
							<?php endif; ?>
						</div>

						<?php if ( file_exists( dirname( __FILE__ ) . '/tpl/_price.php' ) ) {
							include dirname( __FILE__ ) . '/tpl/_price.php';
						} ?>

						<?php if( $product['quantity'] != 0 ) : ?>
						<form method="POST" id="form_singleProduct" class="form-SingleProduct">
							<input type="hidden" name="post_id" value="<?php echo $post->ID ?>">
							<?php echo $info->getHiddenFiends(); ?>
							<div class="product-meta">
								<table class="meta">
									<tr class="m-row">
										<td class="name"><?php _e( 'Quantity', 'rem' ); ?>:</td>
										<td class="value">
											<?php if ( file_exists( dirname( __FILE__ ) . '/tpl/_quantity.php' ) ) {
												include dirname( __FILE__ ) . '/tpl/_quantity.php';
											} ?>
										</td>
									</tr>
									<?php
									if ( ! empty( $product[ 'sku' ] ) && $product[ 'skuAttr' ]  ) {
										rem_showSKU( $product[ 'sku' ], $product[ 'skuAttr' ], get_the_title() );
									}
									?>
									<?php
									$style = '';
									if ( ! cz( 'tp_shipping_details' ) ) {
										$style = 'style="display:none;"';
									} ?>
									<tr class="m-row" <?php echo $style; ?>>
										<td class="name"><?php _e( 'Shipping', 'rem' ); ?>:</td>
										<td class="value">
											<div class="product-shipping">
												<?php echo $product[ 'renderShipping' ]; ?>
											</div>
										</td>
									</tr>
								</table>
							</div>
						</form>
						<div class="product-actions">
							<button onclick="ga('send', 'event', 'Add to Cart', 'Click')" class="btn addCart" data-singleProduct="addCart" type="button">
								<span><?php _e( 'Add to Cart', 'rem' ); ?></span>
							</button>
							<?php if ( adstm_isExpressCheckoutEnabled() ): ?>
								<button type="submit" form="form_singleProduct" name="pay_express_checkout" class="btn checkPayPal" type="button">
									<span><?php _e( 'Check out with', 'rem' ); ?></span>
								</button>
							<?php endif; ?>

						</div>

						<?php endif; ?>

					</div>

				</div>
			</div>
		</div>
	</div>

	<div class="product-content">
		<div class="container">
			<div class="row">

				<div class="col-sm-60">
					<!-- Nav tabs -->
					<ul role="tablist" id="tabs" class="nav nav-tabs">
						<li role="presentation" class="active">
							<a href="#details" data-scroll="details" aria-controls="home" role="tab" data-toggle="tab"><?php _e( 'Product Details', 'rem' ) ?></a>
						</li>
						<?php if ( cz( 'tp_tab_item_review' ) && comments_open() ): ?>
						<li role="presentation" class="">
							<a href="#details" data-scroll="feedback-head" aria-controls="home" role="tab" data-toggle="tab"><?php _e( 'Reviews', 'rem' ) ?> (<?php echo $reviews->countFeedback(); ?>)</a>
						</li>
						<?php endif; ?>
						<?php if ( cz( 'tp_tab_item_specifics' ) ): ?>
							<li role="presentation" class="">
								<a href="#specifics" aria-controls="specifics" role="tab" data-toggle="tab"><?php _e( 'Item Specifics', 'rem' ) ?></a>
							</li>
						<?php endif; ?>
						<?php if ( cz( 'tp_tab_shipping' ) ): ?>
						<li role="presentation" class="">
							<a href="#shipping" aria-controls="shipping" role="tab" data-toggle="tab"><?php _e( 'Shipping &#38; Payment', 'rem' ) ?></a>
						</li>
						<?php endif; ?>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">

						<div role="tabpanel" class="tab-pane tab-details active" id="details">
							<?php if ( file_exists( dirname( __FILE__ ) . '/tpl/_details.php' ) ) {
								include dirname( __FILE__ ) . '/tpl/_details.php';
							} ?>
						</div>
						<?php if ( cz( 'tp_tab_item_specifics' ) ): ?>
							<div role="tabpanel" class="tab-pane tab-specifics" id="specifics">
								<?php if ( file_exists( dirname( __FILE__ ) . '/tpl/_specifics.php' ) ) {
									include dirname( __FILE__ ) . '/tpl/_specifics.php';
								} ?>
							</div>
						<?php endif; ?>
						<?php if ( cz( 'tp_tab_shipping' ) ): ?>
						<div role="tabpanel" class="tab-pane tab-shipping" id="shipping">
							<?php if ( file_exists( dirname( __FILE__ ) . '/tpl/_shipping.php' ) ) {
								include dirname( __FILE__ ) . '/tpl/_shipping.php';
							} ?>
						</div>
						<?php endif; ?>
					</div>

				</div><!-- .col-sm-60 -->
			</div><!-- .row -->
		</div>

		<div class="container">
			<div class="row visible-sm visible-xs">
				<div class="col-xs-60 single-feedback">
					<?php get_template_part( 'tpl/widget/_features' ); ?>
				</div>
			</div>
		</div>

		<div class="container">
			<div id="feedback-mobile" class="row visible-sm visible-xs">
				<div class="col-xs-60 sm-feedback">
					<?php get_template_part( 'tpl/product/tpl/_feedback' ) ?>
				</div>
			</div>
		</div>
	</div>


<?php endwhile; ?>

<?php get_template_part( 'tpl/product/tpl/_recently' ) ?>
<?php get_template_part( 'tpl/product/tpl/_modal-addCart' ) ?>

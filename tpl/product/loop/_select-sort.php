<?php

function sortby_show(
	$ico = array(
		'desc' => 'ic icon-desc',
		'asc'  => 'ic icon-asc',
	)
) {

	$orderby = function_exists('ads_get_orderby_param') ? ads_get_orderby_param() : '';
	$order   = function_exists('ads_get_order_param') ? ads_get_order_param() : '';

	$s = isset( $_GET[ 's' ] ) ? '&s=' . $_GET[ 's' ] : '';

	$list = function_exists('ads_sortby_list') ? ads_sortby_list() : array();

	foreach ( $list as $key => $val ) {

		$selected = ( $key == $orderby ) ? 'selected="selected"' : "";

		if ( is_array( $val[ 'order' ] ) ) {

			printf(
				'<option value="?orderby=%1$s&order=%2$s%3$s" %4$s>%5$s, %6$s</option>',
				$key,
				'desc',
				$s,
				( $key == $orderby && 'desc' == $order  ) ? 'selected="selected"' : "",
				$val[ 'title' ],
				__('high to low')
			);

			printf(
				'<option value="?orderby=%1$s&order=%2$s%3$s" %4$s>%5$s, %6$s</option>',
				$key,
				'asc',
				$s,
				( $key == $orderby && 'asc' == $order ) ? 'selected="selected"' : "",
				$val[ 'title' ],
				__('low to high')
			);
		} else {
			printf(
				'<option value="?orderby=%1$s%2$s" %3$s>%4$s</option>',
				$key, $s, $selected, $val[ 'title' ]
			);
		}
	}
}

?>

<div class="sort">
		<?php echo '<select class="js-select_sort">'; ?>
		<?php sortby_show(); ?>
		<?php echo '</select>'; ?>
</div>

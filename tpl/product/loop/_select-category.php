<?php
function category_select_show() {
	$post_slug = get_query_var( 'product_cat', 'product' );

	$select = wp_dropdown_categories( array(
		'show_option_all' => __( 'All Categories', 'rem' ),
		'taxonomy'        => 'product_cat',
		'show_count'      => 0,
		'hide_empty'      => 1,
		'selected'        => 0,
		'echo'            => 0,
		'orderby'         => 'name',
		'order'           => 'ASC',
		'name'            => 'product_cat',
		'value_field'     => 'slug',
		'depth'           => '10',
		'hierarchical'    => true
	) );

	$select = preg_replace( '#value=["\']0["\']#', 'value="product"', $select );
	$select = preg_replace( '#value="' . $post_slug . '"#', "$1 selected=\"selected\"", $select );
	$select = preg_replace( "#<select([^>]*)>#", "<select$1 onchange='return this.form.submit()'>", $select );

	echo $select;
}

?>

<div class="category-select">
	<form action="<?php bloginfo( 'url' ); ?>/" method="get">
		<div>
			<label class=""><?php _e( 'Category', 'rem' ); ?>:</label>
			<?php category_select_show(); ?>

			<script type="text/javascript">
				var dropdown = document.getElementById( "product_cat" );
				function onCatChange() {
					console.log( dropdown.options[ dropdown.selectedIndex ].value );

					if ( dropdown.options[ dropdown.selectedIndex ].value != -1 ) {
						location.href = "<?php echo get_option( 'home' );?>/" + dropdown.options[ dropdown.selectedIndex ].value;
					}
				}
				dropdown.onchange = onCatChange;
			</script>
		</div>
	</form>

</div>
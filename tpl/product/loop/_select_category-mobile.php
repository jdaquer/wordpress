<div class="wrap-select-list">
	<?php
	$post_slug          = get_query_var( 'product_cat', 'product' );
	$categories         = get_categories( 'taxonomy=product_cat' );
	$select[ 'count' ]  = 0;
	$select[ 'all' ]    = 0;
	$select[ 'active' ] = __( 'All Products', 'rem' );
	foreach ( $categories as $category ) {
		if ( $category->count > 0 ) {
			if ( $category->slug == $post_slug ) {
				$select[ 'active' ] = $category->name;
				$select[ 'count' ]  = $category->count;
			}
			$select[ 'all' ] += $category->count;
		}
	} ?>


	<?php
	printf( '<div class="head js-toggle">%1$s <span>(%2$s)</span></div>',
		$select[ 'active' ],
		$select[ 'count' ] ? $select[ 'count' ] : $select[ 'all' ]
	);
	echo "<div class='list'>";
	echo "<ul>";
	$product_cat_menu = wp_list_categories( array(
		'taxonomy'    => 'product_cat',
		'show_count'  => 1,
		'hide_empty'  => 1,
		'echo'        => 0,
		'title_li'    => '',
		'order'       => 'ASC',
		'orderby'     => 'name',
		'link_before' => '<span class="main-el-icon"></span>'
	) );
	$product_cat_menu = preg_replace( '@\<li([^>]*)>\<a([^>]*)>(.*?)<\/a>\s*(\(.*\))?@i', '<li$1><a$2><span class="main-el-icon"></span><span class="main-el-text">$3<span class="main-el-count">$4</span></span></a>', $product_cat_menu );
	echo $product_cat_menu;
	echo "</ul>";
	echo "</div>";
	?>
</div>

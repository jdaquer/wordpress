<div>
	<?php
	$class = cz( 'tp_menu_close' ) ? 'custom_close' : '';
	echo "<ul id='category-list' class='" . $class . "'>";
	$product_cat_menu = wp_list_categories( array(
		'taxonomy'    => 'product_cat',
		'show_count'  => 1,
		'hide_empty'  => 1,
		'echo'        => 0,
		'order'       => 'ASC',
		'orderby'     => 'name',
		'title_li'    => sprintf( '<h3>%s</h3>', __( 'All Products', 'rem' ) ),
		'link_before' => '<span class="main-el-icon"></span>'
	) );
	$product_cat_menu = preg_replace( '@\<li([^>]*)>\<a([^>]*)>(.*?)<\/a>\s*(\(.*\))?@i', '<li$1><a$2><span class="main-el-icon"></span><span class="main-el-text">$3<span class="main-el-count">$4</span></span></a>', $product_cat_menu );
	echo $product_cat_menu;
	echo "</ul>";
	?>
</div>
<div class="sort">
	<?php
	$orderby = function_exists( 'ads_get_orderby_param' ) ? ads_get_orderby_param() : '';
	$order   = function_exists( 'ads_get_order_param' ) ? ads_get_order_param() : '';

	$s = isset( $_GET[ 's' ] ) ? '&s=' . $_GET[ 's' ] : '';

	$list = function_exists( 'ads_sortby_list' ) ? ads_sortby_list() : array();

	$p        = '<ul class="list">';

	foreach ( $list as $key => $val ) {
		$selected = '';
		if ( $key == $orderby ) {
			$selected = 'current';
			$active   = $val[ 'title' ];
		}

		if ( is_array( $val[ 'order' ] ) ) {

			if ( $key == $orderby && 'desc' == $order ) {
				$active   = $val[ 'title' ] . ', ' . __( 'high to low' );
			}elseif($key == $orderby && 'asc' == $order){
				$active   = $val[ 'title' ] . ', ' . __( 'low to high' );
			}

			$p .= sprintf(
				'<li class="%4$s"><a href="?orderby=%1$s&order=%2$s%3$s" %4$s>%5$s, %6$s</option>',
				$key,
				'desc',
				$s,
				( $key == $orderby && 'desc' == $order ) ? 'current' : "",
				$val[ 'title' ],
				__( 'high to low' )
			);

			$p .= sprintf(
				'<li class="%4$s"><a href="?orderby=%1$s&order=%2$s%3$s">%5$s, %6$s</option>',
				$key,
				'asc',
				$s,
				( $key == $orderby && 'asc' == $order ) ? 'current' : "",
				$val[ 'title' ],
				__( 'low to high' )
			);
		} else {
			$p .= sprintf(
				'<li class="%3$s"><a href="?orderby=%1$s%2$s">%4$s</a></li>',
				$key, $s, $selected, $val[ 'title' ]
			);
		}
	}
	$p .= '</ul>';
	printf( '<div class="head js-toggle">%1$s</div>',
		$active
	);

	echo $p;
	?>
</div>
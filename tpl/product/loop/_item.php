<?php
global $post;
$info = new adsProductTM( array(
	'attributes' => true,
	'alimeta'    => true
) );
$info->setData( $post );

$product = $info->singleProductMin();

?>

<div class="product-item" data-post_id="<?php echo intval($post->ID); ?>" data-currency="<?php echo $product[ 'currency' ]; ?>" data-price="<?php echo $product[ '_price_nc' ]; ?>" data-saleprice="<?php echo $product[ '_salePrice_nc' ]; ?>">
	<a href="<?php the_permalink() ?>">
		<div class="wrap-img">
			<div class="img_content">
				<img src="<?php echo $product[ 'thumb' ]; ?>" alt="">
			</div>
			<?php if ( $product[ 'discount' ] && cz('tp_show_discount')) {
				printf( '<div class="discount"><span>%1$d&percnt; %2$s</span></div>',
					$product[ 'discount' ],
					__('off', 'rem')
				);
			} ?>


		</div>
		<div class="title"><?php the_title() ?></div>
		<span class="starRating">
			<?php
			$promotionVolume = (cz('tp_show_quantity_orders') && $product['promotionVolume']) ? $product['promotionVolume'] .' '. __('orders', 'rem'): false;
			echo $info->starRating($promotionVolume);

			?>
		</span>
		<div class="price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
			<meta itemprop="price" content="<?php echo $product[ '_price_nc' ]; ?>">
			<meta itemprop="priceCurrency" content="<?php echo $product[ 'currency' ]; ?>"/>
			<?php if ( $product[ '_price' ] > 0 && $product[ '_price' ] !== $product[ '_salePrice' ]): ?>
				<div class="old js-price"></div>
			<?php endif; ?>
			<div class="sale js-salePrice"></div>
		</div>
	</a>
</div>
 
<?php
$title = is_post_type_archive() ? __( 'All products', 'rem' ) : adstm_single_term();
?>

<div class="container wrap-loop">
	<div class="row">

			<?php
/*			<div class="col-xs-15 hidden-xs hidden-sm">
			get_template_part( 'tpl/product/loop/_list', 'category' );
			</div>*/
			?>

		<div class="col-xs-60 col-md-60">
			<div class="category-header hidden-xs hidden-sm">
				<div class="wrap-head">
					<h1 class="title col-xs-60"><?php echo esc_attr( $title ); ?></h1>
				</div>
				<div class="select">
					<?php get_template_part( 'tpl/product/loop/_select', 'sort' ); ?>
				</div>
			</div>

			<div class="category-header-mobile visible-xs visible-sm">
				<?php get_template_part( 'tpl/product/loop/_select_category', 'mobile' ); ?>
				<?php get_template_part( 'tpl/product/loop/_select_sort', 'mobile' ); ?>
			</div>

			<div class="list-product product-loop">
				<div class="clearfix">
					<div class="row">
						<?php while ( have_posts() ) : the_post(); ?>
							<div class="item col-xs-30 col-md-15">
								<?php get_template_part( 'tpl/product/loop/_item' ); ?>
							</div>
						<?php endwhile; ?>
					</div>
				</div>

			</div>
		</div>
	</div>
	<?php custom_pagination(); ?>
</div>

<!-- CONTENT -->
<div class="container">
	<div class="row">
		<div class="col-lg-60 b-content">
			<?php $queried_object = get_queried_object();
			echo $queried_object->description; ?>
		</div>
	</div>
</div>

<div class="container hidden-xs hidden-sm">
	<div class="row">
		<?php get_template_part( 'tpl/widget/_features' ); ?>
	</div>
</div>


<?php
$locations = get_nav_menu_locations();

if(isset($locations['сategory'])){

	wp_nav_menu(
		array(
			'menu'           => 'Category menu',
			'container'      => 'сategory',
			'theme_location' => 'сategory',
			'menu_class'     => '',
			'items_wrap'     => '<ul class="categories-menu clearfix">%3$s</ul>'
		)
	);
}else {
	$menuProduct = wp_list_categories(
		array(
			'child_of'            => 0,
			'current_category'    => 0,
			'depth'               => 0,
			'echo'                => 0,
			'exclude'             => '',
			'exclude_tree'        => '',
			'feed'                => '',
			'feed_image'          => '',
			'feed_type'           => '',
			'hide_empty'          => 1,
			'hide_title_if_empty' => false,
			'hierarchical'        => true,
			'order'               => 'ASC',
			'orderby'             => 'name',
			'separator'           => '<br />',
			'show_count'          => 1,
			'show_option_all'     => '',
			'show_option_none'    => '',
			'style'               => 'list',
			'taxonomy'            => 'product_cat',
			'title_li'            => '',
			'use_desc_for_title'  => 1,
		)
	);
	$menuProduct = preg_replace( "#<a([^>]*)>([^>]*)<\/a>\s*\(([^>]*)\)#", "<a$1><span>$2</span><span class=\"count\">($3)</span></a>", $menuProduct );
	printf('<ul class="categories-menu clearfix">%1$s</ul>', $menuProduct);
}

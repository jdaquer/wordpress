<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 10.08.2015
 * Time: 16:43
 */
?>
<?php get_header() ?>

<!-- CONTACT-US -->
<div class="page-contact">
	<div class="container">
		<div class="row">
			<div class="col-md-60">
				<div class="p-heading-page hidden-xs hidden-sm">
					<div class="p-title"><?php the_title(); ?></div>
				</div>
				<div class="p-heading visible-xs visible-sm">
					<div class="p-title"><?php the_title(); ?></div>
				</div>
				<div class="row">
					<?php  $col =''; if ( cz( 'tp_contactUs_map' ) ) {
					    $col = 'col-md-offset-1 pull-right';
					} ?>
                    <div class="col-xs-60 col-xs-offset-0 col-md-25 <?php echo $col;?>">
						<div class="info-text"><?php echo cz('tp_contactUs_text'); ?></div>
						<div class="info-mail">
							<a href="mailto:<?php echo cz( 's_mail' ); ?>" class="email"><?php echo cz( 's_mail' ); ?></a>
						</div>
						<?php if(false): ?>
							<ul class="list-social">
								<?php if ( cz( 's_link_fb' ) ): ?>
									<li>
										<a href="<?php echo cz( 's_link_fb' ); ?>" class="ico-social fb" target="_blank" rel="nofollow"></a>
									</li>
								<?php endif; ?>
								<?php if ( cz( 's_link_in' ) ): ?>
									<li>
										<a href="<?php echo cz( 's_link_in' ); ?>" class="ico-social in" target="_blank" rel="nofollow"></a>
									</li>
								<?php endif; ?>
								<?php if ( cz( 's_link_tw' ) ): ?>
									<li>
										<a href="<?php echo cz( 's_link_tw' ); ?>" class="ico-social tw" target="_blank" rel="nofollow"></a>
									</li>
								<?php endif; ?>
								<?php if ( cz( 's_link_gl' ) ): ?>
									<li>
										<a href="<?php echo cz( 's_link_gl' ); ?>" class="ico-social gp" target="_blank" rel="nofollow"></a>
									</li>
								<?php endif; ?>
								<?php if ( cz( 's_link_pt' ) ): ?>
									<li>
										<a href="<?php echo cz( 's_link_pt' ); ?>" class="ico-social pn" target="_blank" rel="nofollow"></a>
									</li>
								<?php endif; ?>
								<?php if ( cz( 's_link_yt' ) ): ?>
									<li>
										<a href="<?php echo cz( 's_link_yt' ); ?>" class="ico-social yt" target="_blank" rel="nofollow"></a>
									</li>
								<?php endif; ?>
							</ul>
						<?php endif; ?>
						<form class="form-horizontal" method="post" data-validate>

							<div class="form-group form-group-sm">
								<div class="col-sm-60">
									<input type="text" name="nameClient" class="form-control" placeholder="*<?php _e( 'Your name', 'rem' ); ?>" required>
								</div>
							</div>

							<div class="form-group form-group-sm">
								<div class="col-sm-60">
									<input type="email" name="email" class="form-control" placeholder="*<?php _e( 'Email', 'rem' ); ?>" required>
								</div>
							</div>

							<div class="form-group form-group-sm">
								<div class="col-sm-60">
									<textarea class="form-control" name="message" rows="5" placeholder="*<?php _e( 'Your message', 'rem' ); ?>" required></textarea>
								</div>
							</div>

							<div class="form-group form-group-sm">
								<div class="col-sm-60">
									<?php printf( '<button type="submit" name="contactSender" class="btn btn-bord">%s</button>', __( 'Send message', 'rem' ) ) ?>
								</div>
							</div>

						</form>
					</div>
					<?php if(cz('tp_contactUs_map')): ?>
                    <div class="col-xs-60 col-md-34 pull-left">
						<div class="map">
							<img class="img-responsive" src="<?php echo cz('tp_contactUs_map');?>" alt="">
						</div>
					</div>
					<?php endif; ?>
                </div>
			</div>

		</div>
	</div>
	<?php get_footer() ?>

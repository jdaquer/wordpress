<?php
global $adsBasket;
$message  = isset( $_POST[ 'ads-error' ] ) ? $_POST[ 'ads-error' ][ 'message' ] : false;
$gateways = rem_listGateway();

$fields = array(
	'email'        => '',
	'full_name'    => '',
	'address'      => '',
	'city'         => '',
	'country'      => '',
	'state'        => '',
	'postal_code'  => '',
	'phone_number' => '',
	'description'  => '',
	'type'         => ''
);

foreach ( $fields as $k => $v ) {
	$fields[ $k ] = isset( $_POST[ $k ] ) ? esc_attr( $_POST[ $k ] ) : '';
}

?>
<?php get_header() ?>
	<!-- CHECKOUT -->
	<div class="page-checkout">
		<div class="container">
			<div class="row wrap-checkout">
				<div class="col-xs-60 col-md-30 pull-right box-right">
					<div class="bg"></div>
					<div class="row row-list">
						<div class="col-xs-60 head-list">
							<div class="col-xs-40"><i class="fa fa-shopping-cart" aria-hidden="true"></i>
								<span class="text"><?php _e( 'Show order summary', 'rem' ); ?></span>
								<span class="icon-stage">
                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                                <i class="fa fa-angle-up" aria-hidden="true"></i></span>
							</div>
							<div class="col-xs-20 total_price">
								<div class="js-cur_salePrice"></div>
							</div>
						</div>
						<div class="col-xs-60 page-cart-list js-page-cart-list" data-open="head-list">

							<script id="page_cart_item_template" type="text/template">
								<div class="row cart-item" data-key="{{order_id}}" data-post="{{post_id}}">

									<div class="col-xs-60 col-sm-40">
										<div class="col-xs-20 text-center thumb-wrap">
											<span class="remove-item" data-productActions="remove"><i class="fa fa-fw" aria-hidden="true"></i></span>
											<div class="thumb"><img src="{{thumb}}" class="img-responsive"></div>
										</div>
										<div class="col-xs-40">
											<a href="{{link}}" class="title-link">{{title}}</a>
										</div>
									</div>
									<div class="col-xs-20 col-sm-20 col-xs-push-40 col-sm-push-0">
										<div class="text-center">
											<div><strike class="price__old js-total_price">{{total_price}}</strike></div>
											<div><span class="price js-total_salePrice">{{total_salePrice}}</span></div>
										</div>
										<div class="text-center">
											<div class="select_quantity js-wrap_quantity">
												<button class="btn select_quantity__btn" data-quantity="remove">
													<i class="ico--remove">-</i></button>
												<input name="quantity" type="text" value="{{quantity}}" min="1" max="9999" maxlength="5"
												       autocomplete="off">
												<button class="btn select_quantity__btn select_quantity__btn--add" data-quantity="add">
													<i class="ico--add">+</i></button>
											</div>
										</div>
									</div>
									<div class="col-xs-40 col-xs-pull-20 col-sm-pull-0 col-sm-60">
										<div class="info-product">
											<div class="details">{{details}}</div>
											<?php if ( ! isOneFreeShipping() ): ?>
												<div class="shipping">
													<span class="name"><?php _e( 'Shipping', 'rem' ); ?>:</span>
													<span class="js-shipping method">{{shipping}}</span>
												</div>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</script>
							<script id="page_cart_details_img_template" type="text/template">
								<img src="{{url}}" class="img-responsive">
							</script>
							<script id="page_cart_details_template" type="text/template">
								<span>{{title}}</span>:<span class="text">{{variation}}</span>
							</script>
						</div>
					</div>
					<?php if ( rem_isPromocodesEnabled() ): ?>
						<div class="discount" data-open="head-list">
							<div class="wrap">
								<div class="grup-input field b-coupon">
									<input form="form_delivery" id="discount" type="text" name="discount" placeholder="<?php _e( 'Gift card or discount code', 'rem' ) ?>">
									<label for="discount"><?php _e( 'Gift card or discount code', 'rem' ) ?></label>
									<div class="b-coupon__msg b-coupon__msg--err" style="display: none;"><?php _e( 'Code does not exist', 'rem' ) ?></div>
									<div class="b-coupon__msg b-coupon__msg--valid" style="display: none;"><?php _e( 'Code Accepted', 'rem' ) ?></div>
								</div>
								<button class="btn js-coupon__btn"><?php _e( 'Apply', 'rem' ) ?></button>
							</div>
						</div>
					<?php endif; ?>
					<ul class="price-list" data-open="head-list">
						<li class="js-page-total_price-box">
							<div class="name"><?php _e( 'List price', 'rem' ); ?>:</div>
							<div class="value"><span class="js-page-total_price"></span></div>
						</li>
						<li class="js-page-total_save-box">
							<div class="name"><?php _e( 'You save', 'rem' ); ?>:</div>
							<div class="value"><span class="js-page-total_save"></span></div>
						</li>
						<li>
							<div class="name"><?php _e( 'Shipping', 'rem' ); ?>:</div>
							<div class="value"><span class="js-total_shipping"></span></div>
						</li>
					</ul>
					<div class="total-price" data-open="head-list">
						<div class="name"><?php _e( 'Total price', 'rem' ); ?>:</div>
						<div class="value"><span class="js-cur_salePrice"></span></div>
					</div>
				</div>
				<div class="col-xs-60 col-md-30 pull-left box-left">
					<form id="form_delivery" class="form" action="" method="POST" novalidate="novalidate">
						<h3><?php _e( 'Customer information', 'rem' ); ?></h3>
						<div class="row">
							<div class="form-group col-xs-60 col-md-40">
								<div class="field">
									<input form="form_delivery" class="form-control" id="email" type="email" name="email" value="<?php echo( $fields[ 'email' ] ) ?>" placeholder="<?php _e( 'Email', 'rem' ) ?>" required="required">
									<label class="control-label" for="email"><?php _e( 'Email', 'rem' ) ?></label>
								</div>
							</div>
						</div>
						<h3><?php _e( 'Shipping address', 'rem' ); ?></h3>
						<div class="row">
							<div class="form-group col-xs-60">
								<div class="field">
									<input form="form_delivery" class="form-control" id="full_name" type="text" name="full_name" value="<?php echo( $fields[ 'full_name' ] ) ?>" placeholder="<?php _e( 'Full Name', 'rem' ) ?>" required="required">
									<label class="control-label" for="full_name"><?php _e( 'Full Name', 'rem' ) ?></label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-xs-60">
								<div class="field">
									<input form="form_delivery" class="form-control" id="address" type="text" name="address" value="<?php echo( $fields[ 'address' ] ) ?>" placeholder="<?php _e( 'Address', 'rem' ) ?>" required="required">
									<label class="control-label" for="address"><?php _e( 'Address', 'rem' ) ?></label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-xs-30">
								<div class="field">
									<input form="form_delivery" class="form-control" id="city" type="text" name="city" value="<?php echo( $fields[ 'city' ] ) ?>" placeholder="<?php _e( 'City', 'rem' ) ?>" required="required">
									<label class="control-label" for="city"><?php _e( 'City', 'rem' ) ?></label>
								</div>
							</div>
							<div class="form-group col-xs-30">
								<div class="field">
									<select name="country" id="country" class="form-control" required="required">
										<?php rem_get_list_contries( $fields[ 'country' ] ) ?>
									</select>
									<label class="control-label" for="country"><?php _e( 'Country', 'rem' ) ?></label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-xs-30">
								<div class="field">
									<input form="form_delivery" class="form-control" id="state" type="text" name="state" value="<?php echo( $fields[ 'state' ] ) ?>" placeholder="<?php _e( 'State/Region', 'rem' ) ?>" required="required">
									<label class="control-label" for="state"><?php _e( 'State/Region', 'rem' ) ?></label>
								</div>
							</div>
							<div class="form-group col-xs-30">
								<div class="field">
									<input form="form_delivery" class="form-control" id="postal_code" type="text" name="postal_code" value="<?php echo( $fields[ 'postal_code' ] ) ?>" placeholder="<?php _e( 'Postal code', 'rem' ) ?>" required="required">
									<label class="control-label" for="postal_code"><?php _e( 'Postal code', 'rem' ) ?></label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-xs-60">
								<div class="field">
									<input form="form_delivery" class="form-control" id="phone_number" type="tel" name="phone_number" value="<?php echo( $fields[ 'phone_number' ] ) ?>" placeholder="<?php _e( 'Phone (optional)', 'rem' ) ?>">
									<label class="control-label" for="phone_number"><?php _e( 'Phone (optional)', 'rem' ) ?></label>
								</div>
							</div>
						</div>
						<?php if ( isOneFreeShipping() ): ?>
							<h3><?php _e( 'Shipping method', 'rem' ); ?></h3>
							<div class="row">
								<div class="field-group">
									<div class="box-radio">
										<label class="pull-left"><input form="form_delivery" type="radio" checked><?php _e( 'Free International Shipping', 'rem' ) ?>
										</label>
										<div class="pull-right info">
											<span class="wrap-text"><?php _e( 'Free', 'rem' ); ?></span></div>
									</div>
								</div>
							</div>
						<?php endif; ?>
						<?php if ( $gateways ): ?>
							<h3><?php _e( 'Payment method', 'rem' ); ?></h3>
							<div class="info-head"><?php _e( 'All transactions are secure and encrypted. Credit card information is never stored.', 'rem' ); ?></div>
							<div class="payments-list">
								<?php if ( file_exists( dirname( __FILE__ ) . '/tpl/_payments-list.php' ) ) {
									include dirname( __FILE__ ) . '/tpl/_payments-list.php';
								} ?>
								</div>
						<?php endif; ?>
						<div class="b-cart-btn_active">
							<div class="continue_shopping pull-left">
								<span>‹</span>
								<a href="/" class="link_continue_shopping"><?php _e( 'Continue shopping', 'rem' ); ?></a>
							</div>
							<button type="submit" form="form_delivery" name="ads_checkout" value="ads" class="btn pull-right" onclick="ga('send', 'event', 'Proceed to Pay', 'Click')"><?php _e( 'Сomplete order', 'rem' ); ?></button>
						</div>
					</form>
				</div>
			</div>
		</div>

	</div>
	<div id="ads-notify" style="display:none"><?php echo $message; ?></div>
<?php get_footer() ?>
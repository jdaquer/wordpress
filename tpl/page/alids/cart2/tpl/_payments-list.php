<?php
$i = 0;

foreach ( $gateways as $k => $v ) {

	if( $k == 'promo_code' ) continue;

	$i++;

	$checked = ( $fields[ 'type' ]=='' && $i == 1 ) ? 'checked="checked"' : "";
	$checked = ( $fields[ 'type' ] == $k ) ? 'checked="checked"' : $checked;

	if ( 'paypal' == $k ) { ?>
		<div class="item">
			<div class="box-radio">
				<label class="pull-left" for="<?php echo $k ?>"><input form="form_delivery" id="<?php echo $k ?>" type="radio" name="type" value="<?php echo $k ?>" required="required" <?php echo $checked; ?>><?php _e( 'PayPal', 'rem' ) ?>
				</label>
				<div class="pull-right info">
					<span class="wrap-img"><img src="<?php echo $v[ 'logo' ] ?>" alt=""></span>
				</div>
			</div>
			<div class="payments-field <?php echo $k ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/img/cart2/paypal-redirect.png" alt="">
				<div class="text-info"><?php _e( 'You will be redirected to PayPal to complete your purchase securely.', 'rem' ); ?></div>
			</div>
		</div>
	<?php } elseif ( 'cc' == $k && in_array($v['type'], array_keys(ads_ccard_form_access())) ) { ?>
		<div class="item">
			<div class="box-radio">
				<label class="pull-left" for="<?php echo $k ?>"><input form="form_delivery" id="<?php echo $k ?>" type="radio" name="type" value="<?php echo $k ?>" required="required" <?php echo $checked; ?>><?php _e( 'Credit Card', 'rem' ) ?>
				</label>
				<div class="pull-right info">
					<span class="wrap-img"><img src="<?php echo $v[ 'logo' ] ?>" alt=""></span>
				</div>
			</div>
			<div class="payments-field <?php echo $k ?>">
				<div class="card">
					<div class="col-xs-60 card_number">
						<input form="form_delivery" class="form-control" id="number" type="text" inputmode="numeric" pattern="[0-9]*" name="number" placeholder="<?php _e( 'Card number', 'rem' ) ?>" required="required">
					</div>
					<div class="col-sm-30 col-xs-60 name_on_card "><i class="icon"></i>
						<input class="form-control" type="text" name="name_card" placeholder="<?php _e( 'Name on card', 'rem' ) ?>" required="required">
					</div>
					<div class="col-xs-30 col-sm-15">
						<div class="form-control date">
							<input class="" type="number" name="exp_month" pattern="^(0[1-9]|1[0-2])$" placeholder="MM" required="required">
							<span class="separator">/</span>
							<input class="" type="number" name="exp_year" pattern="^([1-2][0-9])$" placeholder="YY" required="required">
						</div>

					</div>
					<div class="col-xs-30 col-sm-15 cvc">
						<input class="form-control" type="number" name="cvv" placeholder="<?php _e( 'CVC', 'rem' ) ?>" required="required">
					</div>
				</div>
			</div>
		</div>
	<?php } else { ?>
		<div class="item">
			<div class="box-radio">
				<label class="pull-left" for="<?php echo $k ?>"><input form="form_delivery" id="<?php echo $k ?>" type="radio" name="type" value="<?php echo $k ?>" <?php echo $checked;?>><?php echo $v['title']; ?>
				</label>
				<div class="pull-right info">
					<span class="wrap-img"><img src="<?php echo $v[ 'logo' ] ?>" alt=""></span>
				</div>
			</div>
			<div class="payments-field <?php echo $k ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/img/cart2/paypal-redirect.png" alt="">
				<div class="text-info"><?php _e( 'Click proceed to pay to complete your order.', 'rem' ); ?></div>
			</div>
		</div>
	<?php }
} ?>
<?php
global $adsBasket, $message;
$message  = isset( $_POST[ 'ads-error' ] ) ? $_POST[ 'ads-error' ][ 'message' ] : false;

function ads_notify_cart($message){
	return $message;
}
add_filter('ads_notify', 'ads_notify_cart');

$gateways = rem_listGateway();

$fields = array(
	'email'        => '',
	'full_name'    => '',
	'address'      => '',
	'city'         => '',
	'country'      => '',
	'state'        => '',
	'postal_code'  => '',
	'phone_number' => '',
	'description'  => '',
	'type'         => ''
);

foreach ( $fields as $k => $v ) {
	$fields[ $k ] = isset( $_POST[ $k ] ) ? esc_attr( $_POST[ $k ] ) : '';
}

?>
<?php get_header() ?>
	<!-- CHECKOUT -->
	<div class="page-checkout cart2">
		<div class="container">
			<div class="row">
				<div class="col-lg-60">
					<div class="title-page"><?php _e( 'Cart', 'rem' ); ?></div>
				</div>
				<div class="col-lg-43">
					<?php if ( file_exists( dirname( __FILE__ ) . '/_cart.php' ) ) {
						include dirname( __FILE__ ) . '/_cart.php';
					} ?>
				</div>
				<div class="col-lg-16 col-lg-offset-1 right-column">
					<?php if ( file_exists( dirname( __FILE__ ) . '/tpl/_sidebar.php' ) ) {
						include dirname( __FILE__ ) . '/tpl/_sidebar.php';
					} ?>
				</div>
			</div>
		</div>
	</div>

<?php get_footer() ?>
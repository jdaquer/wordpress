

<?php if ( file_exists( dirname( __FILE__ ) . '/tpl/_list_order.php' ) ) {
	include dirname( __FILE__ ) . '/tpl/_list_order.php';
} ?>

<div class="take-checkout">
		<div class="num ">
			<div class="name"><?php _e( 'Number of products', 'rem' ) ?>:</div>
			<div class="value"><span class="js-page-total_count"></span></div>
		</div>
		<div class="total">
			<div class="name"><?php _e( 'Total price', 'rem' ) ?>:</div>
			<div class="value"><span class="js-cur_salePrice"></span></div>
		</div>
</div>

<form id='form_delivery' class="" action="" method="POST">

	<?php if ( file_exists( dirname( __FILE__ ) . '/tpl/_form.php' ) ) {
		include dirname( __FILE__ ) . '/tpl/_form.php';
	} ?>

	<?php if ( isOneFreeShipping() ): ?>
		<div class="shipping">
			<div class="wrap-line">
				<h4 class="head-line">
					<?php _e( 'Shipping', 'rem' ) ?>:
				</h4>
			</div>
			<div class="row">
				<div class="form-group col-sm-30">
					<div class="col-sm-44 col-sm-offset-16">
						<div class="text"><?php _e( 'Free worldwide shipping', 'rem' ); ?></div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<?php if ( file_exists( dirname( __FILE__ ) . '/tpl/_payment.php' ) ) {
		include dirname( __FILE__ ) . '/tpl/_payment.php';
	} ?>


	<div class="b-cart-total col-sm-60">
		<div class="row">
			<div class="col-xs-60 col-sm-30 ">
				<?php if ( isPromocodesEnabled() ): ?>
					<a href="#" class="js-b-coupon"><?php _e( 'Have a promotional code?', 'rem' ) ?></a>
					<div class="b-coupon" style="display: none;">
						<label for="discount"><?php _e( 'Promo code', 'rem' ) ?></label>

						<div class="b-coupon__discount">
							<input form="form_delivery" id="discount" type="text" name="discount" placeholder="<?php _e( 'Enter promo code', 'rem' ) ?>">

							<div class="b-coupon__msg b-coupon__msg--err" style="display: none;"><?php _e( 'Code does not exist', 'rem' ) ?></div>
							<div class="b-coupon__msg b-coupon__msg--valid" style="display: none;"><?php _e( 'Code Accepted', 'rem' ) ?></div>
						</div>
						<button class="btn b-coupon__btn js-coupon__btn"><?php _e( 'Apply', 'rem' ) ?></button>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-xs-60 pull-right col-sm-30 ">
				<dl class="b-cart-total_count pull-right js-page-total_price-box">
					<dt><?php _e( 'List price', 'rem' ) ?>:</dt>
					<dd><strike><span class="js-page-total_price"></span></strike>
					</dd>
				</dl>
				<dl class="b-cart-total_count pull-right js-page-total_save-box">
					<dt><?php _e( 'You save', 'rem' ) ?>:</dt>
					<dd class="js-page-total_save"></dd>
				</dl>
				<dl class="b-cart-total_count pull-right">
					<dt><?php _e( 'Shipping', 'rem' ) ?>:</dt>
					<dd class="js-total_shipping"><?php _e( 'Free', 'rem' ) ?></dd>
				</dl>
				<dl class="b-cart-total_price pull-right grand-total">
					<span class="name"><?php _e( 'Total price', 'rem' ) ?>:</span>
					<span class="value numb js-cur_salePrice"></span>
				</dl>
			</div>
		</div>
	</div>

	<?php if(cz('tp_readonly_read_required')): ?>
		<div class="readonly_checkbox">
			<label for="readonly_checkbox">
				<input id="readonly_checkbox" type="checkbox" readonly><?php echo cz('tp_readonly_read_required_text') ?>
			</label>
		</div>
	<?php endif; ?>

	<div class="b-cart-btn_active">
		<div class="pull-left">
			<span>‹</span> <a href="/" class="link_continue_shopping" data-dismiss="modal">
				<?php _e( 'Continue shopping', 'rem' ) ?>
			</a>
		</div>
		<button type="submit" form="form_delivery" name="ads_checkout" class="btn btn_orange pull-right" onclick = "ga('send', 'event', 'Proceed to Pay', 'Click')">
			<?php _e( 'Proceed to Pay', 'rem' ) ?>
		</button>
	</div>
</form>
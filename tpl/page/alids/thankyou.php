<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 11.09.2015
 * Time: 12:14
 */
get_header();

$m_head = cz( 'tp_thankyou_fail_yes_head' );
$m_text = '';
$m_img  = 'fail';
$m_script  = cz('tp_thankyou_fail_yes_head_tag');

if ( isset( $_GET[ 'fail' ] ) && $_GET[ 'fail' ] == 'no' ) {
	$m_head = cz( 'tp_thankyou_fail_no_head' );
	$m_text = cz( 'tp_thankyou_fail_no_text' );
	$m_script  = cz('tp_thankyou_fail_no_head_tag');
	$m_img  = 'chek';
}

?>
	<!-- THANK ORDER TEMPLATE-->
	<div class="container-fluid page-thank-order">
			<div class="container">
				<div class="page-thank-order__main">
					<div class="page-thank-order__top">
						<div class="page-thank-order__top__img">
							<?php printf( '<img src="%1$s">',
								get_template_directory_uri() . '/img/' . $m_img . '.png'
							); ?>
						</div>
						<div class="page-thank__order__content">
							<?php printf( '<h3>%1$s</h3><div class="page-thank__order__content__dis">%2$s</div><script >%3$s</script>',
								$m_head,
								$m_text,
								$m_script
							); ?>

							<div class="page-thank__order_btn">
								<a href="<?php echo esc_url( home_url('/product/') ) ?>"
								   class="btn btn-bord">
									<?php _e( 'Continue shopping', 'rem' ); ?></a>
								<a href="<?php echo esc_url( home_url('/contact-us/') ) ?>"
								   class="btn btn-bord">
									<?php _e( 'Contact Us', 'rem' ); ?></a>
							</div>
						</div>
					</div>
				</div>
		</div>
	</div>
<?php get_footer() ?>
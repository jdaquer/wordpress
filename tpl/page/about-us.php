<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 10.08.2015
 * Time: 16:43
 */
?>

<?php get_header() ?>

<!-- About -->
<div class="page-about">
	<div class="wrap-b1" style="background: url(<?php echo cz('tp_about_img'); ?>) center/cover no-repeat">
		<div class="container">
			<?php echo  cz('tp_about_description'); ?>
		</div>
	</div>


	<?php if(cz('meet_our_team')): ?>
	<div class="meet-our-team">
		<div class="container">
			<div class="head"><?php echo cz('tp_about_title'); ?></div>
			<div class="text"><?php echo cz('tp_about_text'); ?></div>

			<div class="row">
				<div class="list hidden-xs hidden-sm">
					<?php
					for ( $i = 1; $i <= 5; $i ++ ) {
						if(! cz( 'tp_mt1_img_' . $i ))continue;
						printf( '<div class="our-team-item">
                                    <div class="our-team-face">
                                        <img src="%1$s?1000" alt="" class="img-responsive">			
                                    </div>
                                    <div class="name">%2$s</div>
                                    <div>%3$s</div>
                                </div>',
							cz( 'tp_mt1_img_' . $i ),
							cz( 'tp_mt1_name_' . $i ),
							cz( 'tp_mt1_cus_' . $i ) );
					}
					?>
				</div>
				<div class="list list-slider visible-xs visible-sm">

					<?php
					for ( $i = 1; $i <= 5; $i ++ ) {
						if(! cz( 'tp_mt1_img_' . $i ))continue;
						printf( '<div class="our-team-item">
					<div class="our-team-face">
						<img src="%1$s" alt="" class="img-responsive">			
					</div>
					<div class="name">%2$s</div>
					<div>%3$s</div>
				</div>',
							cz( 'tp_mt1_img_' . $i ),
							cz( 'tp_mt1_name_' . $i ),
							cz( 'tp_mt1_cus_' . $i ) );
					}
					?>
				</div>
			</div>
		</div>

	</div>
	<?php endif; ?>

	<div class="keep-contact-with">
		<div class="container">
			<div class="head"><?php echo cz('tp_keep_title'); ?></div>
			<div class="text"><?php echo cz('tp_keep_text'); ?></div>

			<a href="<?php echo esc_url(home_url('/contact-us/')); ?>" class="btn btn-bord"><?php _e( 'Contact Us', 'rem' ); ?></a>
		</div>

	</div>
</div>

<?php get_footer() ?>

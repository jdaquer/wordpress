<?php get_header() ?>

	<!-- CONTACT-US -->
	<div class="page-content">
		<div class="container">
			<div class="row">
				<div class="col-md-60 article">
					<div class="p-heading-page">
						<div class="p-title"><?php the_title(); ?></div>
					</div>
					<div class="row">
						<div class="col-md-60 content">
							<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
								<?php the_content(); ?>
								<?php endwhile; endif; ?>
							<div class="btn-home">
								<a href="/" class="btn btn-bord"><?php _e( 'Go to Homepage', 'rem' ); ?></a>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
<?php get_footer() ?>
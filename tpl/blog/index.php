<style>
    <?php if(!cz('blog_banner_mobile_show_1')):?>
    @media (max-width: 768px) {
        .banner_top{
            display: none;
        }
    }
    <?php endif;?>
    <?php if(!cz('blog_banner_mobile_show_2')):?>
    @media (max-width: 768px) {
        .banner_bottom{
            display: none;
        }
    }
    <?php endif;?>
</style>

<div class="container blog-page">
        <div class="shadow"></div>

		<div class="row">
			<div class="col-sm-60 col-xs-60 col-md-39 col-lg-43">

                <?php get_template_part( 'tpl/blog/tpl/_select_category' ); ?>

                <!-- BREADCRUMBS -->
                <div class="breadcrumbs">
                    <?php adstm_breadcrumbs() ?>
                </div>
                <!-- BREADCRUMBS -->

				<?php
				if ( is_single() ) {
					get_template_part( 'tpl/blog/tpl/_single' );
				} else {  ?>

                    <?php
					get_template_part( 'tpl/blog/tpl/_loop' );
				}
				?>
			</div>

            <div class="visible-md visible-lg col-md-offset-2 col-md-19 col-lg-15">
                <?php get_template_part( 'tpl/blog/tpl/_bar' ); ?>
            </div>

            <div class="popular-products">
                <div class="row">
                    <div class="popular-products-title">
                        <span class="p-title">
                            Most Popular Products from Our Store
                        </span>
                    </div>
                    <?php get_template_part( 'tpl/product/home/_topselling' ); ?>
                </div>
            </div>

		</div>
</div><!-- .container -->
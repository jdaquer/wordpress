<div class="bar">
	<div class="list-cat">
        <div class="banner_top">
            <?php echo cz('blog_banner_1'); ?>
        </div>

        <div class="search" style="margin-top: 20px;">
            <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
                <input type="hidden" name="target" value="post" />
                <input type="hidden" name="post_type" value="post" />
                <div class="input-group">
                    <input type="text" name="s" class="form-control" value="<?php echo get_search_query() ?>" placeholder="Search..." />
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                    </span>
                </div><!-- /input-group -->
            </form>
        </div>

        <div class="categories">
            <h2>Categories</h2>
            <ul>
                <?php
                wp_list_categories(
                    array(
                        'child_of'            => 0,
                        'current_category'    => 0,
                        'depth'               => 0,
                        'echo'                => 1,
                        'exclude'             => '1',
                        'exclude_tree'        => '',
                        'feed'                => '',
                        'feed_image'          => '',
                        'feed_type'           => '',
                        'hide_empty'          => 1,
                        'hide_title_if_empty' => false,
                        'hierarchical'        => true,
                        'order'               => 'ASC',
                        'orderby'             => 'name',
                        'separator'           => '<br />',
                        'show_count'          => 1,
                        'show_option_all'     => '',
                        'show_option_none'    => __( 'No categories', 'rem' ),
                        'style'               => 'list',
                        'taxonomy'            => 'category',
                        'title_li'            => '',
                        'use_desc_for_title'  => 1,
                    )
                );
                ?>
            </ul>
        </div>
	</div>

    <?php if ( cz( 'blog_subscribe' ) ): ?>
        <div class="subscription">
            <div class="f-head"><?php _e( 'STAY UP TO DATE', 'rem' ) ?></div>
            <?php echo cz( 'blog_subscribe' ); ?>
        </div>
    <?php endif; ?>

	<div class="recent-posts">
        <?php

        $query = new WP_Query(array(
            'meta_key' => 'post_views_count',
            'orderby' => 'meta_value_num',
            'order' => 'desc',
            'posts_per_page' => 5,
        ));

        if ($query->have_posts()) :
        ?>
        <div class="popular">
            <h2>Most Popular</h2>

            <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                <div class="item clearfix">
                    <?php printf('<a class="preview" href="%s">%s</a>', get_the_permalink(), theme_thumb_photo($post->ID, 'thumbnail')) ?>
                    <div class="title">
                        <?php printf('<a href="%s">%s</a>', get_the_permalink(), get_the_title()) ?>
                    </div>
                    <div class="date">
                        <?php echo get_the_date() ?>
                    </div>
                </div><!-- .item -->
            <?php endwhile; ?>

        </div>
            <?php wp_reset_postdata(); endif; ?>
	</div>

    <div class="banner_bottom">
	    <?php echo cz('blog_banner_2'); ?>
    </div>

</div>


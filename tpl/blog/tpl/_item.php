<div class="blog-item">
    <div class="row">

        <div class="col-xs-22 col-sm-20 visible-xs visible-sm visible-md visible-lg">
            <?php printf('<a href="%s">%s</a>', get_the_permalink(), theme_thumb_photo($post->ID, 'medium')) ?>
        </div>

        <div class="col-xs-38 col-xs-offset-0 col-sm-39 col-sm-offset-1">
            <?php the_title( '<h2 class="blog-title"><a href="' . get_the_permalink() . '">', '</a></h2>' ); ?>
            <div class="blog-meta">
                <time class="meta-time" datetime="<?php echo get_the_date() ?>" itemprop="datePublished"><?php echo get_the_date() ?></time>

                <?php
                $categories = get_the_category( $post->ID );
                echo '<strong>Category: </strong>';
                echo get_the_category_list( ', ', 1 );
                ?>

                <div class="pull-right hidden-xs">
                    <span class="view"><i class="fa fa-eye"></i> <?php echo getPostViews($post->ID) ?></span>

                    <span class="comment">
                        <i class="fa fa-comment"></i>
                        <?php echo get_comments_number(); ?>
                    </span>
                </div>

            </div>


            <div class="content">
                <div class="hidden-sm hidden-xs">
                    <?php the_excerpt(); ?>
                </div>
                <div class="footer-content">
                    <?php printf( '<a class="btn more-link" href="%1$s" target="_blank">%2$s</a>',
                        get_the_permalink(),
                        __( 'Read More', 'rem' )
                    ); ?>
                </div>
            </div>

        </div>
	</div>
</div>
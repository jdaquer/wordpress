<div class="visible-xs visible-sm">
    <div class="block_line">
        <span class="block_line_title">Blog</span>
        <span class="line"></span>
    </div>
</div>

<div class="visible-xs visible-sm">
    <div class="list">
        <div class="cat_list">
            <form action="<?php bloginfo( 'url' ); ?>/" method="get">
                <div>
                    <?php category_blog_list_show(); ?>
                    <script type="text/javascript">
                        var dropdown = document.getElementById("blog_cat");
                        function onCatChangeBlog() {
                            if ( dropdown.options[dropdown.selectedIndex].value != -1 ) {
                                location.href = dropdown.options[dropdown.selectedIndex].value;
                            }
                        }
                        dropdown.onchange = onCatChangeBlog;
                    </script>
                </div>
            </form>
        </div>
    </div>
</div>

<?php

while ( have_posts() ) : the_post(); ?>

	<div class="blog-item single">
        <div class="visible-xs visible-sm">
            <div class="banner_top">
	            <?php echo cz('blog_banner_1'); ?>
            </div>
        </div>

		<?php the_title( '<h2 class="blog-title"><a href="' . get_the_permalink() . '">', '</a></h2>' ); ?>
		<div class="blog-meta pull-left">
			<time class="meta-time" datetime="<?php echo get_the_date() ?>" itemprop="datePublished"><?php echo get_the_date() ?></time>
			<?php
			$categories = get_the_category( $post->ID );
            echo '<strong>Category: </strong>';
			foreach ( $categories as $category ) {
				printf( '<span><a href=">%1$s">%2$s</a></span>',
					get_category_link($category->cat_ID),
					$category->name
				);
			}
			?>
		</div>

        <div class="pull-right">
            <div class="pull-left">
                <span class="view"><i class="fa fa-eye"></i> <?php echo getPostViews($post->ID) ?></span>
                <span class="comment">
                    <i class="fa fa-comment"></i>
                    <?php echo get_comments_number(); ?>
                </span>
            </div>

		    <div class="blog-share ya-share2 pull-right hidden-xs" data-services="facebook,gplus,pinterest,twitter" data-size="m" data-counter=""></div>
        </div>

		<div class="content">
			<?php the_content(); ?>
		</div>

		<?php if(cz('blog_register_html')): ?>
            <div class="sidebar-blog">
				<?php echo cz('blog_register_html'); ?>
            </div>
		<?php endif; ?>

        <div class="share">
            <div class="blog-share ya-share2 text-center" data-services="facebook,gplus,pinterest,twitter" data-size="m" data-counter=""></div>
        </div>

	<!--<div class="blog-pagination-single">-->
		<?php
        $prev = get_previous_post();
        $next = get_next_post();

        if (empty($prev)) {
            $prev = wp_get_recent_posts(['numberposts' => 1], OBJECT);
            $prev = $prev[0];
        }

        if (empty($next)) {
            $next = wp_get_recent_posts(['numberposts' => 1, 'order' => 'ASC'], OBJECT);
            $next = $next[0];
        }

        ?>

        <div class="post-nav">
            <div class="post-nav-box">
                <div class="row">
                    <div class="col-xs-30">
                        <?php printf('<a href="%s">%s</a>', $prev->guid, __('< Previous post') ) ?>
                    </div>
                    <div class="col-xs-30 text-right">
                        <?php printf('<a href="%s">%s</a>', $next->guid, __('Next post >') ) ?>
                    </div>
                </div><!-- .row -->
            </div><!-- .post-nav-box -->

            <div class="row ">
                <div class="col-xs-30">
                    <div class="item clearfix">
                        <!--<div class="hidden-xs hidden-sm">-->
                            <?php printf('<a href="%s">%s</a>', $prev->guid, theme_thumb_photo($prev->ID, 'thumbnail')) ?>
                        <!--</div>-->
                        <div class="title">
                            <?php printf('<a href="%s">%s</a>', $prev->guid, $prev->post_title) ?>
                        </div>
                        <div class="date">
                            <?php echo get_the_date() ?>
                        </div>
                    </div><!-- .item -->
                </div>
                <div class="col-xs-30">
                    <div class="item clearfix">
                        <!--<div class="hidden-xs hidden-sm">-->
                            <?php printf('<a href="%s">%s</a>', $next->guid, theme_thumb_photo($next->ID, 'thumbnail')) ?>
                        <!--</div>-->
                        <div class="title">
                            <?php printf('<a href="%s">%s</a>', $next->guid, $next->post_title) ?>
                        </div>
                        <div class="date">
                            <?php echo get_the_date() ?>
                        </div>
                    </div><!-- .item -->
                </div>
            </div><!-- .row -->

        </div><!-- .post-nav -->

        <div class="visible-xs visible-sm">
            <div class="banner_bottom">
	            <?php echo cz('blog_banner_2'); ?>
            </div>
        </div>

        <?php

        if ((comments_open() || get_comments_number())) {
            comments_template();
        }
        ?>
        <!--</div>-->
        <?php if(cz('tm_show_blog_comment')): ?>
            <!--<div class="row">
                <div class="col-xs-60">

                    <div class="blog-comments">
                        <div class="fb-comments" data-href="" data-numposts="5" data-colorscheme="light" data-width="100%"></div>
                    </div>

                </div>
            </div>--><!-- .row -->
        <?php endif; ?>
    </div>




<?php endwhile; ?>

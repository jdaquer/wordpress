<div class="block_line">
    <span class="block_line_title">Blog</span>
    <span class="line"></span>
</div>

<div class="visible-xs visible-sm">
    <div class="list">
        <div class="cat_list">
            <form action="<?php bloginfo( 'url' ); ?>/" method="get">
                <div>
                    <?php category_blog_list_show(); ?>
                    <script type="text/javascript">
                        var dropdown = document.getElementById("blog_cat");
                        function onCatChangeBlog() {
                            if ( dropdown.options[dropdown.selectedIndex].value != -1 ) {
                                location.href = dropdown.options[dropdown.selectedIndex].value;
                            }
                        }
                        dropdown.onchange = onCatChangeBlog;
                    </script>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="blog-list">
    <div class="visible-xs visible-sm">
        <div class="banner_top">
	        <?php echo cz('blog_banner_1'); ?>
        </div>
    </div>

    <div class="visible-xs visible-sm">
        <?php
        $show_load_more = true;
        $cat_id = 0;
        if(is_category()) {
            ?>
            <div class="category-blog-header">
                <?php
                $category = get_the_category();
                $show_load_more = false;
                foreach ($category as $cat) {
                    echo '<strong>' . $cat->name . '</strong> <span>(' . $cat->count . ')</span>';
                    if($cat->count >= 20){
                        $show_load_more = true;
                        $cat_id = $cat->term_id;
                    }
                }
                ?>
            </div>
            <?php
        }
        ?>
    </div>

    <?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'tpl/blog/tpl/_item' ); ?>
	<?php endwhile; ?>
</div>

<div class="hidden-sm hidden-xs">
    <div class="pagination-blog">
        <?php custom_pagination(); ?>
    </div>
</div>

<div class="visible-xs visible-sm">
    <?php
    if($show_load_more) {
        ?>
        <div class="text-center">
            <a class="btn load-more-link" id="show_more_posts" href="#" data-offset="15" data-cat="<?php echo $cat_id;?>">Load More</a>
        </div>
        <?php
    }
    ?>
</div>

<div class="visible-xs visible-sm">
    <div class="banner_bottom">
	    <?php echo cz('blog_banner_2'); ?>
    </div>
</div>
<?php
/**
 * Created by PhpStorm.
 * User: sunfun
 * Date: 07.09.16
 * Time: 13:57
 */

function category_blog_list_show() {

	/*	$select = wp_dropdown_categories( array(
			'show_option_all' => __( 'All Categories', 'rem' ),
			'taxonomy' => 'product_cat',
			'show_count'      => 0,
			'hide_empty'      => 1,
			'echo'            => 0,
			'name'            => 'product_cat',
			'value_field'     => 'slug'
		) );
		$select = preg_replace( "#<select([^>]*)>#", "<select$1 onchange='return this.form.submit()'>", $select );
		$select = preg_replace( '#value=["\']0["\']#', 'value="product"', $select );
		echo $select;*/

    $post_slug = get_queried_object()->slug;
	//$post_slug = get_query_var('product_cat', 'product');
	$categories = get_categories(array(
		'taxonomy' => 'category',
		'orderby' => 'name',
		'order'   => 'ASC',
        'child_of' => 0,
    ));

	$select = '<div class="row"><select class="col-xs-60" name="category" id="blog_cat" class="postform" tabindex="-98">';
	$select.= "<option value='".home_url('blog')."'>".__('All Categories', 'rem')."</option>";

	foreach($categories as $category){
		if($category->count > 0){
			$selected = $category->slug == $post_slug ? 'selected="selected"':'';
			$select.= "<option value='".get_term_link( $category->term_id , "category" )."' ".$selected.">".$category->name ." (". $category->count.")</option>";
		}
	}

	$select.= "</select></div>";

	echo $select;
}

?>

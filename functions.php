<?php
remove_action('wp_head', 'wp_print_scripts');
remove_action('wp_head', 'wp_print_head_scripts', 9);
remove_action('wp_head', 'wp_enqueue_scripts', 1);

add_action('wp_footer', 'wp_print_scripts', 5);
add_action('wp_footer', 'wp_enqueue_scripts', 5);
add_action('wp_footer', 'wp_print_head_scripts', 5);

include( __DIR__ . '/inc/rem/lang.php' );
include( __DIR__ . '/inc/update.php' );
include( __DIR__ . '/inc/customization/init.php' );
include( __DIR__ . '/inc/fb_pixel.php' );
include( __DIR__ . '/inc/seo.php' );
include( __DIR__ . '/inc/breadcrumbs.php' );
include( __DIR__ . '/inc/instagram.php' );
include( __DIR__ . '/inc/rem/init.php' );
include( __DIR__ . '/inc/blog.php' );


if ( defined( 'ADS_ERROR' ) && ! ADS_ERROR ) {
	include( __DIR__ . '/inc/alids.php' );
}

/**
 * Remove adminbar for subscribers
 */
if ( is_user_logged_in() && ! current_user_can( "level_2" ) ) {
	add_filter( 'show_admin_bar', '__return_false' );
}

function adstm_wp_seo(){

    $seo = '<title>' . wp_title('', false) . '</title>';

    if( class_exists('\models\seo\Meta') ){
        $seo = \models\seo\Meta::block();
    }

    echo $seo;
}

/**
 * Enable responsive video (bootstrap only)
 *
 * @param $html
 * @param $url
 * @param $attr
 * @param $post_ID
 *
 * @return string
 */
function adstm_oembed_filter( $html, $url, $attr, $post_ID ) {
	return '<div class="embed-responsive embed-responsive-16by9">' . $html . '</div>';
}

add_filter( 'embed_oembed_html', 'adstm_oembed_filter', 10, 4 );


/**
 * Convert post_content \n
 *
 * @param $content
 *
 * @return mixed
 */
if ( ! function_exists( 'nl2br_content' ) ) {
	function nl2br_content( $content ) {
		$content = apply_filters( 'the_content', $content );

		return str_replace( ']]>', ']]>', $content );
	}
}

/**
 * @return bool
 */
function adstm_is_home() {
	return is_front_page() || is_home();
}

add_filter( 'get_the_archive_title', function ( $title ) {
	if ( is_category() || is_tax() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_search() ) {
		$title = sprintf( '%1$s: "%2$s"', __( 'Search', 'rem'), get_search_query() );
	} elseif ( is_post_type_archive() ) {
		$title = post_type_archive_title( '', false );
	} elseif ( is_tag() ) {
		$title = single_tag_title( '', false );
	} elseif ( is_author() ) {
		$title = sprintf( '<span class="vcard">%s</span>', get_the_author() );
	}

	return $title;
} );

function adstm_single_term() {

	$other_title = get_query_var( 'other_title', false );

	return ( $other_title ) ? $other_title : single_term_title( '', false );
}


function adstm_isExpressCheckoutEnabled() {

	if ( ! function_exists( 'isExpressCheckoutEnabled' ) ) {
		return false;
	}

	return isExpressCheckoutEnabled();
}

add_filter( 'body_class', 'adstm_body_classes' );

function adstm_body_classes( $classes ) {
	$pagename = get_query_var( 'pagename' );
	$classes[] = $pagename;
	return $classes;
}

function is_enableSocial(){
	if(cz('s_link_fb') ||
	   cz('s_link_fb')||
	   cz('s_link_in')||
	   cz('s_link_tw')||
	   cz('s_link_tw')||
	   cz('s_link_gl')||
	   cz('s_link_pt')||
	   cz('s_link_yt')){
		return true;
	}
	return false;
}

function adstm_get_host() {
	$url = home_url();
	return parse_url($url, PHP_URL_HOST);
}

function adstm_current_currency() {

	$list_currency = ads_get_list_currency();

	printf( '<img src="%1$s"><span>(%2$s)</span> ',
		pachFlag($list_currency[ ADS_CUR ][ 'flag' ]) . '?100',
		trim( $list_currency[ ADS_CUR ][ 'symbol' ] ) );
}

/**
 * adstm_account_page
 * 
 */
function adstm_account_page() {
    $form = '';
    $menu = '';
    $menus = models\account\Menu::getItems(
        false,
        array(
            models\account\Menu::ORDERS,
            models\account\Menu::LOGOUT
        )
    );

    if (!class_exists('\models\account\User')) {
        return false;
    }

    if (!class_exists('\models\account\RenderForm')) {
        return false;
    }

    $user = new \models\account\User();
    $accountData = $user->find();
    $menu = \models\account\RenderForm::showNavigation($menus);
    $form = \models\account\RenderForm::showForm($accountData);

    echo $menu . $form;
}

/**
 * adstm_orders_page
 * 
 */
function adstm_orders_page($page, $limit) {
    $orders = '';
    $menu = '';
    $menus = models\account\Menu::getItems(
        models\account\Menu::ORDERS,
        array(
            models\account\Menu::ACCOUNT,
            models\account\Menu::LOGOUT
        )
    );

    if (class_exists('\models\account\RenderForm')) {
        $ordersModel = new \models\account\Orders();
        $ordersData = $ordersModel->find($limit, $page);
        $countOrders = $ordersModel->count();
        $menu = \models\account\RenderForm::showNavigation($menus);
        $orders = \models\account\RenderForm::showOrders(
            $ordersData,
            $countOrders,
            $limit,
            $page
        );
    }

    echo $menu . $orders;
}

/**
 * adstm_login_button
 * 
 * @return type
 */
function adstm_login_button()
{
    $loginButton = '';
    if (class_exists('\models\account\RenderForm')) {
        $loginButton = \models\account\RenderForm::showLoginButton();
    }

    return $loginButton;
}

/**
 * adstm_register_form
 * 
 * @return string
 */
function adstm_register_form()
{
    $registerForm = '';
    if (class_exists('\models\account\RenderForm')) {
        $registerForm = \models\account\RenderForm::showRegisterForm();
    }

    return $registerForm;
}

/**
 * adstm_login_form
 * 
 * @return string
 */
function adstm_login_form()
{
    $loginForm = '';
    if (class_exists('\models\account\RenderForm')) {
        $loginForm = \models\account\RenderForm::showLoginForm();
    }

    return $loginForm;
}

/**
 * adstm_confirmation
 * 
 * @return string
 */
function adstm_confirmation()
{
    $confirmation = '';
    if (class_exists('\models\account\RenderForm')) {
        $confirmation = \models\account\RenderForm::confirmBlock();
    }
    return $confirmation;
}

/**
 * my_front_end_login_fail
 * 
 * @param type $username
 */
function my_front_end_login_fail($username) {
   $referrer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
   if (!empty($referrer)
       && !strstr($referrer,'wp-login')
       && !strstr($referrer,'wp-admin')
    ) {
      wp_redirect($referrer . '?login=failed');
      exit;
   }
}

/**
 * failed login
 * 
 */
add_action('wp_login_failed', 'my_front_end_login_fail');

/**
 * verify_username_password
 * 
 * @param type $user
 * @param type $username
 * @param type $password
 */
function verify_username_password($user, $username, $password) {
    $result = false;
    $error = new WP_Error();
    $referrer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : false;

    if ($referrer) {
        $referrer = esc_url(remove_query_arg('login', $referrer));
        $referrer = esc_url(remove_query_arg('password', $referrer));
        $referrer = esc_url(remove_query_arg('loggedout', $referrer));
    }

    if ($username == "" && $referrer) {
        $referrer .= "?login=empty";
        $error->add('empty_username', __('<strong>ERROR</strong>: The username field is empty.'));
        $result = true;
    }

    if (!empty($username) && adstm_check_username($username)) {
        $message = 'not_found_username';
        if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
            $message = 'not_found_email';
        }
        $referrer .= "?login=" . $message;
        $result = true;
        $error->add(
            'invalid_username',
            __( '<strong>ERROR</strong>: Invalid username.' ) .
			' <a href="' . wp_lostpassword_url() . '">' .
			__( 'Lost your password?' ) .
			'</a>'
        );
    }

    if ($password == "" && $referrer) {
        if ($result) {
            $referrer .= "&password=empty";
        } else {
            $referrer .= "?password=empty";
        }
        $error->add('empty_password', __('<strong>ERROR</strong>: The password field is empty.'));
        $result = true;
    } else {
        if (username_exists($username) || email_exists($username)) {
            $_SESSION['username'] = $username;
            $user = adstm_get_user($username);
            if (!wp_check_password($password, $user->data->user_pass, $user->ID)) {
                $referrer .= "?password=invalid";
                $result = true;
                $error->add(
                    'incorrect_passsword',
                    sprintf(
                        /* translators: %s: user name */
                        __( '<strong>ERROR</strong>: The password you entered for the username %s is incorrect.' ),
                        '<strong>' . $username . '</strong>'
                    ) .
                    ' <a href="' . wp_lostpassword_url() . '">' .
                    __( 'Lost your password?' ) .
                    '</a>'
                );
            }
        }
    }

    if ($result) {
        if (strstr($referrer,'wp-login') || strstr($referrer,'wp-admin')) {
            return $error;
        } else {
            $_SESSION['username'] = $username;
            wp_redirect($referrer);
            exit();
        }
    }

    return $user;
}
add_filter('authenticate', 'verify_username_password', 30, 3);

/**
 * adstm_get_user
 * 
 * @param string $username
 * @return object
 */
function adstm_get_user($username)
{
    $searchField = 'login';
    if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
        $searchField = 'email';
    }

    return get_user_by($searchField, $username);
}

/**
 * adstm_check_username
 * 
 * @param string $username
 * @return boolean
 */
function adstm_check_username($username)
{
    if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
        return !email_exists($username);
    }

    return !username_exists($username);
}

/**
 * add_login_errors
 * 
 * @param type $error
 * @return string
 */
function add_login_errors($error) {
    if (!isset($_GET['login'])) {
        return $error;
    }

    $errors = array(
        'empty'              => __('Invalid username', 'rem'),
        'not_found_username' => __('Username not found', 'rem'),
        'not_found_email'    => __('Email not found', 'rem')
    );

    if (!isset($errors[$_GET['login']])) {
        return '';
    }

    return '<span class="help-block">' . $errors[$_GET['login']] . '</span>'; 
}

add_filter('login_errors', 'add_login_errors');

/**
 * add_password_errors
 * 
 * @param type $error
 * @return string
 */
function add_password_errors($error) {
    if (!isset($_GET['password'])) {
        return $error;
    }

    $errors = array(
        'empty'   => __('Invalid password', 'rem'),
        'invalid' => __('Wrong password', 'rem')
    );

    if (!isset($errors[$_GET['password']])) {
        return '';
    }

    return '<span class="help-block">' . $errors[$_GET['password']] . '</span>';
}

add_filter('password_errors', 'add_password_errors');
<?php
/**
 * Created by PhpStorm.
 * User: sunfun
 * Date: 05.09.16
 * Time: 23:05
 */
?>
<div class="header-top hidden-xs hidden-sm">
	<div class="container">
		<div class="row">
			<?php
			$home = sprintf( '<li class="menu-item menu-item-home %1$s">
					<a href="%2$s"><span class="main-el-icon">%3$s</span></a>
				</li>',
				adstm_is_home() ? 'current-menu-item' : '',
				home_url(),
				__( 'Home', 'rem' )

			);
			$args = array(
				'container'      => false,
				'theme_location' => 'top_menu',
				'menu_class'     => 'top-menu',
				'link_before'    => '<span class="main-el-icon"></span>',
				'depth'          => 2,
				'items_wrap'     => '<ul class="top-menu pull-left" >' . $home . '%3$s</ul>'

			);
			wp_nav_menu( $args );
			?>
			<div class="pull-right">
				<?php adstm_login_button();?>
			</div>
			<div class="pull-right dropdown dropdown_currency">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#" ajax_update="currency"></a>
				<ul class="dropdown-menu" role="menu">
					<?php
					if ( function_exists( 'ads_select_currency' ) ) {
						echo ads_select_currency();
					}
					?>
				</ul>
			</div>
			<div class="contact pull-right">
				<?php if ( cz( 's_mail' ) ): ?>
					<a href="mailto:<?php echo cz( 's_mail' ); ?>" class="email"><?php echo cz( 's_mail' ); ?></a>
				<?php endif; ?>
				<?php if ( cz( 's_phone' ) ): ?>
					<a href="tel:<?php echo cz( 's_phone' ); ?>" class="tel"><?php echo cz( 's_phone' ); ?></a>
				<?php endif; ?>
			</div>

		</div>
	</div>

</div>

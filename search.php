<?php get_header() ?>

	<!-- CONTACT-US -->
	<div class="page-content">
		<div class="container">
			<div class="row">
				<div class="col-md-60 article">
					<div class="p-heading">

					</div>
					<?php global $wp_query; ?>
					<div class="info-result-search"><?php printf( '%1$s <b>%2$s</b> (%3$s)',
							__( 'Search Results for', 'rem' ),
							get_search_query() ,
							$wp_query->found_posts,
							_n( 'result', 'results', $wp_query->found_posts, 'rem' )

						); ?>
					</div>
					<div class="row product-loop">
						<?php while ( have_posts() ) : the_post(); ?>
                            <?php
                            if(isset($_GET['post_type']) && $_GET['post_type'] == 'post') {
                                ?>
                                <div class="item col-md-15">
                                    <?php get_template_part( 'tpl/blog/tpl/_item' ); ?>
                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="item col-md-15">
                                    <?php get_template_part( 'tpl/product/loop/_item' ); ?>
                                </div>
                                <?php
                            }
                            ?>

						<?php endwhile; ?>
					</div>
					<div class="row">
						<?php custom_pagination(); ?>
					</div>
				</div>
			</div>

		</div>
	</div>
	</div>
<?php get_footer() ?>
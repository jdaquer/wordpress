<?php get_header();
$pagename = get_query_var( 'pagename' );

$pagenameTemplates = array(
    'thankyou'         => 'tpl/page/alids/thankyou',
    'cart'             => 'tpl/page/alids/cart2/index',
    'account'          => 'tpl/page/account',
    'orders'           => 'tpl/page/orders',
    'userlogin'        => 'tpl/page/userlogin',
    'register'         => 'tpl/page/register',
    'confirmation'     => 'tpl/page/confirmation',
    'customersgallery' => 'tpl/page/customersgallery'
);

if (isset($pagenameTemplates[$pagename])) {
    get_template_part($pagenameTemplates[$pagename]);
} else {
    get_template_part( 'tpl/page/index' );
}
?>
<?php get_footer(); ?>
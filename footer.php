</div><!--wrap-->
<footer class="footer">
	<?php if ( cz( 'tp_footer_payment_methods' ) || cz( 'tp_footer_delivery_methods' ) ): ?>
		<div class="content-partners">
			<div class="container">
				<?php if ( cz( 'tp_footer_payment_methods' ) ): ?>
					<ul class="col-md-60 col-lg-30 payment_methods">
						<li><span><?php _e( 'Payment methods:', 'rem' ) ?></span></li>
						<li><i class="partner p-master"></i></li>
						<li><i class="partner p-visa"></i></li>
						<li><i class="partner p-wunion"></i></li>
						<li><i class="partner p-maestro"></i></li>
						<li><i class="partner p-paypal"></i></li>
						<li><i class="partner p-discover"></i></li>
						<li><i class="partner p-ae"></i></li>
					</ul>
				<?php endif; ?>
					<ul class="col-md-60 col-lg-30">
						<li><span><?php echo cz( 'tp_confidence' ); ?>:</span></li>
						<?php if ( cz( 'tp_confidence_img_1' ) ): ?>
						<li><i class="partner-confidence" style="background: url(<?php echo cz( 'tp_confidence_img_1' ) ; ?>?1000) center center /cover no-repeat, #fff"></i></li>
						<?php endif; ?>
						<?php if ( cz( 'tp_confidence_img_2' ) ): ?>
						<li><i class="partner-confidence" style="background: url(<?php echo cz( 'tp_confidence_img_2' ); ?>?1000) center center /cover no-repeat, #fff"></i></li>
						<?php endif; ?>
						<?php if ( cz( 'tp_confidence_img_3' ) ): ?>
						<li><i class="partner-confidence" style="background: url(<?php echo cz( 'tp_confidence_img_3' ); ?>?1000) center center /cover no-repeat, #fff"></i></li>
						<?php endif; ?>
					</ul>
			</div>
		</div>
	<?php endif; ?>
	<div class="container">
		<div class="footer-row">
			<div class="col-xs-60 col-md-60 col-lg-15 f-col">
				<div class="hidden-xs hidden-sm logo-box">
					<div class="logo">
						<a href="/"><img src="<?php echo cz( 'tp_logo_img_w' ); ?>?1000" alt=""></a>
					</div>
				</div>
				<div class="contact">
					<?php if ( cz( 's_phone' ) ): ?>
					<div>
						<a href="tel:<?php echo( cz( 's_phone' ) ); ?>" class="tel"><?php echo( cz( 's_phone' ) ); ?></a>
					</div>
					<?php endif; ?>
					<?php if ( cz( 's_mail' ) ): ?>
					<div>
						<a href="mailto:<?php echo( cz( 's_mail' ) ); ?>" class="email"><?php echo( cz( 's_mail' ) ); ?></a>
					</div>
					<?php endif; ?>
				</div>
				<div class="copyright" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
					<div>
						<?php echo str_replace( '{{YEAR}}', date( 'Y' ), cz( 'tp_copyright' ) ); ?></div>
					<div itemprop="streetAddress" class="address"><?php echo cz( 'tp_address' ); ?></div>
					<div><?php echo cz( 'tp_copyright__line' ); ?></div>
				</div>
			</div>
			<div class="col-xs-60 col-md-60 col-lg-14 f-col">
				<?php
				$args = array(
					'container'      => false,
					'theme_location' => 'footer',
					'menu_class'     => 'footer-menu',
					'link_before'    => '<span class="main-el-icon"></span>',
					'depth'          => 1

				);
				wp_nav_menu( $args );
				?>
			</div>
			<div class="col-xs-60 col-md-60 col-lg-16 f-col">
				<?php if ( is_enableSocial() ): ?>
					<div class="stay_connected">
						<div class="f-head"><?php _e( 'Stay connected', 'rem' ); ?>
							<ul class="list-social">
								<?php if ( cz( 's_link_fb' ) ): ?>
									<li>
										<a href="<?php echo cz( 's_link_fb' ); ?>" class="ico-social fb" target="_blank" rel="nofollow"></a>
									</li>
								<?php endif; ?>
								<?php if ( cz( 's_link_in' ) ): ?>
									<li>
										<a href="<?php echo cz( 's_link_in' ); ?>" class="ico-social in" target="_blank" rel="nofollow"></a>
									</li>
								<?php endif; ?>
								<?php if ( cz( 's_link_tw' ) ): ?>
									<li>
										<a href="<?php echo cz( 's_link_tw' ); ?>" class="ico-social tw" target="_blank" rel="nofollow"></a>
									</li>
								<?php endif; ?>
								<?php if ( cz( 's_link_gl' ) ): ?>
									<li>
										<a href="<?php echo cz( 's_link_gl' ); ?>" class="ico-social gp" target="_blank" rel="nofollow"></a>
									</li>
								<?php endif; ?>
								<?php if ( cz( 's_link_pt' ) ): ?>
									<li>
										<a href="<?php echo cz( 's_link_pt' ); ?>" class="ico-social pn" target="_blank" rel="nofollow"></a>
									</li>
								<?php endif; ?>
								<?php if ( cz( 's_link_yt' ) ): ?>
									<li>
										<a href="<?php echo cz( 's_link_yt' ); ?>" class="ico-social yt" target="_blank" rel="nofollow"></a>
									</li>
								<?php endif; ?>
							</ul>
						</div>
					</div>
				<?php endif; ?>
				<?php if ( cz( 'tp_subscribe' ) ): ?>
					<div class="subscription">
						<div class="f-head"><?php _e( 'Subscription', 'rem' ) ?></div>
						<?php echo cz( 'tp_subscribe' ); ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-xs-60 col-md-60 col-lg-15 f-col f-col-4">
				<?php get_template_part( 'tpl/widget/_facebook' ); ?>
			</div>
		</div>
	</div>
</footer><!-- .body-footer -->
<?php
global $message;
printf('<script type="text/html" id="ads-notify"/>%1$s</script>',
	apply_filters('ads_notify', $message)
);
?>

<?php wp_footer(); ?>

<?php echo cz( 'tp_footer_script' ); ?>
</body>
</html>

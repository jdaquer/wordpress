<form method="post" accept-charset="UTF-8">
	<input  type="email" name="email" value="" placeholder="<?php _e( 'Please enter you email', 'rem' ); ?>" required="required">
	<div class="help"><?php _e( 'Register now to get updates on promotions and coupons.', 'rem' ); ?></div>
	<button name="submit" type="submit" value="Submit" class="submit"><?php _e( 'Subscribe', 'rem' ); ?></button>
</form>
<?php

/**
 * Created by PhpStorm.
 * User: sunfun
 * Date: 07.09.16
 * Time: 23:06
 */
class customStyle {

	public function __constructor() {

	}

	static public function generate() {
		init_cz();
		$file = dirname( __FILE__ ) . '/style/style_template.php';
		$style = '';
		if ( file_exists( $file ) ) {
			ob_start();
			include( $file );
			$style = ob_get_contents();
			ob_end_clean();

		}
		@file_put_contents( dirname( __FILE__ ) . '/css/custom_style.css', esc_attr($style) );
	}

}



<?php
/**
 * Autor: Pavel Shishkin
 * Date: 30.06.2016
 * Time: 19:04
 */

return array(
	'tp_head'                => '',
	'tp_style'               => '',


	/*base*/
	'tm_show_blog_comment'   => true,
	'tp_head_ga'             => '',
	'tp_facebook_pixel'      => '',
	'tp_create'              => true,
	'tp_color'               => '#ae322f',
	'tp_discount'            => '#ae322f',
	'tp_color_price_product' => '#ae322f',

	'tp_img_left_cart' => IMG_DIR . 'trustf/sslupf.svg',
	'tp_logo_img'   => IMG_DIR . 'logo.png',
	'tp_logo_img_w' => IMG_DIR . 'logo-w.png',
	'tp_favicon'    => TEMPLATE_DIR . '/favicon.ico',
	's_mail'        => 'support@' . adstm_get_host(),
	's_phone'       => '1 (111) 111 11 11',

	'tp_buttons_colors' => '#ae322f',

	'tp_text_top_header'    => __( 'Free Worldwide Shipping', 'rem' ),
	'tp_show_discount'      => true,

	'tp_buttons_cart'       => '#FF8D38',
	'tp_buttons_cart_hover' => '#FF6E01',
	/*home*/
	'id_video_youtube_home' => 'rsbZbmMk3BY',

	'tp_slider_rotating'     => false,
	'tp_slider_speed'        => 3,

	'slider_home' => array(
		array(
			'img'           => IMG_DIR . 'slider1.jpg',
			'head'          => 'What is Lorem Ipsum?',
			'head_color'    =>'#fff',
			'text'          => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan',
			'text_color'    =>'#fff',
			'shop_now_link' => home_url( '/product/' ),
		),
		array(
			'img'           => IMG_DIR . 'slider2.jpg',
			'head'          => 'What is Lorem Ipsum?',
			'head_color'    =>'#fff',
			'text'          => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan',
			'text_color'    =>'#fff',
			'shop_now_link' => home_url( '/product/' ),
		),
		array(
			'img'           => IMG_DIR . 'slider3.jpg',
			'head'          => 'What is Lorem Ipsum?',
			'head_color'    =>'#fff',
			'text'          => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan',
			'text_color'    =>'#fff',
			'shop_now_link' => home_url( '/product/' ),
		),
	),

	'home_list_product' => array(
		array(
			'img'           => IMG_DIR . 'css/list_product1.jpg',
			'head'          => 'TOP SELLING PRODUCTS',
			'text'          => 'dummy Lorem Ipsum  rem Ipsum industry\'s Ipsum industry\'s stan',
			'shop_now_link' => "#top-selling-product",
		),
		array(
			'img'           => IMG_DIR . 'css/list_product2.jpg',
			'head'          => 'BEST DEALS',
			'text'          => 'dummy Lorem Ipsum  rem Ipsum industry\'s Ipsum industry\'s stan dard dummy',
			'shop_now_link' => "#best-deals",
		),
		array(
			'img'           => IMG_DIR . 'css/list_product3.jpg',
			'head'          => 'NEW ARRIVALS',
			'text'          => 'dummy Lorem Ipsum  rem Ipsum industry\'s Ipsum industry\'s stan dard dummy ',
			'shop_now_link' => '#new-arrivals',
		),
	),


	'features'               => true,
	'tp_color_features'      => '#222',
	'tp_color_head_features' => '#222',
	'features_1_head'        => '<b>700+</b> ' . __( 'Clients Love Us!', 'rem' ),
	'features_1_text'        => __( 'We offer best service and great prices on high quality products', 'rem' ),
	'features_2_head'        => __( 'Shipping to', 'rem' ) . ' <b>185</b> ' . __( 'Countries', 'rem' ),
	'features_2_text'        => __( 'Our store operates worldwide and you can enjoy free delivery of all orders', 'rem' ),
	'features_3_head'        => '<b>100%</b>  ' . __( 'Safe Payment', 'rem' ),
	'features_3_text'        => __( 'Buy with confidence using the world’s most popular and secure payment methods', 'rem' ),
	'features_4_head'        => '<b>2000+</b> ' . __( 'Successful Deliveries', 'rem' ),
	'features_4_text'        => __( 'Our Buyer Protection covers your purchase from click to delivery', 'rem' ),

	'tp_home_article_color'                    => '#fff',
	'tp_home_article'                    => '',
	'tp_home_article_bg'                 => IMG_DIR . 'css/article_bg.jpg',

	/*single product*/
	'tp_shipping_details'                => true,
	'tp_tab_item_specifics'              => true,
	'tp_tab_shipping'                    => true,
	'tp_single_shipping_payment_content' => czOptions::getTemplateField( 'tm_single_shipping_payment_content' ),
	'tp_show_quantity_orders'            => true,
	'tp_pack'                            => false,
	'tp_share'                           => false,
	'tp_comment_flag'                    => true,
	'tp_tab_item_review'                 => true,
	/*cart*/

	'tp_phone_number_required' => false,
	'tp_description_required'  => false,


	'tp_paypal_info_enable' => true,
	'tp_paypal_info_text'   => __( 'You can pay with your credit card without creating a PayPal account', 'rem' ),

	'tp_credit_card_info_enable' => true,
	'tp_credit_card_info_text'   => __( 'All transactions are secure and encrypted. Credit card information is never stored.', 'rem' ),

	'tp_readonly_read_required'            => false,
	'tp_readonly_read_required_text'       => __( 'I have read the', 'rem' ) . ' <a href="' . home_url( '/terms-and-conditions/' ) . '">' . __( 'Terms & Conditions', 'rem' ) . '</a>',


	/*Checkout _sidebar*/
	'sidebar_safe_shopping_guarantee_show' => true,
	'sidebar_safe_shopping_guarantee'      => __( 'SAFE SHOPPING GUARANTEE', 'rem' ),

	'sidebar_safe_shopping_guarantee_img_1' => IMG_DIR . 'trust/mcafee.svg',
	'sidebar_safe_shopping_guarantee_img_2' => IMG_DIR . 'trust/norton.svg',
	'sidebar_safe_shopping_guarantee_img_3' => IMG_DIR . 'trust/truste.svg',

	/*SEO*/

	'tp_home_seo_title'                     => '',
	'tp_home_seo_description'               => '',
	'tp_home_seo_keywords'                  => '',

	'tp_seo_products_title'       => __( 'All products', 'rem' ),
	'tp_seo_products_description' => __( 'All products – choose the ones you like and add them to your shopping cart', 'rem' ),
	'tp_seo_products_keywords'    => '',

	/*about*/
	'tp_about_img'                => IMG_DIR . 'about/bg_about.jpg',
	'tp_about_description'        => '<div class="head">' . __( 'Welcome to', 'rem' ) . ' ' . ucfirst( strtolower( preg_replace( '/\.\w*$/', '', adstm_get_host() ) ) ) . '</div>' .
									'<p>'.__( 'We are a team of enthusiastic developers and entrepreneurs who decided to convert their common experience into this web store. We hope you’ll like it as much as we do and have a great shopping experience here. Our prime goal is to create a shop in which you can easily find whatever product you need.', 'rem' ).'</p>',
	'tp_about_color_b1'           => '#fff',
	'meet_our_team'               => true,
	'tp_about_title'              => __( 'MEET OUR TEAM', 'rem' ),
	'tp_about_text'               => __( 'Our team is made up of experienced developers, designers and marketers who do their best to create the interface comfortable to use. It is vital for us to make your shopping easy and pleasant.', 'rem' ),

	'tp_keep_title' => __( 'Keep Contact with Us', 'rem' ),
	'tp_keep_text'  => __( 'We keep working on our web store and are open for any suggestions. If you have any questions or proposals, please do not hesitate contact us via the links below.', 'rem' ),

	'tp_mt1_img_1'  => IMG_DIR . 'about/1.png',
	'tp_mt1_name_1' => __( 'Veronica Wright', 'rem' ),
	'tp_mt1_cus_1'  => __( 'Marketer', 'rem' ),

	'tp_mt1_img_2'  => IMG_DIR . 'about/2.png',
	'tp_mt1_name_2' => __( 'Nataly Robinson', 'rem' ),
	'tp_mt1_cus_2'  => __( 'Developer', 'rem' ),

	'tp_mt1_img_3'  => IMG_DIR . 'about/3.png',
	'tp_mt1_name_3' => __( 'Fiona Long', 'rem' ),
	'tp_mt1_cus_3'  => __( 'UI Specialist', 'rem' ),

	'tp_mt1_img_4'  => IMG_DIR . 'about/4.png',
	'tp_mt1_name_4' => __( 'Thomas Campbell', 'rem' ),
	'tp_mt1_cus_4'  => __( 'Developer', 'rem' ),

	'tp_mt1_img_5'                  => IMG_DIR . 'about/5.png',
	'tp_mt1_name_5'                 => __( 'Gabriel Henry', 'rem' ),
	'tp_mt1_cus_5'                  => __( 'Web Designer', 'rem' ),

	/*contact Us*/
	'tp_contactUs_text'             => __( 'Have any questions or need to get more information about the product? Either way, you’re in the right spot.', 'rem' ),
	'tp_contactUs_map'              => IMG_DIR . 'map.jpg',
	/*thankyou*/
	'tp_thankyou_fail_no_head_tag'  => '',
	'tp_thankyou_fail_no_head'      => __( 'Thank you for your order!', 'rem' ),
	'tp_thankyou_fail_no_text'      => '<p>' . __( 'Your order was accepted and you will get notification on your email address.', 'rem' ) .
	                                   '</p><p>*' . __( 'Please note that if you’ve ordered more than 2 items, you might not get all of them at the same time due to varying locations of our storehouses.', 'rem' ) . '</p>',
	'tp_thankyou_fail_yes_head_tag' => '',
	'tp_thankyou_fail_yes_head'     => '<p>' . __( 'We are sorry, we were unable to successfully process this transaction.' ) . '</p>',
	'tp_thankyou_fail_yes_text'     => '',

	/*social*/
	's_link_fb'                     => '',
	's_fb_apiid'                    => '',
	's_fb_name_widget'              => '',

	's_link_in'       => '',
	's_in_name_api'   => '',
	's_in_name_group' => '',

	's_link_tw'           => '',
	's_link_gl'           => '',
	's_link_pt'           => '',
	's_link_yt'           => '',

	/*contact form*/
	's_send_mail'         => 'support@' . adstm_get_host(),
	's_send_from'         => 'support@' . adstm_get_host(),

	/*subscribe*/
	'tp_subscribe'        => czOptions::getTemplateField( 'tp_subscribe' ),
	/*footer*/
	'tp_confidence'       => __( 'Buy with confidence', 'rem' ),
	'tp_confidence_img_1' => IMG_DIR . 'trustf/goDaddyf.svg',
	'tp_confidence_img_2' => IMG_DIR . 'trustf/nortonf.svg',
	'tp_confidence_img_3' => IMG_DIR . 'trustf/sslf.svg',

	'tp_copyright'               => '© Copyright {{YEAR}}. All Rights Reserved',
	'tp_address'                 => '111 Your Street, Your City, Your State 11111, Your Country',
	'tp_copyright__line'         => adstm_get_host(),
	'tp_footer_script'           => '',
	'tp_footer_payment_methods'  => true,
	'tp_footer_delivery_methods' => true,

    /*blog*/
	'blog_banner_1' => '<a href="#">
                <img src="'.IMG_DIR . 'blog/no_banner.jpg'.'">
            </a>',
	'blog_banner_mobile_show_1'=> true,
	'blog_banner_mobile_show_2'=> true,
	'blog_banner_2' => '<a href="#">
                <img src="'.IMG_DIR . 'blog/no_banner.jpg'.'">
            </a>',

	'blog_subscribe' => czOptions::getTemplateField( 'blog_subscribe' ),
	'blog_register_html' => czOptions::getTemplateField( 'blog_register_html' ),
);
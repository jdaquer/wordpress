<?php $list_product = cz('home_list_product'); ?>

.home .home-list_page .box.box1{
	background-image: url(<?php echo $list_product[0]['img'] ?>?1000);
}
.home .home-list_page .box.box2{
background-image: url(<?php echo $list_product[1]['img'] ?>?1000);
}
.home .home-list_page .box.box3{
background-image: url(<?php echo $list_product[2]['img'] ?>?1000);
}


/*Template color: tp_color*/
	.features div .text-feat b {
		color: <?php echo cz('tp_color') ?>;
	}
	.header-desc .text-shipping {
		color: <?php echo cz('tp_color') ?>;
	}
	.header-desc .cart a {
		background-color: <?php echo cz('tp_color') ?>;
	}
/*Discount color: tp_discount*/
	.product-item .wrap-img .discount {
	background-color: <?php echo cz('tp_discount') ?>!important;
	}
/*Buttons Colors: tp_buttons_colors*/
	.home .home-slider .info .ft .btn.shop_now {
	background: <?php echo cz('tp_buttons_colors') ?>;

	}
/*Prices color: tp_color_price_product*/
	.product-item .price .sale {
		color: <?php echo cz('tp_color_price_product') ?>!important;
	}
		.single-product .product-price .salePrice .value {
		color: <?php echo cz('tp_color_price_product') ?>;
	}
	.single-product .product-price .youSave .save {
		color: <?php echo cz('tp_color_price_product') ?>;
	}
/*Features images color: tp_color_features*/
/*Features titles color:tp_color_head_features*/
	.features div .text-feat {
		color: <?php echo cz('tp_color_head_features') ?>;
	}

/*Home article*/
.wrap-page-article {
background: url(<?php echo cz('tp_home_article_bg') ?>?1000) center top/cover no-repeat;
}

/*header*/
.header-desc .text-secure {
background: url(<?php echo cz('tp_img_left_cart') ?>?1000) center center/cover no-repeat;
}

/*about*/
.page-about .wrap-b1 p,
.page-about .wrap-b1 .head {
    color: <?php echo cz('tp_about_color_b1') ?>;
}

/****/
.cart2 .btn.btn_orange,
.single-product .modal-addCart .modal-footer .btn,
.single-product .product-main .product-actions .btn.addCart {
    background-color: <?php echo cz( 'tp_buttons_cart' ) ?>;
}

.single-product .product-main .product-actions .btn.addCart:hover {
    border: 1px solid <?php echo cz( 'tp_buttons_cart_hover' ) ?>;
    background-color: <?php echo cz( 'tp_buttons_cart_hover' ) ?>;
}

.single-product .modal-addCart .modal-footer .btn:hover {
    border: 1px solid <?php echo cz( 'tp_buttons_cart_hover' ) ?>;
    color: <?php echo cz( 'tp_buttons_cart_hover' ) ?>;
}

.cart2 .btn.btn_orange:hover{
    background-color: <?php echo cz( 'tp_buttons_cart_hover' ) ?>;
}

.page-article{
color: <?php echo cz( 'tp_home_article_color' ) ?>;
}
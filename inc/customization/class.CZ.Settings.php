<?php

/**
 * Author: Vitaly Kukin
 * Date: 24.09.2014
 * Time: 12:14
 * Description: Show settings from Admin Class
 */
class czSettings extends czAdminTpl {

    function __construct() {
        parent::__construct();
    }

    /**
     * list menu
     * @return array
     */
    public function listMenu() {
        $listMenu = array(
            'czbase' => array(
                'tmp'         => 'tmplGeneral',
                'title'       => __( 'General', 'rem' ),
                'description' => __( 'Some basic configuration settings.', 'rem' ),
                'icon'        => 'tachometer',
                'submenu'     => array()
            ),

            'czhead'          => array(
                'tmp'         => 'tmplHead',
                'title'       => __( 'Head', 'rem' ),
                'description' => __( 'Use this section to add and edit scripts and styles.', 'rem' ),
                'icon'        => 'life-ring',
                'submenu'     => array()
            ),
            'czheader'        => array(
                'tmp'         => 'tmplHeader',
                'title'       => __( 'Header', 'rem' ),
                'description' => __( 'Header main elements settings.', 'rem' ),
                'icon'        => 'cog',
                'submenu'     => array()
            ),
            'czhome'          => array(
                'tmp'         => 'tmplHome',
                'title'       => __( 'Home', 'rem' ),
                'description' => __( 'Home page main settings.', 'rem' ),
                'icon'        => 'home',
                'submenu'     => array()
            ),
            'czsingleproduct' => array(
                'tmp'         => 'tmplSingleProduct',
                'title'       => __( 'Single product', 'rem' ),
                'description' => __( 'Single product page main settings.', 'rem' ),
                'icon'        => 'home',
                'submenu'     => array()
            ),
            'czCart' => array(
                'tmp'         => 'tmplCart',
                'title'       => __( 'Checkout', 'rem' ),
                'description' => __( 'Checkout page settings', 'rem' ),
                'icon'        => 'shopping-cart',
                'submenu'     => array()
            ),
            'czabout'         => array(
                'tmp'         => 'tmplAbout',
                'title'       => __( 'About Us', 'rem' ),
                'description' => __( 'About us page settings.', 'rem' ),
                'icon'        => 'users',
                'submenu'     => array()
            ),
            'czthankyou'      => array(
                'tmp'         => 'tmplThankyou',
                'title'       => __( 'Thank You', 'rem' ),
                'description' => __( 'Thank you page settings.', 'rem' ),
                'icon'        => 'smile-o ',
                'submenu'     => array()
            ),
            'czcontactus'     => array(
                'tmp'         => 'tmplContactUs',
                'title'       => __( 'Contact Us', 'rem' ),
                'description' => __( 'Contact Us', 'rem' ),
                'icon'        => 'smile-o ',
                'submenu'     => array()
            ),
            'czblog'     => array(
                'tmp'         => 'tmplBlog',
                'title'       => __( 'Blog', 'rem' ),
                'description' => __( 'Blog', 'rem' ),
                'icon'        => 'smile-o ',
                'submenu'     => array()
            ),
            'czsocial'        => array(
                'tmp'         => 'tmplSocial',
                'title'       => __( 'Social Media', 'rem' ),
                'description' => __( 'Social media pages integration.', 'rem' ),
                'icon'        => 'hand-o-right ',
                'submenu'     => array()
            ),
            'czsubscribe'     => array(
                'tmp'         => 'tmplSubscribe',
                'title'       => __( 'Subscribe Form', 'rem' ),
                'description' => __( 'Subscription form settings for collecting users’ emails.', 'rem' ),
                'icon'        => 'envelope',
                'submenu'     => array()
            ),
            'czfooter'        => array(
                'tmp'         => 'tmplFooter',
                'title'       => __( 'Footer', 'rem' ),
                'description' => __( 'Footer options and settings.', 'rem' ),
                'icon'        => 'bars',
                'submenu'     => array()
            )		);

        return apply_filters( 'cz_list_menu', $listMenu );
    }

    /**
     * Template Base
     */
    function tmplGeneral() {
        $this->block( $this->checkboxField( 'tp_show_discount', array( 'label' => __('Show discount information in the product list', 'rem') ) ) );
        $this->block(
            $this->checkboxField(
                'tm_show_blog_comment',
                array( 'label' => __('Enable Facebook comments', 'rem') )
            )
        );
        $this->block(
            array(
                $this->Button( 'tp_create', array(
                    'label' => __( 'Add default pages and menus', 'rem' ),
                    'value' => true,
                    'text'  => __( 'Create', 'rem' )
                ) )
            )
        );

        $this->block( $this->row( array(
                $this->textField( 's_mail', array(
                    'label' => __( 'Contact email', 'rem' ),
                ) ),
                $this->textField( 's_phone', array(
                    'label' => __( 'Contact phone', 'rem' ),
                ) )
            )
        ) );

        $this->block(
            array(

                $this->row( array(
                    $this->colorField( 'tp_color', array(
                        'label' => __( 'Template color', 'rem' ),
                    ) )
                ) ),
                $this->row( array(
                    $this->colorField( 'tp_discount', array(
                        'label' => __( 'Discount color', 'rem' ),
                    ) )
                ) ),
                $this->row( array(
                    $this->colorField( 'tp_buttons_colors', array(
                        'label' => __( 'Buttons Colors', 'rem' )
                    ) ),
                ) ),
                $this->row( array(
                    $this->colorField( 'tp_color_price_product', array(
                        'label' => __( 'Prices color', 'rem' ),
                    ) )
                ) )
            )
        );

        $this->block(
            array(

                $this->row( array(
                    $this->colorField( 'tp_buttons_cart', array(
                        'label' => __( 'Buttons Colors', 'ami3' ),
                    ) )
                ) ),
                $this->row( array(
                    $this->colorField( 'tp_buttons_cart_hover', array(
                        'label' => __( 'Buttons Colors (click)', 'ami3' ),
                    ) )
                ) ),
            ),
            array('title'=> __('Add to cart/Proceed to checkout/Proceed to pay buttons', 'ami3'))
        );

        $this->block(
            $this->uploadImgField( 'tp_favicon', array(
                    'label'  => __( 'Favicon png, gif', 'rem' ),
                    'width'  => 16,
                    'height' => 16,
                )
            )
        );

        $this->block(
            array(
                $this->row( array(
                    $this->textField( 'tp_head_ga', array(
                        'label' => __( 'Google Analytics Tracking ID', 'rem' ),
                    ) )
                ) ),
                $this->row( array(
                    $this->textField( 'tp_facebook_pixel', array(
                        'label' => __( 'Facebook Pixel Code', 'rem' ),
                    ) )
                ) )
            )
        );

    }

    /**
     * Template Header
     */
    function tmplHead() {
        $this->block(
            array(
                $this->textTextArea( 'tp_head', array(
                    'label' => __( '< head > tag container for head elements', 'rem' ),
                    'rows'  => 10
                ) ),
                $this->textTextArea( 'tp_style', array(
                    'label' => __( 'css style', 'rem' ),
                    'rows'  => 10
                ) )
            )
        );
    }

    /**
     * Template Header
     */
    function tmplHeader() {

        $this->block(
            array(
                $this->row(
                    array(
                        $this->uploadImgField( 'tp_logo_img', array(
                            'label'  => __( 'Website logo', 'rem' ),
                            'width'  => 177,
                            'height' => 44,
                        ) )
                    ) ),
                $this->row(
                    array(
                        $this->uploadImgField( 'tp_img_left_cart', array(
                            'label'  => __( 'Additional image', 'rem' ),
                            'width'  => 100,
                            'height' => 37,
                            'description' => __('Usually used for trusted seals, payment system logos, etc.', 'rem')
                        ) )
                    ) ),
                $this->row(
                    array(
                        $this->textField( 'tp_text_top_header', array(
                            'label' => __( 'Text', 'rem' ),
                        ) )
                    )
                )
            )
        );

    }

    /**
     * Template Settings
     */
    function tmplHome() {

        $slider_home = $this->slider_home;
	    $foo         = array(
		    $this->checkboxField( 'tp_slider_rotating', array( 'label' => __( 'Rotating', 'rem' ) ) ),
		    $this->textField( 'tp_slider_speed', array(
			    'label'       => __( 'Rotating time', 'rem' ),
			    'description' => __( 'seconds', 'rem' )
		    ) )
	    );
        foreach ( $slider_home as $k => $v ) {
            array_push( $foo,
                $this->row(
                    array(
                        $this->uploadImgField( 'slider_home[' . $k . '][img]', array(
                            'value'  => $v[ 'img' ],
                            'label'  => __( 'image', 'rem' ) . ' - ' . $k,
                            'width'  => 1920,
                            'height' => 420,
                        ) )
                    ) )
            );

            array_push( $foo,
                $this->row( array(
                    $this->textField( 'slider_home[' . $k . '][head]', array(
                        'value'       => $v[ 'head' ],
                        'label'       => __( 'Head', 'rem' ) . ' - ' . $k,
                        'description' => '',
                    ) )
                ) )
            );


            array_push( $foo,
                $this->row( array(
                    $this->colorField( 'slider_home[' . $k . '][head_color]', array(
                        'value'       => $v[ 'head_color' ],
                        'label' => __( 'Head color', 'rem' ),
                    ) )
                ) )
            );

            array_push( $foo,
                $this->row( array(
                    $this->textField( 'slider_home[' . $k . '][text]', array(
                        'value'       => $v[ 'text' ],
                        'label'       => __( 'Text', 'rem' ) . ' - ' . $k,
                        'description' => '',
                    ) )
                ) )
            );

            array_push( $foo,
                $this->row( array(
                    $this->colorField( 'slider_home[' . $k . '][text_color]', array(
                        'value'       => $v[ 'text_color' ],
                        'label' => __( 'Text color', 'rem' ),
                    ) )
                ) )
            );

            array_push( $foo,
                $this->row( array(
                    $this->textField( 'slider_home[' . $k . '][shop_now_link]', array(
                        'value'       => $v[ 'shop_now_link' ],
                        'label'       => __( 'Shop now link', 'rem' ) . ' - ' . $k,
                        'description' => '',
                    ) )
                ) )
            );
        }


        $this->block( $this->textField( 'id_video_youtube_home', array(
            'label'       => __( 'YouTube Video ID', 'rem' ),
            'description' => 'https://www.youtube.com/watch?v=<b>He5yBEmyjTA</b>'
        ) ) );

        $this->block( $foo,
            array( 'title' => 'Slider' )
        );

        $home_list_product = $this->home_list_product;
        $foo         = array();
        foreach ( $home_list_product as $k => $v ) {
            array_push( $foo,
                $this->row(
                    array(
                        $this->uploadImgField( 'home_list_product[' . $k . '][img]', array(
                            'value'  => $v[ 'img' ],
                            'label'  => __( 'image', 'rem' ) . ' - ' . $k,
                            'width'  => $k==1 ? 599 : 291,
                            'height' => 360,
                        ) )
                    ) )
            );

            array_push( $foo,
                $this->row( array(
                    $this->textField( 'home_list_product[' . $k . '][head]', array(
                        'value'       => $v[ 'head' ],
                        'label'       => __( 'Head', 'rem' ) . ' - ' . $k,
                        'description' => '',
                    ) )
                ) )
            );

            array_push( $foo,
                $this->row( array(
                    $this->textField( 'home_list_product[' . $k . '][text]', array(
                        'value'       => $v[ 'text' ],
                        'label'       => __( 'Text', 'rem' ) . ' - ' . $k,
                        'description' => '',
                    ) )
                ) )
            );

            array_push( $foo,
                $this->row( array(
                    $this->textField( 'home_list_product[' . $k . '][shop_now_link]', array(
                        'value'       => $v[ 'shop_now_link' ],
                        'label'       => __( 'Shop now link', 'rem' ) . ' - ' . $k,
                        'description' => '',
                    ) )
                ) )
            );
        }


        $this->block( $foo,
            array( 'title' => __('Products', 'rem') )
        );



        $this->block(
            array(
                $this->checkboxField( 'features', array( 'label' => 'Enable features' ) ),

                $this->colorField( 'tp_color_features', array(
                    'label' => __( 'Features images color', 'rem' ),
                ) ),
                $this->colorField( 'tp_color_head_features', array(
                    'label' => __( 'Features titles color', 'rem' ),
                ) ),
                $this->line(),
                $this->textField( 'features_1_head', array(
                    'label' => __( 'Title 1', 'rem' )
                ) ),
                $this->textTextArea( 'features_1_text', array(
                    'label' => __( 'Description 1', 'rem' )
                ) ),

                $this->line(),

                $this->textField( 'features_2_head', array(
                    'label' => __( 'Title 2', 'rem' )
                ) ),
                $this->textTextArea( 'features_2_text', array(
                    'label' => __( 'Description 2', 'rem' )
                ) ),

                $this->line(),

                $this->textField( 'features_3_head', array(
                    'label' => __( 'Title 3', 'rem' )
                ) ),
                $this->textTextArea( 'features_3_text', array(
                    'label' => __( 'Description 3', 'rem' )
                ) ),

                $this->line(),

                $this->textField( 'features_4_head', array(
                    'label' => __( 'Title 4', 'rem' )
                ) ),
                $this->textTextArea( 'features_4_text', array(
                    'label' => __( 'Description 4', 'rem' )
                ) ),
            ),
            array( 'title' => __('Features', 'rem') )
        );

        $this->block(
            array(
	            $this->colorField( 'tp_home_article_color', array(
		            'label' => __( 'Text color', 'rem' ),
	            ) ),
                $this->tinymce( 'tp_home_article',
                    array(
                        'rows'        => 30,
                        'label'       => 'Article',
                        'description' => __( 'Add 300-500 words SEO article to your home page' )
                    ) ),
                $this->uploadImgField( 'tp_home_article_bg', array(
                    'label'  => __( 'Background', 'rem' ),
                    'width'  => 1920,
                    'height' => 979,
                ) )
            ),
            array( 'title' => __('Article', 'rem') )
        );

    }

    /**
     * Template Social
     */
    function tmplSocial() {

        $this->block( array(

            $this->row( array(
                $this->textField( 's_in_name_group', array(
                    'label' => __( 'Widget title', 'rem' ),
                ) ),

                $this->textField( 's_link_in', array(
                    'label' => __( 'Fan page link', 'rem' ),
                ) ),

                $this->textField( 's_in_name_api', array(
                    'label' => __( 'Username', 'rem' ),
                ) )
            ) )
        ), array( 'title' => __( 'Instagram widget', 'rem' ) ) );

        $this->block( array(

            $this->row( array(
                $this->textField( 's_link_fb', array(
                    'label' => __( 'Fan page link', 'rem' ),
                ) ),
            ) )
        ), array( 'title' => __( 'Facebook widget', 'rem' ) ) );


        $this->block( array(
            $this->row( array(
                $this->textField( 's_link_tw', array(
                    'label' => __( 'Twitter link', 'rem' ),
                ) ),
                $this->textField( 's_link_gl', array(
                    'label' => __( 'Google plus link', 'rem' ),
                ) )
            ) ),
            $this->row( array(
                $this->textField( 's_link_pt', array(
                    'label' => __( 'Pinterest link', 'rem' ),
                ) ),
                $this->textField( 's_link_yt', array(
                    'label' => __( 'YouTube link', 'rem' ),
                ) )
            ) )
        ), array( 'title' => __( 'Social pages links', 'rem' ) ) );

    }

    /**
     * Template Footer
     */
    function tmplFooter() {

        $this->block( array(

            $this->row( $this->checkboxField( 'tp_footer_payment_methods', array(
                'label' => __( 'Payment methods', 'rem' )
            ) ) ),
        ), array( 'title' => __('Methods', 'rem') ) );

        $this->block(
            $this->row(
                $this->uploadImgField( 'tp_logo_img_w', array(
                    'label'  => __( 'Logo', 'rem' ),
                    'width'  => 161,
                    'height' => 56,
                ) )
            )
        );

        $this->block( array(

            $this->row( $this->textField( 'tp_confidence', array(
                'label' => __( 'Confidence', 'rem' )
            ) ) ),
            $this->row(
                array(
                    $this->uploadImgField( 'tp_confidence_img_1', array(
                        'label'  => __( 'Image 1', 'rem' ),
                        'width'  => 159,
                        'height' => 51,
                    ) ),
                    $this->uploadImgField( 'tp_confidence_img_2', array(
                        'label'  => __( 'Image 2', 'rem' ),
                        'width'  => 159,
                        'height' => 51,
                    ) ),
                    $this->uploadImgField( 'tp_confidence_img_3', array(
                            'label'  => __( 'Image 3', 'rem' ),
                            'width'  => 159,
                            'height' => 51,
                        )
                    )
                )
            )
        ), array( 'title' => __( 'Confidence', 'rem' ) ) );


        $this->block(
            array(
                $this->row(
                    $this->textTextArea( 'tp_copyright', array() )
                ),
                $this->row(
                    $this->textTextArea( 'tp_address', array() )
                ),
                $this->row(
                    $this->textTextArea( 'tp_copyright__line', array() )
                )
            ),
            array( 'title' => __( 'Copyright', 'rem' ) )
        );

        $this->block(
            $this->row(
                $this->textTextArea( 'tp_footer_script', array(
                    'label' => __( 'Footer tag container', 'rem' ),
                    'rows'  => 20
                ) )
            )
        );

    }

    /**
     * Template About
     */
    function tmplAbout() {

        $this->block(
            array(
                $this->textTextArea( 'tp_about_description', array( 'label' => __('Description', 'rem'), 'rows' => 5 ) ),
                $this->colorField( 'tp_about_color_b1', array( 'label' => __('Description color', 'rem') ) ),
                $this->uploadImgField( 'tp_about_img', array(
                    'label'  => __( 'Article images', 'rem' ),
                    'width'  => 470,
                    'height' => 249,
                    'align'  => 'center'
                ) ),
                $this->line(),
                $this->checkboxField( 'meet_our_team', array( 'label' => __('Show','rem') ) ),
                $this->textField( 'tp_about_title', array( 'label' => __('Title','rem') ) ),
                $this->textField( 'tp_about_text', array( 'label' => __('Text','rem') ) ),

                $this->textField( 'tp_keep_title', array( 'label' => __('Title','rem') ) ),
                $this->textField( 'tp_keep_text', array( 'label' => __('Text','rem') ) ),

                $this->line(),
                $this->row(
                    array(
                        $this->uploadImgField( 'tp_mt1_img_1', array(
                            'label'  => __( 'Photo', 'rem' ),
                            'width'  => 140,
                            'height' => 140,
                            'align'  => 'center'
                        ) ),
                        $this->col(
                            array(
                                $this->textField( 'tp_mt1_name_1' ),
                                $this->textField( 'tp_mt1_cus_1' )
                            )
                        )
                    )
                ),
                $this->line(),
                $this->row(
                    array(
                        $this->uploadImgField( 'tp_mt1_img_2', array(
                            'label'  => __( 'Photo', 'rem' ),
                            'width'  => 140,
                            'height' => 140,
                            'align'  => 'center'
                        ) ),
                        $this->col(
                            array(
                                $this->textField( 'tp_mt1_name_2' ),
                                $this->textField( 'tp_mt1_cus_2' )
                            )
                        )
                    )
                ),
                $this->line(),
                $this->row(
                    array(
                        $this->uploadImgField( 'tp_mt1_img_3', array(
                            'label'  => __( 'Photo', 'rem' ),
                            'width'  => 188,
                            'height' => 188,
                            'align'  => 'center'
                        ) ),
                        $this->col(
                            array(
                                $this->textField( 'tp_mt1_name_3' ),
                                $this->textField( 'tp_mt1_cus_3' )
                            )
                        )
                    )
                ),
                $this->line(),
                $this->row(
                    array(
                        $this->uploadImgField( 'tp_mt1_img_4', array(
                            'label'  => __( 'Photo', 'rem' ),
                            'width'  => 140,
                            'height' => 140,
                            'align'  => 'center'
                        ) ),
                        $this->col(
                            array(
                                $this->textField( 'tp_mt1_name_4' ),
                                $this->textField( 'tp_mt1_cus_4' )
                            )
                        )
                    )
                ),
                $this->line(),
                $this->row(
                    array(
                        $this->uploadImgField( 'tp_mt1_img_5', array(
                            'label'  => __( 'Photo', 'rem' ),
                            'width'  => 140,
                            'height' => 140,
                            'align'  => 'center'
                        ) ),
                        $this->col(
                            array(
                                $this->textField( 'tp_mt1_name_5' ),
                                $this->textField( 'tp_mt1_cus_5' )
                            )
                        )
                    )
                )
            ),
            array( 'title' => __('MEET OUR TEAM' , 'rem'))
        );
    }

    /**
     * Template Thankyou
     */
    function tmplThankyou() {

        $this->block(
            array(
                $this->textField( 'tp_thankyou_fail_no_head', array(
                    'label' => __( 'Title', 'rem' ),
                    'rows'  => 10
                ) ),
                $this->textTextArea( 'tp_thankyou_fail_no_text', array(
                    'label' => __( 'Text', 'rem' ),
                    'rows'  => 10
                ) ),
                $this->textTextArea( 'tp_thankyou_fail_no_head_tag', array(
                    'label' => __( 'Conversion tracking script', 'rem' ),
                    'rows'  => 10
                ) ),
            ),
            array(
                'title' => __('Payment success', 'rem')
            )
        );

        $this->block(
            array(
                $this->textField( 'tp_thankyou_fail_yes_head', array(
                    'label' => __( 'Title', 'rem' ),
                    'rows'  => 10
                ) ),
                $this->textTextArea( 'tp_thankyou_fail_yes_text', array(
                    'label' => __( 'Text', 'rem' ),
                    'rows'  => 10
                ) ),
                $this->textTextArea( 'tp_thankyou_fail_yes_head_tag', array(
                    'label' => __( "Conversion tracking script", 'rem' ),
                    'rows'  => 10
                ) ),
            ),
            array(
                'title' => __('Payment fail', 'rem')
            )
        );
    }


    /**
     * Template Subscribe
     */
    function tmplSubscribe() {

        $this->block(
            $this->row(
                $this->textTextArea( 'tp_subscribe', array(
                    'label' => __( 'Paste your ‘Autoresponder’ code here', 'rem' ),
                    'rows'  => 20
                ) )
            )
        );
    }

    function tmplContactUs() {

        $this->block(
            array(
                $this->row(
                    $this->textTextArea( 'tp_contactUs_text', array(
                        'label' => __( 'Paste your ‘Autoresponder’ code here', 'rem' ),
                        'rows'  => 20
                    ) )
                ),
                $this->row(
                    $this->uploadImgField( 'tp_contactUs_map', array(
                        'label'  => __( 'Image map', 'rem' ),
                        'width'  => 672,
                        'height' => 420,
                    ) )
                )
            )
        );
    }

    function tmplBlog() {

        $this->block(
            array(
                $this->checkboxField( 'blog_banner_mobile_show_1', array( 'label' => __('Show on mobile','rem') ) ),
                $this->row(
                    $this->textTextArea( 'blog_banner_1', array(
                        'label' => __( '', 'rem' ),
                        'rows'  => 5
                    ) )
                ),

            )
        );
        $this->block(
            array(
                $this->checkboxField( 'blog_banner_mobile_show_2', array( 'label' => __('Show on mobile','rem') ) ),
                $this->row(
                    $this->textTextArea( 'blog_banner_2', array(
                        'label' => __( '', 'rem' ),
                        'rows'  => 5
                    ) )
                ),

            )
        );

        $this->block(
            array(
                $this->row(
                    $this->textTextArea( 'blog_subscribe', array(
                        'label' => __( 'Subscription form settings for collecting users’ emails.', 'rem' ),
                        'rows'  => 20
                    ) )
                ),

            )
        );

        $this->block(
            array(
                $this->row(
                    $this->textTextArea( 'blog_register_html', array(
                        'label' => __( '', 'rem' ),
                        'rows'  => 20
                    ) )
                ),

            )
        );
    }



    function tmplSingleProduct() {
        $this->block( $this->checkboxField( 'tp_pack', array( 'label' => __('Enable Packaging block','rem') ) ) );
        $this->block( $this->checkboxField( 'tp_share', array( 'label' => __( 'Enable share button', 'rem' ) ) ) );
        $this->block( $this->checkboxField( 'tp_comment_flag', array( 'label' => __('Enable review flag','rem') ) ) );
        $this->block( $this->checkboxField( 'tp_show_quantity_orders', array( 'label' => __('Enable quantity of orders','rem') ) ) );
        $this->block( $this->checkboxField( 'tp_tab_item_specifics', array( 'label' => __('Enable tab item specifics','rem') ) ) );
        $this->block( $this->checkboxField( 'tp_tab_item_review', array( 'label' => __('Enable tab item review','rem') ) ) );
	    $this->block( $this->checkboxField( 'tp_tab_shipping', array( 'label' => __( 'Enable tab shipping and free returns', 'mic' ) ) ) );
        $this->block( $this->checkboxField( 'tp_shipping_details', array( 'label' => __('Show shipping details on product page','rem') ) ) );
        $this->block( $this->textTextArea( 'tp_single_shipping_payment_content',
            array(
                'label' => __('Shipping & Payment','rem'),
                'rows'  => 20
            ) ) );
    }

    /**
     * Template Settings
     */
    function tmplCart() {
        $this->block( array(
            $this->checkboxField( 'tp_phone_number_required', array( 'label' => __('Phone number field required','rem')  ) ),
            $this->checkboxField( 'tp_description_required', array( 'label' => __('Description field required','rem') ) ),
        ) ,array('title'=> 'Fields options'));

        $this->block( array(
            $this->checkboxField( 'tp_readonly_read_required', array( 'label' => __('Enabled','rem') ) ),
            $this->textTextArea( 'tp_readonly_read_required_text', array( 'label' => __('Text','rem')) ),
        ) ,array('title'=> __('Terms & Conditions Checkbox','rem')));

        $this->block( array(
            $this->checkboxField( 'tp_paypal_info_enable', array( 'label' => __('Show the message on your checkout page','rem') ) ),
            $this->textField( 'tp_paypal_info_text', array( 'label' => __('Additional information for PayPal payment method','rem') ) ),
            $this->line(),
            $this->checkboxField( 'tp_credit_card_info_enable', array( 'label' => __('Show the message on your checkout page','rem') ) ),
            $this->textField( 'tp_credit_card_info_text', array( 'label' => __('Additional information for Credit Cards payment method','rem') ) ),
        ) ,array('title'=> 'Additional information'));

        $this->block( array(
            $this->checkboxField( 'sidebar_safe_shopping_guarantee_show', array( 'label' => __( 'Enable SHOP WITH CONFIDENCE section', 'rem' ) ) ),
            $this->textField( 'sidebar_safe_shopping_guarantee', array( 'label' => __( 'Title', 'rem' ) ) ),
            $this->row(
                array(
                    $this->uploadImgField( 'sidebar_safe_shopping_guarantee_img_1', array(
                        'label'  => __( 'Image 1', 'rem' ),
                        'width'  => 139,
                        'height' => 48,
                    ) ),
                    $this->uploadImgField( 'sidebar_safe_shopping_guarantee_img_2', array(
                        'label'  => __( 'Image 2', 'rem' ),
                        'width'  => 139,
                        'height' => 48,
                    ) ),
                    $this->uploadImgField( 'sidebar_safe_shopping_guarantee_img_3', array(
                            'label'  => __( 'Image 3', 'rem' ),
                            'width'  => 139,
                            'height' => 48,
                        )
                    )
                )
            )
        ), array( 'title' => __( 'Sidebar', 'rem' ) ) );
    }

}

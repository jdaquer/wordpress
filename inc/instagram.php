<?php

/**
 * User: sunfun
 * Date: 03.09.16
 * Time: 22:44
 */
class adstm_instagram {

	private   $userName;

	const ID_WIDGET = 'ADS_WIDGET_INSTAGRAM';

	public function __construct( $userName ) {
		if(!empty($userName))
			$this->userName = $userName;
	}


	/**
	 * @return array|bool
	 */
	public function params() {
		$params = $this->getCache();

		if ( $params ) {
			return $params;
		}

		$params = $this->getParams();

		if($params){
			$this->setCache( $params );
		}

		return $params;
	}

	private function getParams(){
		$html   = $this->loadIframe();
		return $this->parseParams( $html );

	}

	private function getCache() {
		return get_transient(self::ID_WIDGET);
	}

	private function setCache($params){
		return set_transient( self::ID_WIDGET, $params, 24*HOUR_IN_SECONDS );
	}

	static public function clearCache(){
		delete_transient(self::ID_WIDGET);
	}

	private function parseParams( $html ) {

		$foo = array(
			'images'    => array(),
			'photos'    => false,
			'followers' => false,
		);

		if ( preg_match_all( '/ <td>\s*<a[^<>]*>\s*<img src="([^<>]*)">\s*<\/a>\s*<\/td>/Uiu', $html, $match ) ) {
			$foo[ 'images' ] = $match[ 1 ];
			if ( preg_match( '/<\/i>&nbsp;([\d,]*)&nbsp;photos/', $html, $match ) ) {
				$foo[ 'photos' ] = $match[ 1 ];
			}

			if ( preg_match( '/<\/i>&nbsp;([\d,]*)&nbsp;followers/', $html, $match ) ) {
				$foo[ 'followers' ] = $match[ 1 ];
			}


			return $foo;
		}

		return false;
	}

	private function loadIframe() {
		$response = wp_remote_get( 'https://pro.iconosquare.com/widget/gallery?choice=myfeed&username=' . $this->userName . '&hashtag=iconosquare&linking=instagram&show_infos=true&mode=grid' );
		if ( is_array( $response ) && ! is_wp_error( $response ) ) {
			return $response[ 'body' ];
		}

		return false;
	}
}
<?php
/**
 * Author: Vitaly Kukin
 * Date: 14.10.2016
 * Time: 10:16
 */

function rem_updparam() {
    return 'https://alidropship.com/?theme_update=rembrandt';
}

/**
 * Take over the update check
 *
 * @param $checked_data
 *
 * @return mixed
 */
function rem_check_for_update( $checked_data ) {

    global $wp_version;

    if( function_exists('wp_get_theme') ) {
        $theme_data = wp_get_theme(get_option('template'));
        $theme_version = $theme_data->Version;
    }
    else {
        $theme_data = wp_get_theme();
        $theme_version = $theme_data->get( 'Version' );
    }

    $theme_base = get_option('template');

    $api_url = rem_updparam();

    $request = array(
        'slug' => $theme_base,
        'version' => $theme_version
    );
    // Start checking for an update
    $send_for_check = array(
        'body' => array(
            'action' => 'basic_check',
            'request' => serialize($request),
            'api-key' => md5(get_bloginfo('url'))
        ),
        'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
    );
    $raw_response = wp_remote_post($api_url, $send_for_check);

    $response = '';

    if (!is_wp_error($raw_response) && ($raw_response['response']['code'] == 200))
        $response = @unserialize($raw_response['body']);

    // Feed the update data into WP updater
    if (!empty($response))
        $checked_data->response[$theme_base] = (array)$response;

    return $checked_data;
}
add_filter('pre_set_site_transient_update_themes', 'rem_check_for_update');


/**
 * Take over the Theme info screen
 *
 * @param $def
 * @param $action
 * @param $args
 *
 * @return bool|mixed|WP_Error
 */
function rem_theme_api_call( $def, $action, $args ) {

    global $wp_version;

    if( function_exists('wp_get_theme') ) {
        $theme_data = wp_get_theme(get_option('template'));
        $theme_version = $theme_data->Version;
    }
    else {
        $theme_data = wp_get_theme();
        $theme_version = $theme_data->get( 'Version' );
    }

    $theme_base = get_option('template');

    $api_url = rem_updparam();

    if ($args->slug != $theme_base)
        return false;

    $args->slug      = $theme_base;
    $args->version   = $theme_version;
    $args->site      = get_bloginfo( 'url' );

    $request_string = array(
        'body'        => array(
            'action'  => $action,
            'request' => serialize( $args )
        ),
        'user-agent'  => 'WordPress/' . $wp_version . '; ' . get_bloginfo( 'url' )
    );

    $request = wp_remote_post($api_url, $request_string);

    if (is_wp_error($request)) {
        $res = new WP_Error('themes_api_failed',
            __('<p>An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a></p>', 'rem'), $request->get_error_message());
    } else {
        $res = unserialize($request['body']);

        if ($res === false)
            $res = new WP_Error('themes_api_failed', __('An unknown error occurred', 'rem'), $request['body']);
    }

    return $res;
}
add_filter('themes_api', 'rem_theme_api_call', 10, 3);
<?php
/**
 * Created by PhpStorm.
 * User: sunfun
 * Date: 29.08.16
 * Time: 15:55
 */


function rem_theGallery( $items, $alt = '' ) {

	if ( ! $items || count( $items ) == 0 ) { ?>
		<div class="product-slider">
		</div>
		<?php
		return false;
	}

	?>
	<div class="product-slider">

		<div class="img-main slider-for">
			<?php

			foreach ( $items as $k => $item ) {
				printf(
					'<div><div class="img-wrap"><div class="img-content"><img class="img-responsive" data-zoom-image="%1$s" src="%1$s" alt=""/></div></div></div>',
					$item[ 'full' ],
					$item[ 'ads-large' ]
				);
			}
			?>
		</div>
		<ul class="thumb-list">
			<?php

			foreach ( $items as $k => $item ) {
				printf(
					'<li class="item-touch" data-image="%2$s">
							<div class="img"><img src="%1$s" alt="%2$s" data-zoom-image="%3$s" class="img-responsive" /></div>
							</li>',
					$item[ 'ads-large' ],
					$alt,
					$item[ 'full' ]
				);
			}
			?>
		</ul>
	</div>
	<?php
}

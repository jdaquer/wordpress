<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 09.09.2015
 * Time: 8:51
 */

function rem_create_pages() {

    global $wp_rewrite;

    if ( isset( $_POST[ 'tp_create' ] ) && $_POST[ 'tp_create' ] == true && is_admin() ) {
        update_option( 'posts_per_rss', 20 );
        update_option( 'posts_per_page', 20 );
        update_option( 'show_on_front', 'page' );
        update_option( 'comments_per_page', 50 );
        update_option( 'page_comments', 1 );
        //$wp_rewrite->set_permalink_structure( '/%category%/%postname%/' );
        $wp_rewrite->set_permalink_structure( '/%postname%/' );

        $page = new rem_PageTemplate();
        $page->addPage( array( 'title' => __('Home', 'rem'), 'post_name' => 'home', 'static' => 'front' ) );
        $page->addPage( array( 'title' => __('Blog', 'rem'), 'post_name' => 'blog', 'static' => 'posts' ) );

        $page->addPage( array( 'title' => __('Subscription', 'rem'), 'post_name' => 'subscription' ) );
        $page->addPage( array( 'title' => __('Thank you', 'rem'), 'post_name' => 'thank-you-contact' ) );
        $page->addPage( array( 'title' => __('Payment methods', 'rem'), 'post_name' => 'payment-methods' ) );
        $page->addPage( array( 'title' => __('Shipping & delivery', 'rem'), 'post_name' => 'shipping-delivery' ) );
        $page->addPage( array( 'title' => __('About us', 'rem'), 'post_name' => 'about-us' ) );
        $page->addPage( array( 'title' => __('Contact us', 'rem'), 'post_name' => 'contact-us' ) );
        $page->addPage( array( 'title' => __('Privacy policy', 'rem'), 'post_name' => 'privacy-policy' ) );
        $page->addPage( array( 'title' => __('Terms and conditions', 'rem'), 'post_name' => 'terms-and-conditions' ) );
        $page->addPage( array( 'title' => __('Refunds & Returns Policy', 'rem'), 'post_name' => 'refund-policy' ) );
        $page->addPage( array( 'title' => __('Frequently asked questions', 'rem'), 'post_name' => 'frequently-asked-questions' ) );
        $page->addPage( array( 'title' => __('Track your order', 'rem'), 'post_name' => 'track-your-order' ) );
        $page->create();

        $memu   = array();
        $memu[] = array( 'title' => __( 'Products', 'rem' ), 'url' => '/product/' );
        $memu[] = array( 'title' => __( 'Shipping', 'rem' ), 'url' => '/shipping-delivery/' );
        $memu[] = array( 'title' => __( 'Returns', 'rem' ), 'url' => '/refund-policy/' );
        $memu[] = array( 'title' => __( 'About', 'rem' ), 'url' => '/about-us/' );
        $memu[] = array( 'title' => __( 'Tracking', 'rem' ), 'url' => '/track-your-order/' );
        $memu[] = array( 'title' => __( 'Contact', 'rem' ), 'url' => '/contact-us/' );
        createMemu( $memu, __('Top Menu', 'rem'), 'top_menu' );

        $memu   = array();
        $memu[] = array( 'title' => __( 'About us', 'rem' ), 'url' => '/about-us/' );
        $memu[] = array( 'title' => __( 'Privacy policy', 'rem' ), 'url' => '/privacy-policy/' );
        $memu[] = array( 'title' => __( 'Terms and conditions', 'rem' ), 'url' => '/terms-and-conditions/' );
        $memu[] = array( 'title' => __( 'Payment methods', 'rem' ), 'url' => '/payment-methods/' );
        $memu[] = array( 'title' => __( 'Shipping & delivery', 'rem' ), 'url' => '/shipping-delivery/' );
        $memu[] = array( 'title' => __( 'Returns Policy', 'rem' ), 'url' => '/refund-policy/' );
        $memu[] = array( 'title' => __( 'Contact us', 'rem' ), 'url' => '/contact-us/' );
        $memu[] = array( 'title' => __( 'Frequently asked questions', 'rem' ), 'url' => '/frequently-asked-questions/' );
        createMemu( $memu, __('Footer', 'rem'), 'footer' );

        $memu   = array();
        $memu[] = array( 'title' => __( 'Costumes', 'rem' ), 'url' => '/costumes/', 'slug' => 'costumes' );
        $memu[] = array( 'title' => __( 'Custom category', 'rem' ), 'url' => '/custom-category/', 'slug' => 'custom-category' );
        $memu[] = array( 'title' => __( 'Gifts', 'rem' ), 'url' => '/gifts/', 'slug' => 'gifts' );
        $memu[] = array( 'title' => __( 'Jewelry', 'rem' ), 'url' => '/jewelry/', 'slug' => 'jewelry' );
        $memu[] = array( 'title' => __( 'Men clothing', 'rem' ), 'url' => '/men-clothing/', 'slug' => 'men-clothing' );
        $memu[] = array( 'title' => __( 'Phone cases', 'rem' ), 'url' => '/phone-cases/', 'slug' => 'phone-cases' );
        $memu[] = array( 'title' => __( 'Posters', 'rem' ), 'url' => '/posters/', 'slug' => 'posters' );
        $memu[] = array( 'title' => __( 'T-shirts', 'rem' ), 'url' => '/t-shirts/', 'slug' => 't-shirts' );
        $memu[] = array( 'title' => __( 'Toys', 'rem' ), 'url' => '/toys/', 'slug' => 'toys' );
        $memu[] = array( 'title' => __( 'Women clothing', 'rem' ), 'url' => '/women-clothing/', 'slug' => 'women-clothing' );
        add_action( 'init', 'createCategoryProduct', 10, $memu );

    }
}
add_action('admin_init', 'rem_create_pages');

//@TODO
function createCategoryProduct()
{
	$category   = array();
	$category[] = array( 'title' => __( 'Costumes', 'rem' ), 'url' => '/costumes/', 'slug' => 'costumes' );
	$category[] = array( 'title' => __( 'Custom category', 'rem' ), 'url' => '/custom-category/', 'slug' => 'custom-category' );
	$category[] = array( 'title' => __( 'Gifts', 'rem' ), 'url' => '/gifts/', 'slug' => 'gifts' );
	$category[] = array( 'title' => __( 'Jewelry', 'rem' ), 'url' => '/jewelry/', 'slug' => 'jewelry' );
	$category[] = array( 'title' => __( 'Men clothing', 'rem' ), 'url' => '/men-clothing/', 'slug' => 'men-clothing' );
	$category[] = array( 'title' => __( 'Phone cases', 'rem' ), 'url' => '/phone-cases/', 'slug' => 'phone-cases' );
	$category[] = array( 'title' => __( 'Posters', 'rem' ), 'url' => '/posters/', 'slug' => 'posters' );
	$category[] = array( 'title' => __( 'T-shirts', 'rem' ), 'url' => '/t-shirts/', 'slug' => 't-shirts' );
	$category[] = array( 'title' => __( 'Toys', 'rem' ), 'url' => '/toys/', 'slug' => 'toys' );
	$category[] = array( 'title' => __( 'Women clothing', 'rem' ), 'url' => '/women-clothing/', 'slug' => 'women-clothing' );
	foreach ( $category as $key => $item ) {
		wp_insert_term(
			$item[ 'title' ],
			'product_cat',
			array(
				'description' => '',
				'slug'        => $item[ 'slug' ],
				'parent'      => 0
			)
		);

	}
}

function createMemu( $memu, $menu_name, $location = false )
{
	$menu_exists = wp_get_nav_menu_object( $menu_name );
	if ( !$menu_exists ) {
		$menu_id = wp_create_nav_menu( $menu_name );

		if ( $location ) {
			$locations              = get_theme_mod( 'nav_menu_locations' );
			$locations[ $location ] = $menu_id;
			set_theme_mod( 'nav_menu_locations', $locations );
		}

		foreach ( $memu as $key => $item ) {
			wp_update_nav_menu_item( $menu_id, 0, array(
				'menu-item-title'    => $item[ 'title' ],
				'menu-item-position' => $key,
				'menu-item-url'      => home_url( $item[ 'url' ] ),
				'menu-item-status'   => 'publish' ) );
		}

		return true;
	}

	return false;
}

class rem_PageTemplate
{
	private
		$_pages = array();

	function __construct()
	{
	}

	/**
	 * @param $page
	 * @throws Exception
	 */
	public function addPage( $page )
	{

		if ( empty( $page[ 'post_name' ] ) )
			throw new Exception( 'no post_name' );

		$page[ 'content' ] = $this->getContent( $page[ 'post_name' ] );

		array_push( $this->_pages, $page );
	}

	/**
	 * @return int|WP_Error
	 */
	public function create()
	{
		foreach ( $this->_pages as $page ) {

			$new_page = array(
				'post_type'    => 'page',
				'post_title'   => $page[ 'title' ],
				'post_name'    => $page[ 'post_name' ],
				'post_content' => $page[ 'content' ],
				'post_status'  => 'publish',
				'post_author'  => 1,
			);

			if ( !$this->issetPage( $page[ 'post_name' ] ) ) {
				$id = wp_insert_post( $new_page );
				if ( isset( $page[ 'static' ] ) && $page[ 'static' ] == 'front' ) update_option( 'page_on_front', $id );
				if ( isset( $page[ 'static' ] ) && $page[ 'static' ] == 'posts' ) update_option( 'page_for_posts', $id );
			}
		}
	}

	/**
	 * @param $slug
	 * @return bool
	 */
	private function issetPage( $slug )
	{
		$args       = array(
			'pagename'           => $slug
		);

		$page_check = new WP_Query( $args );
		if ( $page_check->post ) return true;

		return false;
	}

	/**
	 * @param $pagename
	 * @return mixed|string
	 */
	private function getContent( $pagename )
	{
		$file = dirname( __FILE__ ) . '/pages_default/' . $pagename . '.php';
		if ( file_exists( $file ) ) {
			ob_start();
			include( $file );
			$text = ob_get_contents();
			ob_end_clean();

			return $text;
		}

		return '';
	}
}
<?php
/**
 * Author: Vitaly Kukin
 * Date: 04.06.2015
 * Time: 10:38
 */

/**
 * Handler for contact form
 */
function rem_handler_contact() {

	if ( ! isset( $_POST[ 'contactSender' ] ) ) {
		return;
	}

	$foo = array(

		'nameClient' => 'strip_tags',
		'email'      => 'is_email',
		'message'    => 'strip_tags'
	);

	$args  = array();
	$error = false;

	foreach ( $foo as $key => $val ) {

		if ( $error ) {
			break;
		}

		if ( ! isset( $_POST[ $key ] ) ) {
			$error = $key;
		} else {

			$result = call_user_func( $val, trim( $_POST[ $key ] ) );

			if ( $result && ! empty( $result ) ) {
				$args[ $key ] = $result;
			} else {
				$error = $key;
			}
		}
	}

	if ( $error ) {
		$_POST[ 'error' ] = $error;
	} else {

		$defaults = array(
			'nameClient' => '',
			'email'      => '',
			'phone'      => '',
			'location'   => '',
			'message'    => ''
		);

		$args = wp_parse_args( $args, $defaults );

		extract( $args, EXTR_SKIP );

		$defaults = array(
			'adminMailSend' => '',
			'template'      => '',
			'subject'       => '',
			'from_email'    => '',
			'from_name'     => ''
		);

		$options = new \ads\adsOptions();
		$argsNotifi = $options->get('ads_notifi_contact');
		$argsNotifi['template'] = trim(stripcslashes($argsNotifi['template']));

		foreach ( $args as $k => $v ) {
			$argsNotifi[ 'template' ] = str_replace( '{{' . $k . '}}', esc_attr($v), $argsNotifi[ 'template' ] );
		}

		if(empty($argsNotifi['template'])){

			$argsNotifi['template']     = "
                User Name: " . $args[ 'nameClient' ] . "\r\n
                Email: " . $args[ 'email' ] . "\r\n\n
                Message\r\n
                " . $args[ 'message' ] . "\r\n
            ";

		}

		rem_sendmail(
			array(
				'email_to'   => array( $argsNotifi[ 'adminMailSend' ] ),
				'subject'    => $argsNotifi[ 'subject' ],
				'content'    => $argsNotifi[ 'template' ],
				'from_email' => $argsNotifi[ 'from_email' ],
				'from_name'  => $argsNotifi[ 'from_name' ]
			)
		);

		wp_redirect( home_url( '/thank-you-contact' ) );
		exit();
	}
}

add_action( 'wp', 'rem_handler_contact' );


function rem_sendmail( $ms ) {
	$sm = \SendMail\SendMail::i();

	return $sm->send( array(
		'to'         => $ms[ 'email_to' ],
		'subject'    => $ms[ 'subject' ],
		'html'       => $ms[ 'content' ],
		'from_email' => $ms[ 'from_email' ],
		'from_name'  => $ms[ 'from_name' ]
	) );
}

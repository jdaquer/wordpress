<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 28.12.2015
 * Time: 15:49
 */
?>
<p><?php _e( 'Payment methods include PayPal and Credit cards.', 'rem' ); ?></p>
<p><?php _e( 'PayPal is a safer, easier way to send and receive money online. When you select PayPal as the payment method, you will be linked to the PayPal site where you can make payment.', 'rem' ); ?></p>
<p><?php _e( 'PayPal can be used to purchase items by Credit Card (Visa, MasterCard, Discover, and American Express), Debit Card, or E-check (i.e. using your regular Bank Account).', 'rem' ); ?></p>
<p>1) <?php _e( 'After viewing your items on your shopping cart page, you can click and check out with PayPal. Then you will leave our site and enter PayPal’s website.', 'rem' ); ?><br>
	2) <?php _e( 'You can sign in to your PayPal account, or you can create a new one if you haven’t got one.', 'rem' ); ?><br>
	3) <?php _e( 'You can use the PayPal as you want according to the on-screen instructions.', 'rem' ); ?></p>
<p><?php _e( 'Usually, PayPal e-check will take 3-5 business days to be confirmed by PayPal.', 'rem' ); ?></p>
<p><?php _e( 'The reasons why we suggest you use PayPal:', 'rem' ); ?><br>
	<?php _e( 'Payment is traceable. By using your PayPal account, you can trace the status of your payment.', 'rem' ); ?><br>
	<?php _e( 'When you make payment for your order, you don’t need to use your credit card online (you can transfer directly from your bank account). When you use your credit card through PayPal, nobody will see your credit card number, which will minimize the risk of unauthorized use.', 'rem' ); ?></p>


<h3><?php _e( 'Section 1 – what do we do with your information?', 'rem' ); ?></h3>
<p><?php _e( 'When you purchase something from our store, as part of the buying and selling process, we collect the personal information you give us such as your name, address and email address.', 'rem' ); ?></p>
<p><?php _e( 'When you browse our store, we also automatically receive your computer’s internet protocol (IP) address in order to provide us with information that helps us learn about your browser and operating system.', 'rem' ); ?></p>
<p><?php _e( 'Email marketing: With your permission, we may send you emails about our store, new products and other updates.', 'rem' ); ?></p>
<h3><?php _e( 'Section 2 – consent', 'rem' ); ?></h3>
<p><?php _e( 'How do you get my consent?', 'rem' ); ?></p>
<p><?php _e( 'When you provide us with personal information to complete a transaction, verify your credit card, place an order, arrange for a delivery or return a purchase, we imply that you consent to our collecting it and using it for that specific reason only.', 'rem' ); ?></p>
<p><?php _e( 'If we ask for your personal information for a secondary reason, like marketing, we will either ask you directly for your expressed consent, or provide you with an opportunity to say no.', 'rem' ); ?><br>
	<?php _e( 'How do I withdraw my consent?', 'rem' ); ?></p>
<p><?php _e( 'If after you opt-in, you change your mind, you may withdraw your consent for us to contact you, for the continued collection, use or disclosure of your information, at anytime, by contacting us at&nbsp;', 'rem' ); ?>contact@<?php echo adstm_get_host(); ?></p>
<h3><?php _e( 'Section 3 – disclosure', 'rem' ); ?></h3>
<p><?php _e( 'We may disclose your personal information if we are required by law to do so or if you violate our Terms of Service.', 'rem' ); ?><br>
	<?php _e( 'Payment:', 'rem' ); ?><br>
	<?php _e( 'All direct payment gateways adhere to the standards set by PCI-DSS as managed by the PCI Security Standards Council, which is a joint effort of brands like Visa, MasterCard, American Express and Discover.', 'rem' ); ?></p>
<p><?php _e( 'PCI-DSS requirements help ensure the secure handling of credit card information by our store and its service providers.', 'rem' ); ?></p>
<h3><?php _e( 'Section 4 – third-party services', 'rem' ); ?></h3>
<p><?php _e( 'In general, the third-party providers used by us will only collect, use and disclose your information to the extent necessary to allow them to perform the services they provide to us.', 'rem' ); ?></p>
<p><?php _e( 'However, certain third-party service providers, such as payment gateways and other payment transaction processors, have their own privacy policies in respect to the information we are required to provide to them for your purchase-related transactions.', 'rem' ); ?></p>
<p><?php _e( 'For these providers, we recommend that you read their privacy policies so you can understand the manner in which your personal information will be handled by these providers.', 'rem' ); ?></p>
<p><?php _e( 'In particular, remember that certain providers may be located in or have facilities that are located in a different jurisdiction than either you or us. So if you elect to proceed with a transaction that involves the services of a third-party service provider, then your information may become subject to the laws of the jurisdiction(s) in which that service provider or its facilities are located.', 'rem' ); ?></p>
<p><?php _e( 'As an example, if you are located in Canada and your transaction is processed by a payment gateway located in the United States, then your personal information used in completing that transaction may be subject to disclosure under United States legislation, including the Patriot Act.', 'rem' ); ?></p>
<p><?php _e( 'Once you leave our store’s website or are redirected to a third-party website or application, you are no longer governed by this Privacy Policy or our website’s Terms of Service.', 'rem' ); ?></p>
<h3><?php _e( 'Section 5 – security', 'rem' ); ?></h3>
<p><?php _e( 'To protect your personal information, we take reasonable precautions and follow industry best practices to make sure it is not inappropriately lost, misused, accessed, disclosed, altered or destroyed.', 'rem' ); ?></p>
<p><?php _e( 'If you provide us with your credit card information, the information is encrypted using secure socket layer technology (SSL) and stored with a AES-256 encryption. Although no method of transmission over the Internet or electronic storage is 100% secure, we follow all PCI-DSS requirements and implement additional generally accepted industry standards.', 'rem' ); ?></p>
<h3><?php _e( 'Section 6 – age of consent', 'rem' ); ?></h3>
<p><?php _e( 'By using this site, you represent that you are at least the age of majority in your state or province of residence and you have given us your consent to allow any of your minor dependents to use this site.', 'rem' ); ?></p>
<h3><?php _e( 'Section 7 – changes to this privacy policy', 'rem' ); ?></h3>
<p><?php _e( 'We reserve the right to modify this privacy policy at any time, so please review it frequently. Changes and clarifications will take effect immediately upon their posting on the website. If we make material changes to this policy, we will notify you here that it has been updated, so that you are aware of what information we collect, how we use it, and under what circumstances, if any, we use and/or disclose it.', 'rem' ); ?></p>
<p><?php _e( 'If our store is acquired or merged with another company, your information may be transferred to the new owners so that we may continue to sell products to you.', 'rem' ); ?></p>
<h3><?php _e( 'Questions and contact information', 'rem' ); ?></h3>
<p><?php _e( 'If you would like to: access, correct, amend or delete any personal information we have about you, register a complaint, or simply want more information contact our Privacy Compliance Officer at&nbsp;', 'rem' ); ?>contact@<?php echo adstm_get_host(); ?></p>
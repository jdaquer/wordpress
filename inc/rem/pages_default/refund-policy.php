<h3><?php _e( 'Order cancellation', 'rem' ); ?></h3>

<p><?php _e( 'All orders can be cancelled until they are shipped. If your order has been paid and you need to make a change or cancel an order, you must contact us within 12 hours. Once the packaging and shipping process has started, it can no longer be cancelled.', 'rem' ); ?></p>

<h3><?php _e( 'Refunds', 'rem' ); ?></h3>

<p><?php _e( 'Your satisfaction is our #1 priority. Therefore, if you’d like a refund you can request one no matter the reason.', 'rem' ); ?></p>

<p><?php _e( 'If you did', 'rem' ); ?> <b><?php _e( 'not', 'rem' ); ?></b> <?php _e( 'receive the product within the guaranteed time( 45 days not including 2-5 day processing) you can request a refund or a reshipment.', 'rem' ); ?>
	<?php _e( 'If you received the wrong item you can request a refund or a reshipment.', 'rem' ); ?>
	<?php _e( 'If you do not want the product you’ve received you may request a refund but you must return the item at your expense and the item must be unused.', 'rem' ); ?>
</p>

<ul>
	<li><?php _e( 'Your order did not arrive due to factors within your control (i.e. providing the wrong shipping address)', 'rem' ); ?></li>
	<li><?php _e( 'Your order did not arrive due to exceptional circumstances outside the control of', 'rem' ); ?> <a class="q" href="/"><?php echo get_bloginfo('name');?></a> <?php _e( '(i.e. not cleared by customs, delayed by a natural disaster)', 'rem' ); ?>.</li>
	<li><?php _e( 'Other exceptional circumstances outside the control of', 'rem' ); ?> <span class="q"><?php echo adstm_get_host(); ?></span></li>
</ul>

<p class="info">*<?php _e( 'You can submit refund requests within 15 days after the guaranteed period for delivery (45 days) has expired. You can do it by sending a message on', 'rem' ); ?> <a href="<?php echo esc_url(home_url('/contact-us/'))?>"><?php _e('Contact Us', 'rem');?></a> <?php _e( 'page', 'rem' ); ?>.
</p>
<p><?php _e( 'If you are approved for a refund, then your refund will be processed, and a credit will automatically be applied to your credit card or original method of payment, within 14 days.', 'rem' ); ?></p>

<h3>Exchanges</h3>
<p><?php _e( 'If for any reason you would like to exchange your product, perhaps for a different size in clothing. You must contact us first and we will guide you through the steps.', 'rem' ); ?></p>
<p><?php _e( 'Please do not send your purchase back to us unless we authorise you to do so.', 'rem' ); ?></p>


<p><?php _e( 'We are proud to offer international shipping services that currently operate in over 200 countries and islands world wide.  Nothing means more to us than bringing our customers great value and service.  We will continue to grow to meet the needs of all our customers, delivering a service beyond all expectation anywhere in the world.', 'ads' ); ?></p>
<h3><?php _e( 'How do you ship packages?', 'rem' ); ?></h3>

<p><?php _e( 'Packages from our warehouse in China will be shipped by ePacket or EMS depending on the weight and size of the product. Packages shipped from our US warehouse are shipped through USPS.', 'rem' ); ?></p>
<h3><?php _e( 'Do you ship worldwide?', 'rem' ); ?></h3>

<p><?php _e( 'Yes. We provide free shipping to over 200 countries around the world. However, there are some locations we are unable to ship to. If you happen to be located in one of those countries we will contact you.', 'rem' ); ?></p>
<h3><?php _e( 'What about customs?', 'rem' ); ?></h3>

<p><?php _e( 'We are not responsible for any custom fees once the items have shipped. By purchasing our products, you consent that one or more packages may be shipped to you and may get custom fees when they arrive to your country.', 'rem' ); ?></p>
<h3><?php _e( 'How long does shipping take?', 'rem' ); ?></h3>

<?php _e( 'Shipping time varies by location. These are our estimates', 'rem' ); ?>:
<div class="table-responsive">
	<table class="table table-striped">
		<thead>
		<tr>
			<th><?php _e( 'Location', 'rem' ); ?></th>
			<th>*<?php _e( 'Estimated Shipping Time', 'rem' ); ?></th>
		</tr>
		</thead>
        <tr>
            <td><?php _e( 'United States', 'rem' ); ?></td>
            <td>10-20 <?php _e( 'Business days', 'rem' ); ?></td>
        </tr>
        <tr>
            <td><?php _e( 'Canada, Europe', 'rem' ); ?></td>
            <td>10-20 <?php _e( 'Business days', 'rem' ); ?></td>
        </tr>
        <tr>
            <td><?php _e( 'Australia, New Zealand', 'rem' ); ?></td>
            <td>10-30 <?php _e( 'Business days', 'rem' ); ?></td>
        </tr>
        <tr>
            <td><?php _e( 'Mexico, Central America, South America', 'rem' ); ?></td>
            <td>15-30 <?php _e( 'Business days', 'rem' ); ?></td>
        </tr>
        <tr>
            <td><?php _e( 'Asia', 'rem' ); ?></td>
            <td>10-20 <?php _e( 'Business days', 'rem' ); ?></td>
        </tr>
        <tr>
            <td><?php _e( 'Africa', 'rem' ); ?></td>
            <td>15-45 <?php _e( 'Business days', 'rem' ); ?></td>
        </tr>
	</table>
	<span class="info">*<?php _e( 'This doesn’t include our 2-5 day processing time.', 'rem' ); ?></span>
</div>

<h3><?php _e( 'Do you provide tracking information?', 'rem' ); ?></h3>

<p><?php _e( 'Yes, you will receive an email once your order ships that contains your tracking information. If you haven’t received tracking info within 5 days, please contact us.', 'rem' ); ?></p>
<h3><?php _e( 'My tracking says “no information available at the moment”.', 'rem' ); ?></h3>

<p><?php _e( 'For some shipping companies, it takes 2-5 business days for the tracking information to update on the system.', 'rem' ); ?>
	<?php _e( 'If your order was placed more than 5 business days ago and there is still no information on your tracking number, please contact us.', 'rem' ); ?>
</p>
<h3><?php _e( 'Will my items be sent in one package?', 'rem' ); ?></h3>

<p><?php _e( 'For logistical reasons, items in the same purchase will sometimes be	sent in separate packages, even if you\'ve specified combined shipping.', 'rem' ); ?></p>

<p><?php _e( 'If you have any other questions, please contact us and we will do our best to help you out.', 'rem' ); ?></p>
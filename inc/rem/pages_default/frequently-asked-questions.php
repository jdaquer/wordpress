<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 28.12.2015
 * Time: 16:05
 */
?>
        <p><h3><?php _e( 'How to search products?', 'rem' ); ?></h3></p>
		<p><?php _e( 'Search for products by entering the product name or keyword into the', 'rem' ); ?> <em><?php _e( 'Search Bar', 'rem' ); ?></em> <?php _e( 'at the top of any page. Try to enter a general description. The more keywords you use, the less products you will get in the results page. When you find a product you’re interested in, simply click the product name or the product image for more details.', 'rem' ); ?></p>
		<p><h3><?php _e( 'How are shipping costs calculated?', 'rem' ); ?></h3></p>
		<p><?php _e( 'Shipping costs are calculated based on shipping method (air, sea or land) and product weight / volume. Different shipping companies have different rates, so it’s best to check and compare which is most affordable and economical. For more details on how shipping costs are calculated, please', 'rem' ); ?> <u><a href="/contact-us/"><?php _e( 'contact us', 'rem' ); ?></a></u> <?php _e( 'directly', 'rem' ); ?>.</p>
		<p><h3><?php _e( 'What is Buyer Protection?', 'rem' ); ?></h3></p>
		<p><?php _e( 'Buyer Protection is a set of guarantees that enables buyers to shop with confidence on our website.', 'rem' ); ?></p>
		<p><?php _e( 'You are protected when:', 'rem' ); ?></p>
		<ul>
			<li><?php _e( 'The item you ordered did not arrive within the time promised by the seller.', 'rem' ); ?></li>
			<li><?php _e( 'The item you received was not as described.', 'rem' ); ?></li>
			<li><?php _e( 'The item you received that was assured to be genuine was fake.', 'rem' ); ?></li>
		</ul>



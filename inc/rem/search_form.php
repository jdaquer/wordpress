<?php
/**
 * Search form hook
 *
 * @param $form
 *
 * @return string
 */
function rem_search_form( $form ) {

	ob_start();

	?>
	<form role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ) ?>">
		<input type="text" value="<?php echo get_search_query() ?>" name="s" id="s"/>
		<button type="submit" id="searchsubmit"><span class="fa fa-search"></span></button>
	</form>

	<?php

	$return = ob_get_contents();
	ob_end_clean();

	return $return;
}

add_filter( 'get_search_form', 'rem_search_form' );

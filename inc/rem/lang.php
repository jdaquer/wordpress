<?php
function rem_lang_init() {

	load_theme_textdomain('rem', get_template_directory() . '/lang');
}
add_action( 'init', 'rem_lang_init' );
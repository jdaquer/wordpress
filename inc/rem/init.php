<?php
if ( is_admin() ) {
	include( __DIR__ . '/create_page_template.php' );
}

include( __DIR__ . '/search_form.php' );
include( __DIR__ . '/pagination.php' );
include( __DIR__ . '/contact_form.php' );
include( __DIR__ . '/review.php' );
/*single*/
include( __DIR__ . '/gallery.php' );
include( __DIR__ . '/sku.php' );
/**
 * Register primary menu
 */
register_nav_menus( array(
	'top_menu' => 'Top Menu',
	'footer'  => 'Footer Menu',
	'сategory' => 'Category menu',
	//	'footer1'  => 'Footer 1',
	//	'footer2'  => 'Footer 2',
	//	'footer3'  => 'Footer 3'
) );


/**
 * Add theme support for Featured Images
 */
//add_image_size( 'big-blog', 824, 349, true );
//add_image_size( 'min-blog', 285, 177, true );
add_theme_support( 'post-thumbnails' );

/**
 * Enqueue script
 */
function rem_enqueue_script() {
	wp_register_script( 'bootstrap3', get_template_directory_uri() . '/js/bootstrap.min.js',
		array(
			'jquery'
		), '1.0', true );

	/**
	 * init single.php blog
	 * */

	// Yandex Share
	wp_register_script( 'ya.shims', '//yastatic.net/es5-shims/0.0.2/es5-shims.min.js', null, '0.0.2', true );
	wp_register_script( 'ya.share', '//yastatic.net/share2/share.js', array( 'ya.shims' ), '2.0.0', true );
	// Facebook SDK
	wp_register_script( 'facebook', sprintf( '//connect.facebook.net/%1$s/sdk.js#xfbml=1&version=v2.5&appId=1049899748393568', get_bloginfo( 'language' ) ), array(), '2.5.0', true );

	wp_register_script('baguetteBox', get_template_directory_uri() . '/js/baguetteBox.js', array(), '1.0', true);
    wp_register_script('baguetteInit', get_template_directory_uri() . '/js/baguetteInit.js', array('baguetteBox'), '1.0', true);
	/**/
	wp_register_script( 'home', get_template_directory_uri() . '/js/home.js', array(
		'jquery'
	), '1.0', true );
	wp_register_script( 'core-tmpl', get_template_directory_uri() . '/js/core.js', array(
		'jquery'
	), '1.0', true );

	wp_register_script( 'rem', get_template_directory_uri() . '/js/script.js', array(
		'jquery',
		'core-tmpl',
		'bootstrap3',
		'front-cart'
	), '1.0', true );

	wp_localize_script( 'rem', 'alidAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

	wp_localize_script( 'rem', 'micLang',
		rem_shippingList() );

	wp_localize_script( 'rem', 'tmplLang',
		array(
			'readonly_checkbox' => __('Please tick on Terms & Conditions box', 'mic')
		));

	wp_localize_script( 'rem', 'adstmCustomize',
		array(
			'sliderRotating' => cz( 'tp_slider_rotating' ),
			'sliderSpeed' => cz( 'tp_slider_speed' )
		));


	wp_localize_script( 'rem', 'ADSCacheCurrency', currencyInfo() );

	wp_enqueue_script( 'rem' );

	if ( is_front_page() ) {
		wp_enqueue_script('home');
	}

	$pagename = get_query_var( 'pagename' );
	if ( $pagename == 'cart' ) {
		wp_enqueue_script( 'front-abandoned' , '', '', '', true);
		wp_enqueue_script( 'front-coupon', '', '', '', true );
		wp_enqueue_script( 'front-validateForm', '', '', '', true );
        do_action('rem_enable_checkout');
	}

	wp_enqueue_script( 'facebook');

	if ( is_singular( 'product' ) ) {
		wp_enqueue_script( 	'ya.share');
		wp_enqueue_script( 	'baguetteInit');
        wp_enqueue_script('front-add-review');
	} elseif(is_singular()) {
		wp_enqueue_script( 	'ya.share');
	}

    if ($pagename == 'account') {
        wp_enqueue_script('front-account', '', '', '', true);
    }

    if ($pagename == 'userlogin') {
        wp_enqueue_script('front-userlogin', '', '', '', true);
    }

    if ($pagename == 'register') {
        wp_enqueue_script( 'front-recaptcha-script' );
        wp_enqueue_script('front-register-account', '', '', '', true);
    }

    if ($pagename == 'orders') {
        wp_enqueue_script('front-pagination', '', '', '', true);
        wp_enqueue_script('front-orders', '', '', '', true);
    }

    if ($pagename == 'customersgallery') {
        if (get_site_option('adsgallery_status', 0) == 1) {
            wp_enqueue_script('front-adsgallery', '', '', '', true);
        }
    }
}

add_action( 'wp_enqueue_scripts', 'rem_enqueue_script' );

/**
 * Enable checkout
 * 
 * @return boolean
 */
function rem_enable_checkout()
{
    $getaways = ads_list_getaways();

    if (!is_array($getaways)) {
        return false;
    }

    if (!isset($getaways['cc'])) {
        return false;
    }

    if (!isset($getaways['cc']['type'])) {
        return false;
    }

    if ($getaways['cc']['type'] == 'squareup') {
        wp_enqueue_script('squareup', 'https://js.squareup.com/v2/paymentform');
        wp_localize_script(
            'front-squareup',
            'squareapp',
            array(
                'applicationid' => ads_ccard_settings()['applicationId'],
                'card_number'   => __('Card number', 'rem'),
                'cvv'           => __('CVV', 'rem'),
                'expdate'       => __('MM / YY', 'rem'),
                'postalcode'    => __('Postal code', 'rem')
            )
        );

        wp_enqueue_script('front-squareup');
        return true;
    }

    wp_enqueue_script('front-checkout');
    return true;
}

add_action('rem_enable_checkout', 'rem_enable_checkout');

function rem_shippingList() {

	if(function_exists('ads_get_shipping_titles')){
		return ads_get_shipping_titles();
	}

	return array(
		'free'          => __( 'FREE', 'rem' ),
		'super_savings' => __( 'Super savings', 'rem' ),
		'standard'      => __( 'Standard', 'rem' ),
		'expedited'     => __( 'Expedited', 'rem' ),
	);
}

/**
 * Filter to excerpt
 *
 * @param $length
 *
 * @return int
 */
function rem_excerpt_length( $length ) {
	return 50;
}

add_filter( 'excerpt_length', 'rem_excerpt_length' );

/**
 * Filter to name pages
 *
 * @param $pagename
 *
 * @return string
 */
add_filter( 'ads_template_pagename', function ( $pagename ) {
	return 'alids';
} );

/**
 * Excerpt after text
 *
 * @param $more
 *
 * @return string
 */
function rem_excerpt_more( $more ) {
	return "...";
}

add_filter( 'excerpt_more', 'rem_excerpt_more' );

/**
 * @return array
 */
function rem_listGateway() {
	$foo      = array();
	$gateways = array();

	if ( function_exists( 'ads_list_getaways' ) ) {
		$gateways = ads_list_getaways();
	}

	if ( $gateways ) {
		foreach ( $gateways as $k => $v ) {
			$foo[ $k ] = array(
				'title'  => $v[ 'title' ],
				'type'   => $v[ 'type' ],
				'logo'   => get_template_directory_uri() . '/img/payments/' . $k . '.jpg',
				'fields' => array()
			);
		}
	}

	return $foo;
}

function rem_isExpressCheckoutEnabled() {

	if ( ! function_exists( 'isExpressCheckoutEnabled' ) ) {
		return false;
	}

	return isExpressCheckoutEnabled();
}

function rem_isPromocodesEnabled() {
	if ( ! function_exists( 'isPromocodesEnabled' ) ) {
		return false;
	}

	return isPromocodesEnabled();
}


function rem_get_list_contries( $selected = '' ) {
	if ( function_exists( 'ads_get_list_contries' ) ) {
		ads_get_list_contries( $selected );
	}
}

/**
 * Get count items in Basket
 *
 * @return int
 */
function rem_quantityOrders() {
	global $adsBasket;

	return $adsBasket->countItems();
}

function currencyInfo(){

	$listCurrency = ads_get_list_currency();
	$ADS_CUVALUE = unserialize(ADS_CUVALUE);

	$foo['list_currency'] = $listCurrency;
	$foo['ADS_CUVALUE'] = $ADS_CUVALUE;
	$foo['ADS_CUVAL'] = ADS_CUVAL;
	$foo['ADS_CUR'] = ADS_CUR;
	return $foo;
}
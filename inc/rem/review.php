<?php
/**
 * Created by PhpStorm.
 * User: sunfun
 * Date: 07.09.16
 * Time: 9:21
 */
if(!function_exists('list_review')){
	function list_review( $comment, $args, $depth ) {
        $images = maybe_unserialize($comment->images);
        $size = 'ads-thumb';
        $gallery = \ads\adsPost::get_gallery($images, $size);

        if (!$gallery) {
	            $gallery = array();
	        }
		?>
		<tr itemscope itemtype="http://schema.org/Review" <?php comment_class('feedback-one'); ?> id="li-comment-<?php comment_ID() ?>">
			<td class="star-text" itemprop="author" itemscope itemtype="http://schema.org/Person">
				<?php printf( '<span class="name" itemprop="name">%1$s</span>', $comment->comment_author); ?>
				<?php if($comment->flag AND cz('tp_comment_flag')){
					printf( '<img src="%1$s"/>',  pachFlag( $comment->flag ) . '?1000' );
				} ?>
			</td>
			<td class="star-text">
					<span itemprop="itemReviewed" itemscope itemtype="http://schema.org/Thing">
					<meta itemprop="name" content="<?php the_title(); ?>"/>
					</span>

				<div class="stars" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
					<meta itemprop="ratingValue" content="<?php echo $comment->star; ?>"/>
					<meta itemprop="bestRating" content="5.0"/>
					<?php  adsFeedBackTM::renderStarRating( $comment->star ); ?>
				</div>
			</td>
			<td class="feedback" itemprop="reviewBody">
				<?php
				printf( '<div class="date">%1$s</div><p class="text">%2$s</p>', date_i18n( 'j M Y H:i', strtotime( $comment->comment_date ) ), $comment->comment_content ); ?>
                <div class="gallery">
                    <?php foreach ($gallery as $image):?>
	                        <a href="<?php echo $image['url']?>">
		                            <img src="<?php echo $image[$size];?>" >
		                        </a>
	                    <?php endforeach; ?>
                </div>
			</td>
		</tr>
		<?php
	}
}


if(!function_exists('rem_comment_form_add')){
	function rem_comment_form_add( $post_id = null, $args = array() ) {
		global $id;

		if ( null === $post_id ) {
			$post_id = $id;
		} else {
			$id = $post_id;
		}

		$commenter     = wp_get_current_commenter();
		$user          = wp_get_current_user();
		$user_identity = $user->exists() ? $user->display_name : '';
		$req           = get_option( 'require_name_email' );
		$aria_req      = ( $req ? " aria-required='true'" : '' );
		$fields        = array(
			'author' => '<label for="author" class="col-sm-10 control-label">' . __( 'Name (required)', 'rem' ) . '</label>
						<div class="col-sm-offset-1 col-sm-49"><input class="form-control" id="author" name="author" type="text"
						value="' . esc_attr( $commenter[ 'comment_author' ] ) . '" ' . $aria_req . '/></div>',
			'email'  => '<label for="email" class="col-sm-10 control-label">' . __( 'Email (required)', 'rem' ) . '</label>
						<div class="col-sm-offset-1 col-sm-49"><input class="form-control" id="email" name="email" type="text"
						value="' . esc_attr( $commenter[ 'comment_author_email' ] ) . '" ' . $aria_req . '/></div>',
		);

		$defaults = array(
			'fields'               => apply_filters( 'comment_form_default_fields', $fields )
			,
			'comment_field'        => '<label for="comment" class="col-sm-10 control-label">' . __( 'Comment', 'rem' ) . '</label>
                                <div class="col-sm-offset-1 col-sm-49">
                                    <textarea class="form-control" name="comment" id="comment" aria-required="true" rows="8"></textarea>
                                    </div>'
			,
			'must_log_in'          => '<p>' . sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.', 'rem' ),
					wp_login_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>'
			,
			'logged_in_as'         => '<p>' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>', 'rem' ),
					admin_url( 'profile.php' ), $user_identity,
					wp_logout_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>'
			,
			'comment_notes_before' => ''
			,
			'comment_notes_after'  => ''
			,
			'id_form'              => 'commentform'
			,
			'id_submit'            => 'submit'
			,
			'title_reply'          => '<h2>' . __( "Leave a Review", 'rem' ) . '</h2>'
			,
			'title_reply_to'       => __( "Leave a Reply to %s", 'rem' )
			,
			'cancel_reply_link'    => __( "Cancel reply", 'rem' )
			,
			'label_submit'         => __( "Send", 'rem' )
		);
		$args     = wp_parse_args( $args, apply_filters( 'comment_form_defaults', $defaults ) );

		if ( comments_open( $post_id ) ) :

			do_action( 'comment_form_before' );

			comment_form_title( $args[ 'title_reply' ], $args[ 'title_reply_to' ] ); ?>
			<small><?php cancel_comment_reply_link( $args[ 'cancel_reply_link' ] ); ?></small>

			<?php

			if ( get_option( 'comment_registration' ) && ! is_user_logged_in() ) :

				echo $args[ 'must_log_in' ];

				do_action( 'comment_form_must_log_in_after' );

			else :

				?>

				<form class="form-horizontal" action="<?php echo site_url( '/wp-comments-post.php' ); ?>"
				      method="post" id="<?php echo esc_attr( $args[ 'id_form' ] ); ?>">

					<?php
					do_action( 'comment_form_top' );

					if ( is_user_logged_in() ) :

						echo apply_filters( 'comment_form_logged_in', $args[ 'logged_in_as' ], $commenter, $user_identity );

						do_action( 'comment_form_logged_in_after', $commenter, $user_identity );

					else :

						echo $args[ 'comment_notes_before' ];

						do_action( 'comment_form_before_fields' );

						foreach ( (array) $args[ 'fields' ] as $name => $field ) {
							echo '<div class="form-group">' . apply_filters( "comment_form_field_{$name}", $field ) . '</div>';
						}

						do_action( 'comment_form_after_fields' );

					endif;

					?>
					<div class="form-group">
						<label for="comment" class="col-sm-10 control-label"><?php _e( 'Star' ); ?></label>
						<div class="col-sm-offset-1 col-sm-49">
							<?php for ( $i = 5; $i > 0; $i -- ) {
								printf( '<label class="checkbox-inline"><input type="radio" name="star" value="%1$s" class="widefat"  %2$s/> %1$s</label>',
									$i,
									0
								);
							}
							?>
						</div>
					</div>
					<div class="form-group">
						<label for="comment" class="col-sm-10 control-label"></label>
						<div class="col-sm-offset-1 col-sm-49">
							<select name="flag" id="" class="form-control">
								<?php
								if( function_exists('ads_shipping_countries') ) foreach ( ads_shipping_countries() as $k => $v ) {
									printf( '<option value="%1$s" %3$s>%2$s</option>',
										$k,
										$v,
										0
									);
								}
								?>
							</select>
						</div>
					</div>
					<?php
					echo '<div class="form-group">' . apply_filters( 'comment_form_field_comment', $args[ 'comment_field' ] ) . '</div>';
					?>

					<div class="form-group">
						<div class="col-sm-offset-11 col-sm-49">
							<input name="submit" type="submit" class="btn btn-large"
							       id="<?php echo esc_attr( $args[ 'id_submit' ] ); ?>"
							       value="<?php echo esc_attr( $args[ 'label_submit' ] ); ?>"/>
						</div>
					</div>

					<?php
					echo $args[ 'comment_notes_after' ];

					comment_id_fields( $post_id );

					do_action( 'comment_form', $post_id );

					?>

				</form>

				<?php

			endif;

			do_action( 'comment_form_after' );

		else :

			do_action( 'comment_form_comments_closed' );

		endif;
	}
}
<?php

/**
 * Pagination
 */
function custom_pagination($isPageOf = false) {
	global $wp_query;
	$big = 999999999; // need an unlikely integer
	$total = $wp_query->max_num_pages;
	$paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
	$pages = paginate_links( array(
		'base'               => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format'             => '?page=%#%',
		'total'              => $total,
		'current'            => max( 1, $paged ),
		// 'show_all'           => true,
		// 'end_size'           => 1,
		// 'mid_size'           => 2,
		'prev_next'          => true,
		'prev_text'          => '<i class="fa fa-caret-left"></i>',
		'next_text'          => '<i class="fa fa-caret-right"></i>',
		'type'               => 'array',
		// 'add_args'           => false,
		// 'add_fragment'       => '',
		// 'before_page_number' => '',
		// 'after_page_number'  => '',
	) );

	if( is_array( $pages ) ) {

		echo '<div class="wrap-pagination">';
		echo '<ul class="pagination">';

		if ( $isPageOf ) {
			$keyFirst = 0;
			$keyLast = count($pages) - 1;

			// var_dump( $pages[$keyFirst] );
			// var_dump( $pages[$keyLast] );

			if ( is_numeric($pages[$keyFirst]) ) {
				echo "<li>$pages[$keyFirst]</li>";
			}

			printf( '<li><span>%1$s %3$d %2$s %4$d</span></li>', __('Page'), __('of'), $paged, $total );

			if ( is_numeric($pages[$keyLast]) ) {
				echo "<li>$pages[$keyLast]</li>";
			}

		} else {

			foreach ( $pages as $page ) {
				echo "<li>$page</li>";
			}

		}

		echo '</ul>';
		echo '</div>';
	}
}
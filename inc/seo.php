<?php
/**
 * The title meta tag to page
 */

function rem_single_term() {

	$title = single_term_title( '', false );

	$other_title = get_query_var( 'other_title', false );

	return ( $other_title ) ? $other_title : $title;
}

function rem_title() {

	global $page, $paged, $wp, $post;
	$title = '';
	$tPage = '';

	if ( $paged >= 2 || $page >= 2 ) {
		$tPage = ' - ' . sprintf( __( 'Page %s', 'rem' ), max( $paged, $page ) );
	}

	if ( adstm_is_home() ) {
		$title = cz( 'tp_home_seo_title' );
		echo $title . $tPage;

		return;
	}

	if ( is_single() && get_post_type() == 'product' ) {
		$obj = new adsProductTM();
		$obj->getById( $post->ID );
		$title = $obj->getSeoTitle();
		if ( ! $title ) {
			$nameProduct = single_post_title( '', false );
			$title       = $nameProduct . ' - ' . __( 'free shipping worldwide', 'rem' );
		}
		echo $title . $tPage;

		return;
	}

	if ( is_tax( 'product_cat' ) ) {

		$title = get_query_var( 'seo_title' );

		if ( $title ) {
			echo $title;

			return;
		}

		$nameCat = rem_single_term();

		$title = __( 'Online shopping for ', 'rem' ) . $nameCat . ' ' . __( 'with free worldwide shipping', 'rem' );
		echo $title . $tPage;

		return;
	}

	if ( ! is_single() && ! is_tax( 'product_cat' ) && get_post_type() == 'product' ) {
		$title = cz( 'tp_seo_products_title' );
		echo $title . $tPage;

		return;
	}

	$slug = isset( $wp->query_vars[ "pagename" ] ) ? $wp->query_vars[ "pagename" ] : '';

	switch ( $slug ) {
		case 'about-us':
			$title = __( 'About us', 'rem' );
			break;
		case 'cart':
			$title = __( 'Your shopping cart', 'rem' );
			break;
		case 'contact-us':
			$title = __( 'Contact us', 'rem' );
			break;
		case 'frequently-asked-questions':
			$title = __( 'FAQ – popular questions answered', 'rem' );
			break;
		case 'payment-methods':
			$title = __( 'Payment methods available for you' );
			break;
		case 'shipping-delivery':
			$title = __( 'Our shipping methods and terms', 'rem' );
			break;
		case 'subscription':
			$title = __( 'Thank you for subscription!', 'rem' );
			break;
		case 'thankyou-contact':
			$title = __( 'Thank you for contacting us!', 'rem' );
			break;
	}

	if ( $title ) {
		echo $title . $tPage;

		return;
	}

	wp_title( '|', true, 'right' );

	$site_name = get_bloginfo( 'name' );
	if ( $site_name ) {
		echo " $site_name" . $tPage;
	}

}

/**
 *
 */
function rem_description() {
	global $page, $paged, $wp, $post;
	$tPage = '';

	if ( $paged >= 2 || $page >= 2 ) {
		$tPage = ' - ' . sprintf( __( 'Page %s', 'rem' ), max( $paged, $page ) );
	}
	$description = '';

	if ( adstm_is_home() ) {
		$description = cz( 'tp_home_seo_description' );
		echo $description . $tPage;

		return;
	}

	if ( is_single() && get_post_type() == 'product' ) {
		$obj = new adsProductTM();
		$obj->getById( $post->ID );
		$description = $obj->getSeoDescription();
		if ( ! $description ) {
			$nameProduct = single_post_title( '', false );
			$description = __( 'Buy', 'rem' ) . ' ' . $nameProduct . ' ' . __( 'at', 'rem' ) . ' ' . adstm_get_host() . '! ' .
			               __( 'Free shipping to 185 countries. 45 days money back guarantee.', 'rem' );
		}
		echo $description . $tPage;

		return;
	}

	if ( is_tax( 'product_cat' ) ) {

		$description = get_query_var( 'seo_description' );
		if ( $description ) {
			echo $description;

			return;
		}
		$nameCat     = rem_single_term();
		$description = __( 'Great selection of', 'rem' ) . ' ' . $nameCat . ' ' .
		               __( 'at affordable prices! Free shipping to 185 countries. 45 days money back guarantee. Friendly customer service.', 'rem' );
		echo $description . $tPage;

		return;
	}

	if ( ! is_single() && ! is_tax( 'product_cat' ) && get_post_type() == 'product' ) {
		$description = cz( 'tp_seo_products_description' );
		echo $description . $tPage;

		return;
	}

	$slug = isset( $wp->query_vars[ "pagename" ] ) ? $wp->query_vars[ "pagename" ] : '';

	switch ( $slug ) {
		case 'about-us':
			$description = __( 'We are doing our best to make your shopping online effortless and carefree, we hope you enjoy your experience with us.', 'rem' );
			break;
		case 'cart':
			$description = __( 'Add products to your shopping cart and buy easily anytime with free shipping worldwide.', 'rem' );
			break;
		case 'contact-us':
			$description = __( 'If you have any questions just fill in the contact form and we will respond you promptly.', 'rem' );
			break;
		case 'frequently-asked-questions':
			$description = __( 'Frequently asked questions answered by our store support team, find the questions you wanted to ask', 'rem' );
			break;
		case 'payment-methods':
			$description = __( 'You can use these payment methods to pay for your purchases in this store', 'rem' );
			break;
		case 'shipping-delivery':
			$description = __( 'Our shipping methods include DHL and EMS, terms of delivery depend on customer location and shipping service company', 'rem' );
			break;
		case 'subscription':
			$description = __( 'Subscribe to our newsletter and get the latest updates on your favorite items in our store', 'rem' );
			break;
		case 'thankyou-contact':
			$description = __( 'We are happy to get your feedback, our support team will revise your message and answer in the short run', 'rem' );
			break;
	}

	echo $description . $tPage;

	return;
}

function rem_keywords() {
	global $post;
	$keywords = '';

	if ( adstm_is_home() ) {
		echo cz( 'tp_home_seo_keywords' );

		return;
	}

	if ( is_single() && get_post_type() == 'product' ) {
		$obj = new adsProductTM();
		$obj->getById( $post->ID );
		$keywords = $obj->getSeoKeywords();
		if ( ! $keywords ) {
			$nameProduct = single_post_title( '', false );
			$nameProduct = trim( $nameProduct );
			$nameProduct       = str_replace( array( ' ' ), ',', $nameProduct );
			$nameProduct = str_replace( array( ',,'), ',', $nameProduct );
			echo $nameProduct;
			return;
		}
	}

	/*products all*/
	if ( ! is_single() && ! is_tax( 'product_cat' ) && get_post_type() == 'product' ) {
		$keywords = cz( 'tp_seo_products_keywords' );
		if ( $keywords ) {
			echo $keywords;

			return;
		}

		return;
	}

	if ( is_tax( 'product_cat' ) ) {
		$keywords = get_query_var( 'seo_keywords' );
		if ( $keywords ) {
			echo $keywords;

			return;
		}

		return;
	}

	echo $keywords;
}



<?php get_header(); ?>

	<?php if ( is_post_type_archive('product') || is_tax( 'product_cat' ) )
			// Is Products
			get_template_part( 'tpl/product/index' );

		else
			// If Blogs
			get_template_part( 'tpl/blog/index' ); ?>

<?php get_footer(); ?>